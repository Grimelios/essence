#include "Tileset.h"

namespace Essence
{
	Tileset::Tileset(std::string filename, const int tileWidth, const int tileHeight, const int tileSpacing) :
		filename(std::move(filename)),
		tileWidth(tileWidth),
		tileHeight(tileHeight),
		tileSpacing(tileSpacing)
	{
	}

	const std::string& Tileset::GetFilename() const
	{
		return filename;
	}

	int Tileset::GetTileWidth() const
	{
		return tileWidth;
	}

	int Tileset::GetTileHeight() const
	{
		return tileHeight;
	}

	int Tileset::GetTileSpacing() const
	{
		return tileSpacing;
	}
}
