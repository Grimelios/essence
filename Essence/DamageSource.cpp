#include "DamageSource.h"
#include "Space.h"
#include "ITargetable.h"

namespace Essence
{
	DamageSource::DamageSource(const int damage, const int lifetime, Space* space, Shape* shape, Entity* source) :
		source(source),
		damage(damage)
	{
		sensor = space->CreateSensor(shape, this, SensorTypes::Dynamic);
		sensor->onContact = [this](void* data)
		{
			ITargetable* target = static_cast<ITargetable*>(data);
			target->ApplyDamage(this->damage);
		};

		// A lifetime of -1 indicates that the damage source is infinite (like a static pool of lava).
		if (lifetime != -1)
		{
			timer = SingleTimer(lifetime, std::nullopt);
		}
	}

	bool DamageSource::HasExpired() const
	{
		return timer.has_value() && timer.value().HasCompleted();
	}

	void DamageSource::Dispose()
	{
		sensor->Dispose();
	}

	void DamageSource::Update(const float dt)
	{
		if (timer.has_value())
		{
			timer->Update(dt);
		}
	}
}
