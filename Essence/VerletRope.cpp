#include "VerletRope.h"
#include "Space.h"
#include <glm/detail/func_geometric.inl>

namespace Essence
{
	VerletRope::VerletRope(const bool isAscendable) :
		isAscendable(isAscendable)
	{
	}

	VerletRope::VerletRope(const int segmentCount, const int segmentLength, const bool isAscendable) :
		points(segmentCount + 1, VerletPoint()),
		offsets(segmentCount),
		segmentLength(segmentLength),
		isAscendable(isAscendable)
	{
		if (isAscendable)
		{
			box = std::optional<AxisBox>();
		}
	}

	std::vector<VerletPoint>& VerletRope::GetVerletPoints()
	{
		return points;
	}

	std::vector<glm::vec2> VerletRope::GetPoints() const
	{
		std::vector<glm::vec2> pList;
		pList.reserve(points.size());

		for (const VerletPoint& p : points)
		{
			pList.push_back(p.position);
		}

		return pList;
	}

	Bounds VerletRope::GetBounds() const
	{
		// If this function is called, it's assumed that the rope is ascendable.
		return box->ToBounds();
	}

	void VerletRope::SetSegmentData(const int segmentCount, const int segmentLength)
	{
		this->segmentLength = segmentLength;

		points.reserve(segmentCount);
		offsets.reserve(segmentCount);

		for (int i = 0; i < segmentCount; i++)
		{
			points.emplace_back();
		}
	}

	void VerletRope::Update(const float dt)
	{
		for (VerletPoint& p : points)
		{
			if (p.fixed)
			{
				continue;
			}

			glm::vec2& position = p.position;

			const glm::vec2 temp = position;

			position.y += 15 * dt;
			position += (position - p.previousPosition) * 0.98f;
			p.previousPosition = temp;
		}

		const int count = static_cast<int>(points.size());

		for (int k = 0; k < 8; k++)
		{
			for (int i = 0; i < count - 1; i++)
			{
				const glm::vec2 p1 = points[i].position;
				const glm::vec2 p2 = points[i + 1].position;

				const float distance = glm::distance(p1, p2);

				if (distance != 0 && distance > segmentLength)
				{
					offsets[i] = (distance - segmentLength) * (p2 - p1) / distance / 2.0f;
				}
				else
				{
					offsets[i] = glm::vec2(0);
				}
			}

			for (int i = 0; i < count; i++)
			{
				VerletPoint& p = points[i];

				if (p.fixed)
				{
					continue;
				}

				glm::vec2& position = p.position;

				if (i == 0)
				{
					position += offsets[0] * 2.0f;
				}
				else if (i == count - 1)
				{
					position -= offsets[i - 1] * 2.0f;
				}
				else
				{
					position += offsets[i] - offsets[i - 1];
				}
			}
		}

		if (!isAscendable)
		{
			return;
		}

		glm::vec2 min(std::numeric_limits<float>::max());
		glm::vec2 max(std::numeric_limits<float>::min());

		for (const auto& point : points)
		{
			const glm::vec2 p = point.position;

			min.x = std::min(p.x, min.x);
			min.y = std::min(p.y, min.y);
			max.x = std::max(p.x, max.x);
			max.y = std::max(p.y, max.y);
		}

		box->SetWidth(max.x - min.x);
		box->SetHeight(max.y - min.y);
		box->position.x = min.x + box->GetHalfWidth();
		box->position.y = min.y + box->GetHalfHeight();
	}
}
