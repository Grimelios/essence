#pragma once
#include "Entity.h"
#include "Shader.h"

namespace Essence
{
	class Lava : public Entity
	{
	private:

		Shader shader;

	public:

		Lava();

		void Draw(SpriteBatch& sb) override;
	};
}
