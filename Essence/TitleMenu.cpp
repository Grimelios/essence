#include "TitleMenu.h"
#include "BasicMenuItem.h"
#include "ContentCache.h"
#include "Messaging.h"
#include "GameFunctions.h"

namespace Essence
{
	TitleMenu::TitleMenu() : Menu(32)
	{
		const SpriteFont& font = ContentCache::GetFont("Basic");

		std::array<std::string, 5> names =
		{
			"Continue",
			"New Game",
			"Load Game",
			"Settings",
			"Exit"
		};

		ItemList items;

		for (int i = 0; i < static_cast<int>(names.size()); i++)
		{
			ItemPointer pointer = std::make_unique<BasicMenuItem>(font, names[i], Alignments::Left | Alignments::Top,
				i, this);

			items.push_back(std::move(pointer));

			MenuItem* item = items.back().get();

			item->SetEnabled(i > 0);
			containers.push_back(item);
		}

		Initialize(items, 1);
	}

	void TitleMenu::Submit(const int index)
	{
		switch (index)
		{
			// Continue
			case 0: break;
			
			// New Game
			case 1: break;

			// Load Game
			case 2: break;

			// Settings
			case 3: break;

			// Exit
			case 4: Messaging::Send(MessageTypes::Exit, nullptr); break;
		}
	}
}
