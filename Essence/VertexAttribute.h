#pragma once
#include <glad/glad.h>

namespace Essence
{
	class VertexAttribute
	{
	private:

		GLenum type;

		int count;
		int offset;

		bool normalized;

	public:

		VertexAttribute(int count, int offset, GLenum type, bool normalized);

		GLenum GetType() const;

		int GetCount() const;
		int GetOffset() const;

		bool IsNormalized() const;
	};
}
