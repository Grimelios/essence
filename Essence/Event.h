#pragma once
#include <vector>
#include <functional>

namespace Essence
{
	template<class... Args>
	class Event
	{
		using Func = std::function<void(Args...)>;

		std::vector<Func> functions;

	public:

		void Add(Func function);
		void Invoke(Args... args);
	};

	template<class... Args>
	void Event<Args...>::Add(Func function)
	{
		functions.push_back(std::move(function));
	}

	template<class... Args>
	void Event<Args...>::Invoke(Args... args)
	{
		for (auto& f : functions)
		{
			f(args...);
		}
	}
}
