#pragma once
#include "Timer.h"

namespace Essence
{
	class RepeatingTimer : public Timer
	{
	private:

		using RepeatingFunction = std::function<bool(float)>;

		RepeatingFunction trigger;

	public:

		RepeatingTimer(int duration, RepeatingFunction trigger, float elapsed = 0);

		void Update(float dt) override;
	};
}
