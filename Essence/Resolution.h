#pragma once
#include <glm/vec2.hpp>

namespace Essence
{
	class Resolution
	{
	public:

		// To handle different window sizes, the full game is always rendered at a preset resolution, then scaled to
		// fit the current window. This ensures that all players will see the same portion of the world, such that it's
		// not possible to gain an advantage through an increased visual range.
		static const int Width;
		static const int Height;

		static int WindowWidth;
		static int WindowHeight;

		static glm::ivec2 GetDimensions();
		static glm::ivec2 GetWindowDimensions();
	};
}
