#pragma once
#include "LivingEntity.h"

namespace Essence
{
	class Enemy : public LivingEntity
	{
	protected:

		Enemy();

	public:

		virtual ~Enemy() = 0;
	};

	inline Enemy::~Enemy() = default;
}
