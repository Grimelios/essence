#pragma once
#include <string>
#include "SpriteFont.h"

namespace Essence
{
	class ContentCache;
	class FontLoader
	{
	private:

		int ParseValue(const std::string& s) const;
		void ParseKerning();

	public:

		SpriteFont Load(const std::string& name, bool useKerning = true) const;
	};
}
