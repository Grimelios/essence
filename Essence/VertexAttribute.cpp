#include "VertexAttribute.h"

namespace Essence
{
	VertexAttribute::VertexAttribute(const int count, const int offset, const GLenum type, const bool normalized) :
		type(type),
		count(count),
		offset(offset),
		normalized(normalized)
	{
	}

	GLenum VertexAttribute::GetType() const
	{
		return type;
	}

	int VertexAttribute::GetCount() const
	{
		return count;
	}

	int VertexAttribute::GetOffset() const
	{
		return offset;
	}

	bool VertexAttribute::IsNormalized() const
	{
		return normalized;
	}
}
