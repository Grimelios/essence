#pragma once
#include "IRenderable.h"
#include "Sprite.h"
#include "SpriteText.h"

namespace Essence
{
	class GLTester : public IRenderable
	{
	private:

		Sprite sprite1;
		Sprite sprite2;
		Sprite sprite3;
		SpriteText text1;
		SpriteText text2;

	public:

		GLTester();

		void Draw(SpriteBatch& sb) override;
	};
}
