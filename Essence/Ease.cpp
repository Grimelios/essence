#include "Ease.h"

namespace Essence
{
	float Ease::Compute(const EaseTypes type, const float t)
	{
		switch (type)
		{
			case EaseTypes::QuadraticIn: return QuadraticIn(t);
			case EaseTypes::QuadraticOut: return QuadraticOut(t);
			case EaseTypes::CubicIn: return CubicIn(t);
			case EaseTypes::CubicOut: return CubicOut(t);
		}

		// This is equivalent to linear.
		return t;
	}

	float Ease::QuadraticIn(const float t)
	{
		return t * t;
	}

	float Ease::QuadraticOut(const float t)
	{
		return -(t * (t - 2));
	}

	float Ease::CubicIn(const float t)
	{
		return t * t * t;
	}

	float Ease::CubicOut(const float t)
	{
		const float f = t - 1;

		return f * f * f + 1;
	}
}
