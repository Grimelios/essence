#include "HudElement.h"

namespace Essence
{
	HudElement::HudElement(const int offsetX, const int offsetY, const Alignments alignment) :
		offsetX(offsetX),
		offsetY(offsetY),
		alignment(alignment)
	{
	}

	int HudElement::GetOffsetX() const
	{
		return offsetX;
	}

	int HudElement::GetOffsetY() const
	{
		return offsetY;
	}

	Alignments HudElement::GetAlignment() const
	{
		return alignment;
	}
}
