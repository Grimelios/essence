#include "SplashLoop.h"

namespace Essence
{
	SplashLoop::SplashLoop(Camera& camera, SpriteBatch& sb) : GameLoop(camera, sb)
	{
	}

	void SplashLoop::Initialize()
	{
	}

	void SplashLoop::Update(const float dt)
	{
	}

	void SplashLoop::Draw(SpriteBatch& sb)
	{
	}
}
