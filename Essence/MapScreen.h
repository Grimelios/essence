#pragma once
#include "Screen.h"
#include "MapLegend.h"

namespace Essence
{
	class MapScreen : public Screen
	{
	private:

		MapLegend legend;

	public:

		explicit MapScreen(ScreenManager* parent);

		void OnResize(int width, int height) override;
		void Show() override;
		void Hide() override;
	};
}
