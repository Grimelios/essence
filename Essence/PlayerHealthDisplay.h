#pragma once
#include "HudElement.h"

namespace Essence
{
	class PlayerHealthDisplay : public HudElement
	{
	public:

		PlayerHealthDisplay(int offsetX, int offsetY, Alignments alignment);

		void OnDamage(int damage);
		void OnHeal(int healing);
		void OnMaxHealthIncrease(int maxHealth);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
