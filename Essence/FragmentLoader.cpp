#include "FragmentLoader.h"
#include "JsonUtilities.h"
#include "Bed.h"
#include "Door.h"
#include "Tilemap.h"
#include "Banner.h"
#include "Window.h"
#include "Fire.h"
#include "EntityTypes.h"

namespace Essence
{
	FragmentLoader::EntityList FragmentLoader::Load(const std::string& filename)
	{
		std::map<std::string, Json> blocks = JsonUtilities::Load("Fragments/" + filename);
		EntityList entities;

		for (const auto& pair : blocks)
		{
			switch (pair.first[0])
			{
				// Entities
				case 'E': entities = CreateEntities(pair.second); break;
			}
		}

		return entities;
	}

	FragmentLoader::EntityList FragmentLoader::CreateEntities(const Json& block)
	{
		EntityList list;

		for (const Json& j : block)
		{
			const std::string dump = j.dump();

			const int x = j.at("X").get<int>();
			const int y = j.at("Y").get<int>();

			EntityPointer e = CreateEntity(j);
			e->SetPosition(glm::vec2(x, y));
			list.push_back(std::move(e));
		}

		return list;
	}

	FragmentLoader::EntityPointer FragmentLoader::CreateEntity(const Json& j)
	{
		const auto& properties = j.find("Properties");

		switch (j.at("Type").get<EntityTypes>())
		{
			case EntityTypes::Banner: return std::make_unique<Banner>(j);
			case EntityTypes::Bed: return std::make_unique<Bed>();
			case EntityTypes::Door: return std::make_unique<Door>(properties.value());
			case EntityTypes::Fire: return std::make_unique<Fire>();
			case EntityTypes::Window: return std::make_unique<Window>();
			case EntityTypes::Tilemap: return std::make_unique<Tilemap>(j);
		}

		return nullptr;
	}
}
