#include "Bounds.h"

namespace Essence
{
	Bounds::Bounds() : Bounds(0, 0, 0, 0)
	{
	}

	Bounds::Bounds(const int width, const int height) : Bounds(0, 0, width, height)
	{
	}

	Bounds::Bounds(const int x, const int y, const int width, const int height) :
		x(x),
		y(y),
		width(width),
		height(height)
	{
	}

	Bounds::Bounds(const glm::ivec2& location, const int width, const int height) :
		Bounds(location.x, location.y, width, height)
	{
	}

	int Bounds::GetLeft() const
	{
		return x;
	}

	int Bounds::GetRight() const
	{
		return x + width - 1;
	}

	int Bounds::GetTop() const
	{
		return y;
	}

	int Bounds::GetBottom() const
	{
		return y + height - 1;
	}

	glm::ivec2 Bounds::GetLocation() const
	{
		return glm::ivec2(x, y);
	}

	glm::ivec2 Bounds::GetCenter() const
	{
		return glm::ivec2(x + width / 2, y + height / 2);
	}

	void Bounds::SetLocation(const glm::ivec2& location)
	{
		x = location.x;
		y = location.y;
	}

	void Bounds::SetCenter(const glm::ivec2& center)
	{
		x = center.x - width / 2;
		y = center.y - height / 2;
	}

	void Bounds::SetLeft(const int left)
	{
		x = left;
	}

	void Bounds::SetRight(const int right)
	{
		x = right - width + 1;
	}

	void Bounds::SetTop(const int top)
	{
		y = top;
	}

	void Bounds::SetBottom(const int bottom)
	{
		y = bottom - height + 1;
	}

	void Bounds::Expand(const int amount)
	{
		x -= amount;
		y -= amount;
		width += amount * 2;
		height += amount * 2;
	}

	bool Bounds::Contains(const glm::vec2& position) const
	{
		return position.x >= x && position.x <= GetRight() && position.y >= y && position.y <= GetBottom();
	}

	std::array<glm::vec2, 4> Bounds::GetCorners() const
	{
		const int right = GetRight();
		const int bottom = GetBottom();

		return
		{
			glm::vec2(x, y),
			glm::vec2(right, y),
			glm::vec2(right, bottom),
			glm::vec2(x, bottom)
		};
	}
}
