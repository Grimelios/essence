#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include "SingleTimer.h"
#include "Bounds.h"

namespace Essence
{
	class Letterbox : public IDynamic, public IRenderable
	{
	private:

		static const int RevealTime = 500;
		static const int BarHeight = 40;

		Bounds upperBounds;
		Bounds lowerBounds;

		std::unique_ptr<SingleTimer> timer;

		bool visible = false;
		bool appearing = false;

		void OnResize();
		void Toggle();
		void Tick(float progress);

	public:

		Letterbox();

		bool IsVisible() const;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
