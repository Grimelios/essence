#pragma once
#include "Entity.h"
#include "TilemapData.h"
#include "TilemapRender.h"
#include "AxisBox.h"

namespace Essence
{
	class Body;
	class Tilemap : public Entity
	{
	private:

		// Using a separate rendering class allows non-tilemap classes to render tilemaps with less overhead. That
		// makes this entity mostly a wrapper around that renderer (with Entity features as well).
		TilemapRender render;

		std::vector<AxisBox> boxes;
		std::vector<Body*> bodies;

	public:

		explicit Tilemap(const Json& j);
		explicit Tilemap(const TilemapData& data);

		void Initialize(Scene* scene, EntityLayer* layer) override;
		void SetPosition(const glm::vec2& position) override;
		void Dispose() override;
		void Draw(SpriteBatch& sb) override;
	};
}
