#pragma once
#include "Entity.h"
#include "Shader.h"
#include "TilemapRender.h"

namespace Essence
{
	class RainbowMoss : public Entity
	{
	private:

		Shader shader;
		TilemapRender renderer;

	public:

		RainbowMoss();

		void Initialize(Scene* scene, EntityLayer* layer) override;
		void Draw(SpriteBatch& sb) override;
	};
}
