#include "Rope.h"

namespace Essence
{
	Rope::Rope(const int length) : Entity(EntityGroups::Objects),
		rope(true)
	{
		const float fLength = static_cast<float>(length);
		const float segmentLength = fLength / 30.0f;
		const int segmentCount = static_cast<int>(fLength / segmentLength);

		rope.SetSegmentData(segmentCount, static_cast<int>(segmentLength));
	}

	std::vector<glm::vec2> Rope::GetPoints() const
	{
		return rope.GetPoints();
	}

	void Rope::Update(const float dt)
	{
		rope.Update(dt);
	}

	void Rope::Draw(SpriteBatch& sb)
	{
		const auto& points = rope.GetVerletPoints();

		for (int i = 0; i < static_cast<int>(points.size()); i++)
		{
			//sb.DrawLine(points[i].position, points[i + 1].position, glm::vec4(1));
		}
	}
}
