#pragma once
#include "Entity.h"
#include "VerletCloth.h"

namespace Essence
{
	class Banner : public Entity
	{
	public:

		glm::vec2 start;
		glm::vec2 end;

		//VerletCloth cloth;

		explicit Banner(const Json& j);

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
