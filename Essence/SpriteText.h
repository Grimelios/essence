#pragma once
#include "Component2D.h"
#include <optional>
#include <string>
#include <vector>

namespace Essence
{
	class SpriteFont;
	class SpriteText : public Component2D
	{
	private:

		const SpriteFont& font;

		std::vector<float> buffer;
		std::optional<std::string> value = std::nullopt;

		unsigned renderCount = 0;

	public:

		explicit SpriteText(const std::string& font, Alignments alignment = Alignments::Left | Alignments::Top);
		explicit SpriteText(const SpriteFont& font, Alignments alignment = Alignments::Left | Alignments::Top);

		SpriteText(const std::string& font, const std::string& value,
			Alignments alignment = Alignments::Left | Alignments::Top);
		SpriteText(const SpriteFont& font, const std::string& value,
			Alignments alignment = Alignments::Left | Alignments::Top);

		// Using getter and setter functions for value allows the origin to be re-computed.
		const std::optional<std::string>& GetValue() const;
		const SpriteFont& GetFont() const;

		void Clear();
		void SetValue(const std::string& value);
		void Draw(SpriteBatch& sb) override;
	};
}
