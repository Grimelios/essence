#include "Color.h"
#include <cstring>

namespace Essence
{
	const Color Color::White;
	const Color Color::Black(0);
	const Color Color::Red(255, 0, 0);
	const Color Color::Green(255, 0, 255);
	const Color Color::Blue(0, 0, 255);

	Color::Color() : Color(255, 255, 255, 255)
	{
	}

	Color::Color(const int value) : Color(value, value, value, 255)
	{
	}

	Color::Color(const int r, const int g, const int b) : Color(r, g, b, 255)
	{
	}

	Color::Color(const int r, const int g, const int b, const int a) :
		r(static_cast<std::byte>(r)),
		g(static_cast<std::byte>(g)),
		b(static_cast<std::byte>(b)),
		a(static_cast<std::byte>(a))
	{
	}

	float Color::ToFloat() const
	{
		// See https://stackoverflow.com/questions/3991478/building-a-32-bit-float-out-of-its-4-composite-bytes.
		float f;

		std::byte bytes[] = { r, g, b, a };

		memcpy(&f, bytes, 4);

		return f;
	}
}
