#include "FileUtilities.h"
#include <fstream>
#include <filesystem>
#include "StringUtilities.h"

// I should be able to simply use std::filesystem based on the C++ 17 standard, but for whatever reason, it's not
// recognized.
namespace fs = std::experimental::filesystem;
namespace Essence
{
	std::string FileUtilities::ReadAllText(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/195323/what-is-the-most-elegant-way-to-read-a-text-file-with-c/195350.
		std::ifstream stream(filename);

		if (!stream.good())
		{
			throw std::runtime_error("File [" + filename + "] is not available.");
		}

		std::string value = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());

		stream.close();

		return value;
	}

	std::vector<std::string> FileUtilities::ReadAllLines(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/7868936/read-file-line-by-line/7868998.
		std::ifstream stream(filename);
		std::string line;
		std::vector<std::string> lines;

		while (std::getline(stream, line))
		{
			lines.push_back(std::move(line));
		}

		stream.close();

		return lines;
	}

	std::vector<std::string> FileUtilities::GetFiles(const std::string& directory,
		const std::optional<std::string>& extension)
	{
		std::vector<std::string> files;

		// See https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c/37494654#37494654.
		for (const auto& entry : fs::directory_iterator(directory))
		{
			const std::string filename = entry.path().filename().generic_string();

			if (!extension.has_value() || StringUtilities::EndsWith(filename, extension.value()))
			{
				files.push_back(entry.path().filename().generic_string());
			}
		}

		return files;
	}

	void FileUtilities::SkipLine(std::ifstream& stream)
	{
		stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	void FileUtilities::SkipLines(std::ifstream& stream, const int count)
	{
		for (int i = 0; i < count; i++)
		{
			// See https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line.
			stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}

	void FileUtilities::Write(const std::string& filename, const std::string& value, const WriteModes mode)
	{
		// See https://stackoverflow.com/questions/15056406/c-fstream-overwrite-instead-of-append.
		std::ofstream stream(filename, mode == WriteModes::Append ? std::ios_base::app : std::ios_base::out);
		stream << value;
		stream.close();
	}
}
