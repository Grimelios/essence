#pragma once
#include "LivingEntity.h"

namespace Essence
{
	class Character : public LivingEntity
	{
	private:

		std::string name;

	protected:

		explicit Character(std::string name);

		const std::string& GetName() const;
	};
}
