#pragma once

namespace Essence
{
	// Moving this enum to its own class was necessary to properly link shapes to the helper.
	enum class ShapeTypes
	{
		AxisBox = 0,
		Line = 1,
		Count = 2
	};
};
