#include "Jumper.h"
#include "Player.h"
#include "EntityLayer.h"
#include "AxisBox.h"
#include "Texture2D.h"
#include "SingleTimer.h"
#include "Body.h"

namespace Essence
{
	Jumper::Jumper() : ComplexEntity(EntityGroups::Creatures),
		sprite("Jumper.png")
	{
		const Texture2D& texture = sprite.GetTexture();

		shape = std::make_unique<AxisBox>(texture.GetWidth(), texture.GetHeight());
		components.push_back(&sprite);
	}

	void Jumper::OnPlayerJump(const float speed)
	{
		if (onGround)
		{
			timers.CreateTimer<SingleTimer>(600, [this](const float time)
			{
				Jump();
			});
		}
	}

	void Jumper::Jump()
	{
		body->velocity.y = -500;
		onGround = false;
	}

	bool Jumper::OnCollision(const glm::vec2& normal, Entity* entity)
	{
		if (normal.y == -1)
		{
			onGround = true;
		}

		return true;
	}

	void Jumper::Initialize(Scene* scene, EntityLayer* layer)
	{
		player = static_cast<Player*>(layer->GetEntities(EntityGroups::Player).GetHead()->data.get());
		player->onJump.Add([this](const float speed)
		{
			OnPlayerJump(speed);
		});

		Entity::Initialize(scene, layer);
	}
}
