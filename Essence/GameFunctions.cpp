#include "GameFunctions.h"
#include "Bounds.h"
#include "StringUtilities.h"
#include "SpriteFont.h"
#include <glm/detail/func_geometric.inl>

namespace Essence
{
	int GameFunctions::ComputeSign(const float value)
	{
		// See https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c.
		return (0 < value) - (value < 0);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& value)
	{
		return ComputeAngle(glm::vec2(0), value);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& start, const glm::vec2& end)
	{
		const float dX = end.x - start.x;
		const float dY = end.y - start.y;

		return std::atan2(dY, dX);
	}

	float GameFunctions::ComputeDistanceToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
		glm::vec2& result)
	{
		return std::sqrt(ComputeDistanceSquaredToLine(l1, l2, p, result));
	}

	float GameFunctions::ComputeDistanceSquaredToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
		glm::vec2& result)
	{
		// See https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment.
		const float squared = ComputeDistanceSquared(l1, l2);

		if (squared == 0.0)
		{
			result = l1;

			return ComputeDistanceSquared(p, l1);
		}

		const float t = std::max(0.0f, std::min(1.0f, dot(p - l1, l2 - l1) / squared));

		result = l1 + t * (l2 - l1);

		return ComputeDistanceSquared(p, result);
	}

	float GameFunctions::ComputeLengthSquared(const glm::vec2& value)
	{
		return value.x * value.x + value.y * value.y;
	}

	float GameFunctions::ComputeDistanceSquared(const glm::vec2& p1, const glm::vec2& p2)
	{
		return ComputeLengthSquared(p2 - p1);
	}

	bool GameFunctions::IsInteger(const std::string& s)
	{
		// See https://stackoverflow.com/questions/2844817/how-do-i-check-if-a-c-string-is-an-int.
		if (s.empty() || (!isdigit(s[0]) && s[0] != '-' && s[0] != '+'))
		{
			return false;
		}

		char* p;
		strtol(s.c_str(), &p, 10);

		return *p == 0;
	}

	std::string GameFunctions::ToString(const glm::vec2& value)
	{
		const int x = static_cast<int>(value.x);
		const int y = static_cast<int>(value.y);

		return std::to_string(x) + ", " + std::to_string(y);
	}

	std::string GameFunctions::ToString(const Bounds& bounds)
	{
		return std::to_string(bounds.x) + ", " + std::to_string(bounds.y) + ", " + std::to_string(bounds.width) +
			", " + std::to_string(bounds.height);
	}

	std::vector<std::string> GameFunctions::WrapLines(const std::string& value, const SpriteFont& font,
		const int width)
	{
		std::vector<std::string> lines;
		std::vector<std::string> words = StringUtilities::Split(value, ' ');
		std::string currentLine;

		int currentWidth = 0;

		const int spaceWidth = font.Measure(" ").x;

		for (const std::string& word : words)
		{
			const int wordWidth = font.Measure(word).x;

			if (currentWidth + wordWidth > width)
			{
				lines.push_back(std::move(currentLine));
				currentLine = word + " ";
				currentWidth = wordWidth + spaceWidth;
			}
			else
			{
				currentWidth += wordWidth + spaceWidth;
				currentLine += word + " ";
			}
		}

		if (!currentLine.empty())
		{
			lines.push_back(std::move(currentLine));
		}

		return lines;
	}

	glm::vec2 GameFunctions::ParseVec2(const std::string& s)
	{
		// Vec2s are in the form "x,y" (note the lack of a space).
		const int index = StringUtilities::IndexOf(s, ',');
		const float x = std::stof(s.substr(0, index));
		const float y = std::stof(s.substr(index + 1, s.size() - index - 1));

		return glm::vec2(x, y);
	}

	std::vector<glm::vec2> GameFunctions::ParseVec2List(const std::string& s)
	{
		// This assumes tokens are separated by spaces.
		const std::vector<std::string> tokens = StringUtilities::Split(s, ' ');

		std::vector<glm::vec2> points;
		points.reserve(tokens.size());

		for (const std::string& t : tokens)
		{
			points.push_back(ParseVec2(t));
		}

		return points;
	}

	glm::vec2 GameFunctions::ComputeDirection(const float angle)
	{
		const float x = std::cos(angle);
		const float y = std::sin(angle);

		return glm::vec2(x, y);
	}

	glm::vec2 GameFunctions::ComputeOrigin(const int width, const int height, Alignments alignment)
	{
		const int alignmentValue = static_cast<int>(alignment);

		const bool left = (alignmentValue & static_cast<int>(Alignments::Left)) > 0;
		const bool right = (alignmentValue & static_cast<int>(Alignments::Right)) > 0;
		const bool top = (alignmentValue & static_cast<int>(Alignments::Top)) > 0;
		const bool bottom = (alignmentValue & static_cast<int>(Alignments::Bottom)) > 0;

		const float x = static_cast<float>(left ? 0 : (right ? width : width / 2));
		const float y = static_cast<float>(top ? 0 : (bottom ? height : height / 2));

		return glm::vec2(x, y);
	}
}
