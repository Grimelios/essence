#include "VerletCloth.h"
#include <glm/detail/func_geometric.inl>

namespace Essence
{
	VerletCloth::VerletCloth(const int width, const int height, const int segmentLength) :
		width(width),
		height(height),
		segmentLength(static_cast<float>(segmentLength)),
		points(width, height, VerletPoint())
	{
		crossSegmentLength = length(glm::vec2(this->segmentLength));
		constraints.reserve((width - 1) * height + width * (height - 1));

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width - 1; j++)
			{
				const int endpoint = -1;// j > 0 && j < width - 2 ? -1 : j / (width - 2);

				constraints.emplace_back(points.At(j, i), points.At(j + 1, i), this->segmentLength, endpoint);
			}
		}

		for (int i = 0; i < height - 1; i++)
		{
			const int endpoint = -1;// i > 0 && i < height - 2 ? -1 : i / (height - 2);

			for (int j = 0; j < width; j++)
			{
				constraints.emplace_back(points.At(j, i), points.At(j, i + 1), this->segmentLength, endpoint);
			}
		}

		auto& list = points.GetList();

		for (int i = 0; i < list.size(); i++)
		{
			list[i].index = i;
		}
	}

	int VerletCloth::GetWidth() const
	{
		return width;
	}

	int VerletCloth::GetHeight() const
	{
		return height;
	}

	Grid<VerletPoint>& VerletCloth::GetPoints()
	{
		return points;
	}

	void VerletCloth::Update(const float dt)
	{
		for (VerletPoint& p : points.GetList())
		{
			if (p.fixed)
			{
				continue;
			}

			glm::vec2& position = p.position;

			const glm::vec2 temp = position;

			position.y += 15 * dt;
			position += (position - p.previousPosition) * 0.1f;
			p.previousPosition = temp;
		}

		for (int iteration = 0; iteration < 8; iteration++)
		{
			for (VerletConstraint& c : constraints)
			{
				c.Compute();
			}

			for (VerletConstraint& c : constraints)
			{
				c.Resolve();
			}
		}
	}
}
