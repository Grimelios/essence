#include "TilemapData.h"
#include "StringUtilities.h"
#include "JsonUtilities.h"

namespace Essence
{
	TilemapData::TilemapData(const Json& j)
	{
		width = j.at("Width").get<int>();
		height = j.at("Height").get<int>();
		tileset = j.at("Tileset").get<std::string>();
		tiles.reserve(width * height);

		const std::string rawTiles = j.at("Tiles").get<std::string>();

		// This assumes that tiles will be listed in one long comma-separated string (no line breaks).
		for (const std::string& s : StringUtilities::Split(rawTiles, ','))
		{
			tiles.push_back(std::stoi(s));
		}

		std::vector<Json> jCollisions = j.at("Collisions");

		for (const Json& box : jCollisions)
		{
			const int x = box.at("X").get<int>();
			const int y = box.at("Y").get<int>();
			const int width = box.at("Width").get<int>();
			const int height = box.at("Height").get<int>();

			boxes.emplace_back(x, y, width, height);
		}
	}
}
