#pragma once
#include "IDynamic.h"
#include <glm/vec2.hpp>

namespace Essence
{
	class AngleSpring : public IDynamic
	{
	private:

		float k = 0;
		float damping = 0;

	public:

		AngleSpring(float k, float damping);

		glm::vec2 position = glm::vec2(0);

		float angle = 0;
		float angularVelocity = 0;
		float targetAngle = 0;
		float length = 0;

		void Update(float dt) override;
	};
}
