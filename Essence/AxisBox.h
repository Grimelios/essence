#pragma once
#include "Shape.h"
#include "Bounds.h"

namespace Essence
{
	class AxisBox : public Shape
	{
	private:

		float halfWidth;
		float halfHeight;

	public:

		AxisBox();
		explicit AxisBox(float size);
		AxisBox(int width, int height);
		AxisBox(float width, float height);
		AxisBox(int x, int y, int width, int height);
		AxisBox(float x, float y, float width, float height);
		AxisBox(const glm::vec2& center, float width, float height);

		float GetWidth() const;
		float GetHeight() const;
		float GetHalfWidth() const;
		float GetHalfHeight() const;

		void SetWidth(float width);
		void SetHeight(float height);

		Bounds ToBounds() const;
	};
}
