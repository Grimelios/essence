#include "GLTester.h"
#include "SpriteBatch.h"

namespace Essence
{
	GLTester::GLTester() : 
		sprite1("Ori.png"),
		sprite2("Ori.png"),
		sprite3("Ori.png"),
		text1("Debug", "Trouble in Terrorist Town"),
		text2("Basic", "Ori and the Blind Forest")
	{
		sprite1.SetPosition(350, 250);
		sprite2.SetPosition(400, 200);
		sprite2.SetColor(255, 255, 0);
		sprite3.SetPosition(100, 210);
		sprite3.SetColor(0, 255, 0);
		sprite3.SetScale(0.6f);
		text1.SetColor(255, 0, 0);
		text1.SetPosition(100, 400);
		text2.SetColor(128, 255, 128);
		text2.SetPosition(130, 430);
	}

	void GLTester::Draw(SpriteBatch& sb)
	{
		sb.DrawLine(glm::vec2(100), glm::vec2(500, 300), Color(0, 255, 255));
		sb.DrawLine(glm::vec2(120, 200), glm::vec2(600, 250), Color(255, 0, 255));
		sb.Flush();

		sprite1.SetRotation(sprite1.GetRotation() + 0.01f);
		sprite1.SetScale(sprite1.GetScale() + 0.001f);
		sprite3.SetRotation(sprite3.GetRotation() - 0.01f);
		sprite3.SetPosition(sprite3.GetPosition() + glm::vec2(1, 0));
		sprite3.Draw(sb);
		sprite1.Draw(sb);
		sprite2.Draw(sb);
		text1.Draw(sb);
		text2.Draw(sb);
	}
}
