#pragma once
#include "ControlClass.h"

namespace Essence
{
	class PlayerControls : ControlClass
	{
	public:

		PlayerControls();

		std::vector<InputBind> runLeft;
		std::vector<InputBind> runRight;
		std::vector<InputBind> jump;
		std::vector<InputBind> ascend;
		std::vector<InputBind> divebomb;
		std::vector<InputBind> interact;
		std::vector<InputBind> attack;
	};
}
