#include "Lava.h"

namespace Essence
{
	Lava::Lava() : Entity(EntityGroups::World)
	{
		shader.Attach(ShaderTypes::Vertex, "Lava.vert");
		shader.Attach(ShaderTypes::TesselationControl, "Lava.tesc");
		shader.Attach(ShaderTypes::TesselationEvaluation, "Lava.tese");
		shader.Attach(ShaderTypes::Fragment, "Lava.tese");
		shader.CreateProgram();
		shader.AddAttribute<float>(3, GL_FLOAT, false);
		shader.AddAttribute<float>(2, GL_FLOAT, false);
		shader.AddAttribute<float>(3, GL_FLOAT, false);

		glPatchParameteri(GL_PATCH_VERTICES, 3);
	}

	void Lava::Draw(SpriteBatch& sb)
	{
	}
}
