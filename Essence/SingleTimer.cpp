#include "SingleTimer.h"

namespace Essence
{
	SingleTimer::SingleTimer(const int duration, std::optional<SingleFunction> trigger, const float elapsed) :
		Timer(duration, elapsed),

		trigger(std::move(trigger))
	{
	}

	void SingleTimer::Update(const float dt)
	{
		if (paused)
		{
			return;
		}

		elapsed += dt;

		if (elapsed >= duration)
		{
			if (tick.has_value())
			{
				tick.value()(1);
			}

			if (trigger.has_value())
			{
				trigger.value()(elapsed - duration);
			}

			completed = true;

			return;
		}

		if (tick.has_value())
		{
			tick.value()(elapsed / duration);
		}
	}
}
