#include "Player.h"
#include "Messaging.h"
#include <glm/vec2.hpp>
#include "Texture2D.h"
#include "AxisBox.h"
#include "Body.h"
#include "Scene.h"
#include "World.h"
#include "IBouncy.h"

namespace Essence
{
	Player::Player() : LivingEntity(EntityGroups::Player),
		sprite("Player.png"),
		controller(*this, playerData)
	{
		const Texture2D& texture = sprite.GetTexture();
		
		shape = std::make_unique<AxisBox>(texture.GetWidth(), texture.GetHeight());
		components.push_back(&sprite);

		for (int i = 0; i < TotalSkills; i++)
		{
			skillsUnlocked[i] = false;
			skillsEnabled[i] = false;
		}
	}

	void Player::OnLanding(Entity* entity)
	{
		IBouncy* bouncy = dynamic_cast<IBouncy*>(entity);

		if (bouncy != nullptr)
		{
			body->velocity.y = static_cast<float>(-bouncy->GetBounceSpeed());
		}
		else
		{
			onGround = true;
			jumpActive = false;
			body->velocity.y = 0;
			jumpBindUsed = std::nullopt;
			skillsEnabled[JumpIndex] = skillsUnlocked[JumpIndex];
		}

		jumpsRemaining = 1;
		jumpDecelerating = false;
		skillsEnabled[DoubleJumpIndex] = skillsUnlocked[DoubleJumpIndex];
	}

	void Player::UnlockSkill(const PlayerSkills skill)
	{
		const int index = static_cast<int>(skill);

		skillsUnlocked[index] = true;
		skillsEnabled[index] = CheckSkillEnabledOnUnlock(skill);
	}

	bool Player::CheckSkillEnabledOnUnlock(const PlayerSkills skill) const
	{
		switch (skill)
		{
			case PlayerSkills::Jump:
			case PlayerSkills::DoubleJump: return onGround;
			case PlayerSkills::Ascend: return false;
			case PlayerSkills::Divebomb: return !onGround;
		}

		return true;
	}

	bool Player::OnCollision(const glm::vec2& normal, Entity* entity)
	{
		if (normal.y == -1)
		{
			OnLanding(entity);
		}

		return true;
	}

	void Player::Update(const float dt)
	{
		const float gravityForce = scene->GetWorld()->gravity * dt;
		const float limit = playerData.jumpSpeedLimited;

		glm::vec2& velocity = body->velocity;

		if (jumpDecelerating)
		{
			// The downward force of gravity will always be less than jump deceleration (which is artificially high).
			// However, jump deceleration is only meant to push the player towards the limit speed, so if the current Y
			// velocity is close enough to the limit speed that gravity would bring it there anyway, jump deceleration
			// is ignored and simple gravity is used instead. This is an edge case, but likely a fairly common one.
			if (limit - velocity.y <= gravityForce)
			{
				velocity.y += gravityForce;
				jumpDecelerating = false;
			}
			else
			{
				velocity.y += playerData.jumpDeceleration * dt;;

				if (velocity.y >= limit)
				{
					velocity.y = limit;
					jumpDecelerating = false;
				}
			}
		}
	}

	void Player::Draw(SpriteBatch& sb)
	{
		Entity::Draw(sb);
	}
}
