#include "PauseMenu.h"
#include "ContentCache.h"
#include "BasicMenuItem.h"
#include "Messaging.h"
#include "PauseScreen.h"

namespace Essence
{
	PauseMenu::PauseMenu(PauseScreen* parent) : Menu(24),
		parent(parent)
	{
		const SpriteFont& font = ContentCache::GetFont("Pause");

		std::array<std::string, 4> values =
		{
			"Resume",
			"Settings",
			"Exit to Title",
			"Exit to Desktop"
		};

		ItemList items;

		for (int i = 0; i < static_cast<int>(values.size()); i++)
		{
			ItemPointer pointer = std::make_unique<BasicMenuItem>(font, values[i], Alignments::Center, i, this);

			items.push_back(std::move(pointer));

			MenuItem* item = items.back().get();

			containers.push_back(item);
		}

		Initialize(items, 0);
	}

	void PauseMenu::Submit(const int index)
	{
		switch (index)
		{
			// Resume
			case 0: parent->Unpause(); break;

			// Settings
			case 1: break;

			// Exit to title
			case 2: break;

			// Exit to desktop
			case 3: Messaging::Send(MessageTypes::Exit, nullptr); break;
		}
	}
}
