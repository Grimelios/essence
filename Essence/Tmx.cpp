#include "Tmx.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"
#include "GameFunctions.h"

namespace Essence
{
	const std::string Tmx::TmxDirectory = Paths::Content + "Tmx/";
	const std::map<std::string, EntityTypes> Tmx::TypeMap =
	{
		{ "Banner", EntityTypes::Banner },
		{ "Bed", EntityTypes::Bed },
		{ "Door", EntityTypes::Door },
		{ "Fire", EntityTypes::Fire },
		{ "Window", EntityTypes::Window },
	};

	const std::map<std::string, TiledShapes> Tmx::ShapeMap =
	{
		{ "point", TiledShapes::Point },
		{ "polyline", TiledShapes::Polyline }
	};

	void Tmx::Convert()
	{
		for (const std::string& filename : FileUtilities::GetFiles(TmxDirectory, ".tmx"))
		{
			ParseFragment(filename);
		}
	}

	void Tmx::ParseFragment(const std::string& filename)
	{
		std::string source = FileUtilities::ReadAllText(TmxDirectory + filename);

		XmlDocument document;
		document.parse<0>(source.data());

		XmlNode* map = document.first_node("map");

		Json j;
		Json tilemap;
		Json collisions;
		Json entities;

		for (XmlNode* node = map->first_node(); node != nullptr; node = node->next_sibling())
		{
			const char c = node->name()[0];

			// Tileset
			if (c == 't')
			{
				// This assumes each .tmx file contains one tileset with the layer directly after.
				XmlNode* tileset = node;
				XmlNode* layer = node = node->next_sibling();

				tilemap = ParseTiles(map, tileset, layer);
			}
			// Object group
			else if (c == 'o')
			{
				// Object group nodes have only one attribute (their name).
				const char n = node->first_attribute()->value()[0];

				// Entities
				if (n == 'E')
				{
					for (const auto& e : ParseEntities(node))
					{
						entities.push_back(e);
					}
				}
				// Collisions
				else if (n == 'C')
				{
					collisions = ParseCollisions(node);
				}
			}
		}

		tilemap["Type"] = static_cast<int>(EntityTypes::Tilemap);
		tilemap["X"] = 0;
		tilemap["Y"] = 0;
		tilemap["Collisions"] = collisions;
		entities.push_back(std::move(tilemap));

		j["Entities"] = entities;

		const std::string jsonFilename = StringUtilities::RemoveExtension(filename) + ".json";

		FileUtilities::Write(Paths::Json + "Fragments/" + jsonFilename, j.dump());
	}

	Tmx::Json Tmx::ParseTiles(XmlNode* map, XmlNode* tileset, XmlNode* layer)
	{
		const int width = std::stoi(layer->first_attribute("width")->value());
		const int height = std::stoi(layer->first_attribute("height")->value());

		Json j;
		j["Width"] = width;
		j["Height"] = height;
		j["TileWidth"] = std::stoi(map->first_attribute("tilewidth")->value());
		j["TileHeight"] = std::stoi(map->first_attribute("tileheight")->value());
		j["Tileset"] = StringUtilities::RemoveExtension(tileset->first_attribute("source")->value());

		std::vector<std::string> lines = StringUtilities::Split(layer->first_node()->value(), '\n');
		std::string tiles;

		// This accounts for all tiles with commas in between.
		tiles.reserve(width * height * 2 - 1);

		// The first line is an empty newline.
		for (int i = 1; i <= height; i++)
		{
			std::vector<std::string> tokens = StringUtilities::Split(lines[i], ',');

			for (int k = 0; k < width; k++)
			{
				// Tiled uses zero to represent empty tiles rather than -1.
				tiles += std::to_string(std::stoi(tokens[k]) - 1);

				if (i == height && k == width - 1)
				{
					break;
				}

				tiles += ',';
			}
		}

		j["Tiles"] = tiles;

		return j;
	}

	std::vector<Tmx::Json> Tmx::ParseEntities(XmlNode* group)
	{
		std::vector<Json> blocks;

		for (XmlNode* node = group->first_node(); node != nullptr; node = node->next_sibling())
		{
			Json j;
			j["Type"] = TypeMap.at(node->first_attribute("type")->value());

			const int x = std::stoi(node->first_attribute("x")->value());
			const int y = std::stoi(node->first_attribute("y")->value());

			XmlNode* shape = node->first_node();

			// I'm pretty sure the properties node always comes before the shape, but better safe.
			while (shape->name()[1] == 'r')
			{
				shape = shape->next_sibling();
			}

			// For simplicity, every entity is given coordinates (even line-based objects).
			j["X"] = x;
			j["Y"] = y;

			switch (ShapeMap.at(shape->name()))
			{
				case TiledShapes::Polyline: j["Points"] = shape->first_attribute()->value(); break;
			}

			XmlNode* properties = node->first_node("properties");

			if (properties != nullptr)
			{
				Json jProperties;

				for (XmlNode* p = properties->first_node(); p != nullptr; p = p->next_sibling())
				{
					const std::string& key = p->first_attribute()->value();
					const std::string& value = p->first_attribute("value")->value();

					if (GameFunctions::IsInteger(value))
					{
						jProperties[key] = std::stoi(value);
					}
					else
					{
						jProperties[key] = value;
					}
				}

				j["Properties"] = jProperties;
			}

			blocks.push_back(j);
		}

		return blocks;
	}

	Tmx::Json Tmx::ParseCollisions(XmlNode* group)
	{
		Json j;

		for (XmlNode* node = group->first_node(); node != nullptr; node = node->next_sibling())
		{
			Json jBox;
			jBox["X"] = std::stoi(node->first_attribute("x")->value());
			jBox["Y"] = std::stoi(node->first_attribute("y")->value());
			jBox["Width"] = std::stoi(node->first_attribute("width")->value());
			jBox["Height"] = std::stoi(node->first_attribute("height")->value());

			j.push_back(jBox);
		}

		return j;
	}
}
