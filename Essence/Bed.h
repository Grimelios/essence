#pragma once
#include "IBouncy.h"
#include "Entity.h"
#include "Sprite.h"

namespace Essence
{
	class Bed : public Entity, public IBouncy
	{
	private:

		int bounceSpeed = 500;

		Sprite sprite;

	public:

		Bed();

		int GetBounceSpeed() const override;
	};
}
