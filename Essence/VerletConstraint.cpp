#include "VerletConstraint.h"
#include "VerletPoint.h"
#include <glm/detail/func_geometric.inl>

namespace Essence
{
	VerletConstraint::VerletConstraint(VerletPoint& p1, VerletPoint& p2, const float segmentLength, const int endpoint) :
		p1(p1),
		p2(p2),
		segmentLength(segmentLength),
		endpoint(endpoint)
	{
	}

	void VerletConstraint::Compute()
	{
		const float distance = glm::distance(p1.position, p2.position);

		shouldConstrain = distance != 0 && distance > segmentLength;

		if (shouldConstrain)
		{
			offset = (distance - segmentLength) * (p2.position - p1.position) / distance / 2.0f;
		}
	}

	void VerletConstraint::Resolve() const
	{
		if (!shouldConstrain)
		{
			return;
		}

		// It's assumed that two adjacent points will never both be fixed.
		if (p1.fixed || (endpoint == 1 && !p2.fixed))
		{
			p2.position -= offset * 2.0f;

			return;
		}
		
		if (p2.fixed || endpoint == 0)
		{
			p1.position += offset * 2.0f;
			
			return;
		}

		p1.position += offset;
		p2.position -= offset;
	}
}
