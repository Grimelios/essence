#include "World.h"
#include "Entity.h"

namespace Essence
{
	Body* World::CreateBody(Shape* shape, const BodyTypes bodyType, Entity* owner)
	{
		Body* body = bodies.Add(this, shape, bodyType);
		body->owner = owner;
		body->position = owner->GetPosition();

		return body;
	}

	void World::Remove(Body* body)
	{
		BodyNode* node = bodies.GetHead();

		do
		{
			if (&node->data == body)
			{
				bodies.Remove(node);

				return;
			}

			node = node->next;
		}
		while (node != nullptr);
	}

	void World::Step(const float dt) const
	{
		if (bodies.IsEmpty())
		{
			return;
		}

		for (BodyNode* node = bodies.GetHead(); node != nullptr; node = node->next)
		{
			Body& body = node->data;

			if (body.bodyType == BodyTypes::Static)
			{
				continue;
			}

			if (body.affectedbyGravity)
			{
				body.velocity.y += gravity * dt;
			}

			body.position += body.velocity * dt;
		}

		if (bodies.GetCount() == 1)
		{
			return;
		}

		for (BodyNode* node1 = bodies.GetHead(); node1 != nullptr; node1 = node1->next)
		{
			Body& body1 = node1->data;

			// For the time being, dynamic-dynamic collisions are ignored.
			if (body1.bodyType == BodyTypes::Static)
			{
				continue;
			}

			for (BodyNode* node2 = node1->next; node2 != nullptr; node2 = node2->next)
			{
				Body& body2 = node2->data;

				if (body2.bodyType == BodyTypes::Dynamic)
				{
					continue;
				}

				helper.Compare(body1, body2);
			}
		}

		for (BodyNode* node = bodies.GetHead(); node != nullptr; node = node->next)
		{
			Body& body = node->data;

			if (body.bodyType == BodyTypes::Dynamic)
			{
				body.owner->SetPosition(body.position);
			}
		}
	}
}
