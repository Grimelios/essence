#include "Camera.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Resolution.h"
#include "Entity.h"

namespace Essence
{
	const glm::mat4& Camera::GetView() const
	{
		return view;
	}

	void Camera::Update(const float dt)
	{
		if (mode == CameraModes::Follow)
		{
			// It's assume that if the camera is in follow mode, it has a valid target assigned.
			const glm::vec2& targetPosition = target->GetPosition();

			// Flooring to integers reduces visible jitter while the target moves.
			position.x = std::floor(targetPosition.x);
			position.y = std::floor(targetPosition.y);
		}

		glm::vec3 translation(glm::vec2(origin) - position, 0);
		translation.x /= Resolution::Width / 2;
		translation.y /= Resolution::Height / 2;

		const glm::mat4 matrix = translate(glm::mat4(1), translation);

		view = matrix;
	}
}
