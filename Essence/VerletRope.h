#pragma once
#include "IDynamic.h"
#include <vector>
#include "VerletPoint.h"
#include "AxisBox.h"
#include <optional>

namespace Essence
{
	class Bounds;
	class VerletRope : public IDynamic
	{
	private:

		std::vector<VerletPoint> points;
		std::vector<glm::vec2> offsets;

		std::optional<AxisBox> box = std::nullopt;

		int segmentLength = 0;
		bool isAscendable;

	public:

		// This constructor is useful when segment data needs to be computed.
		explicit VerletRope(bool isAscendable);
		VerletRope(int segmentCount, int segmentLength, bool isAscendable);

		std::vector<VerletPoint>& GetVerletPoints();
		std::vector<glm::vec2> GetPoints() const;
		
		Bounds GetBounds() const;

		void SetSegmentData(int segmentCount, int segmentLength);
		void Update(float dt) override;
	};
}
