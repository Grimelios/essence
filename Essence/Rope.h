#pragma once
#include "Entity.h"
#include "IAscendable.h"
#include "VerletRope.h"

namespace Essence
{
	class Rope : public Entity, public IAscendable
	{
	private:

		VerletRope rope;

	public:

		explicit Rope(int length);

		std::vector<glm::vec2> GetPoints() const override;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
