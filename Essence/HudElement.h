#pragma once
#include "Container2D.h"
#include "Alignments.h"

namespace Essence
{
	enum class HudElementTypes
	{
		LocationDisplay,
		PlayerHealthDisplay,
		ZoneDisplay
	};

	class HudElement : public Container2D
	{
	private:

		int offsetX;
		int offsetY;

		Alignments alignment;

	public:

		HudElement(int offsetX, int offsetY, Alignments alignment);
		virtual ~HudElement() = 0;

		bool visible = false;

		int GetOffsetX() const;
		int GetOffsetY() const;

		Alignments GetAlignment() const;
	};

	inline HudElement::~HudElement() = default;
}
