#include "Space.h"
#include "Shape.h"

namespace Essence
{
	Sensor* Space::CreateSensor(Shape* shape, void* owner, SensorTypes sensorType)
	{
		return sensors.Add(shape, this, owner, sensorType);
	}

	void Space::Remove(Sensor* sensor)
	{
		for (SensorNode* node = sensors.GetHead(); node != nullptr; node = node->next)
		{
			if (&node->data == sensor)
			{
				sensors.Remove(node);

				return;
			}
		}
	}

	void Space::Advance() const
	{
		/*
		for (SensorNode* node1 = sensors.GetHead(); node1 != nullptr; node1 = node1->next)
		{
			Sensor& s1 = node1->data;

			for (SensorNode* node2 = node1->next; node2 != nullptr; node2 = node2->next)
			{
				Sensor& s2 = node2->data;
				Sensor::ContactList& contacts = s1.contactList;

				const bool intersects = s1.GetShape()->Intersects(s2.GetShape());

				if (contacts.GetCount() > 0)
				{
					Sensor::ContactNode* node = ListUtilities::Contains(contacts, &s2);

					if (node != nullptr && !intersects)
					{
						contacts.Remove(node);
						s2.contactList.Remove(&s1);

						ISensitive* owner1 = s1.GetOwner();
						ISensitive* owner2 = s2.GetOwner();

						owner1->OnSeparation(owner2);
						owner2->OnSeparation(owner1);
					}
				}
				else if (intersects)
				{
					s1.contactList.Add(&s2);
					s2.contactList.Add(&s1);

					ISensitive* owner1 = s1.GetOwner();
					ISensitive* owner2 = s2.GetOwner();

					owner1->OnSense(owner2);
					owner2->OnSense(owner1);
				}
			}
		}
		*/
	}
}
