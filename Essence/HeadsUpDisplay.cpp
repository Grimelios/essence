#include "HeadsUpDisplay.h"
#include "JsonUtilities.h"
#include "ZoneDisplay.h"
#include "Resolution.h"
#include "LocationDisplay.h"
#include "PlayerHealthDisplay.h"

namespace Essence
{
	HeadsUpDisplay::HeadsUpDisplay()
	{
		std::vector<Json> blocks = JsonUtilities::Load("Hud.json");

		for (const Json& j : blocks)
		{
			const std::string type = j.at("Type").get<std::string>();
			const int offsetX = j.at("OffsetX").get<int>();
			const int offsetY = j.at("OffsetY").get<int>();
			const Alignments alignment = j.at("Alignment").get<Alignments>();

			elements.push_back(CreateElement(type, offsetX, offsetY, alignment));
		}

		OnResize();
	}

	std::unique_ptr<HudElement> HeadsUpDisplay::CreateElement(const std::string& type, const int offsetX,
		const int offsetY, const Alignments alignment) const
	{
		switch (type[0])
		{
			//case 'L': return std::make_unique<LocationDisplay>(offsetX, offsetY, alignment);
			case 'P': return std::make_unique<PlayerHealthDisplay>(offsetX, offsetY, alignment);
			case 'Z': return std::make_unique<ZoneDisplay>(offsetX, offsetY, alignment);
		}

		return nullptr;
	}

	void HeadsUpDisplay::OnResize()
	{
		const int width = Resolution::Width;
		const int height = Resolution::Height;

		for (auto& element : elements)
		{
			const int alignment = static_cast<int>(element->GetAlignment());
			const int offsetX = element->GetOffsetX();
			const int offsetY = element->GetOffsetY();

			const bool left = (alignment & static_cast<int>(Alignments::Left)) > 0;
			const bool right = (alignment & static_cast<int>(Alignments::Right)) > 0;
			const bool top = (alignment & static_cast<int>(Alignments::Top)) > 0;
			const bool bottom = (alignment & static_cast<int>(Alignments::Bottom)) > 0;

			const int x = left ? offsetX : (right ? width - offsetX : width / 2 + offsetX);
			const int y = top ? offsetY : (bottom ? height - offsetY : height / 2 + offsetY);

			element->SetLocation(glm::ivec2(x, y));
		}
	}

	void HeadsUpDisplay::Update(const float dt)
	{
		for (auto& element : elements)
		{
			if (element->visible)
			{
				element->Update(dt);
			}
		}
	}

	void HeadsUpDisplay::Draw(SpriteBatch& sb)
	{
		for (auto& element : elements)
		{
			if (element->visible)
			{
				element->Draw(sb);
			}
		}
	}
}
