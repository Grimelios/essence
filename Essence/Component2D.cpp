#include "Component2D.h"

namespace Essence
{
	Component2D::Component2D(const Alignments alignment) : alignment(alignment)
	{
	}

	const glm::vec2& Component2D::GetPosition() const
	{
		return position;
	}

	const glm::vec2& Component2D::GetScale() const
	{
		return scale;
	}

	const Color& Component2D::GetColor() const
	{
		return color;
	}

	SpriteModifiers Component2D::GetMods() const
	{
		return mods;
	}

	float Component2D::GetRotation() const
	{
		return rotation;
	}

	void Component2D::SetPosition(const glm::vec2& position)
	{
		this->position = position;

		positionChanged = true;
	}

	void Component2D::SetScale(const glm::vec2& scale)
	{
		this->scale = scale;

		positionChanged = true;
	}

	void Component2D::SetColor(const Color& color)
	{
		this->color = color;

		colorChanged = true;
	}

	void Component2D::SetRotation(const float rotation)
	{
		this->rotation = rotation;

		positionChanged = true;
	}

	void Component2D::SetOpacity(const int opacity)
	{
		color.a = static_cast<std::byte>(opacity);
		colorChanged = true;
	}

	void Component2D::SetMods(const SpriteModifiers mods)
	{
		this->mods = mods;

		positionChanged = true;
	}
}
