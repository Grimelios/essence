#pragma once

namespace Essence
{
	class IBouncy
	{
	public:

		virtual ~IBouncy() = default;
		virtual int GetBounceSpeed() const = 0;
	};
}
