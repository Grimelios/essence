#include "Localizer.h"
#include "ILocalizable.h"
#include "JsonUtilities.h"
#include "MapUtilities.h"

namespace Essence
{
	int Localizer::language = static_cast<int>(SupportedLanguages::English);
	Localizer::TargetList Localizer::targetList;
	Localizer::EntryMap Localizer::entryMap;

	std::string Localizer::Register(ILocalizable* target, const std::string& key)
	{
		if (entryMap.empty())
		{
			std::map<std::string, std::vector<std::string>> rawMap = JsonUtilities::Load("Localization/Strings.json");

			for (const auto& pair : rawMap)
			{
				Entry entry;
				const auto& v = pair.second;

				// See https://stackoverflow.com/questions/21276889/copy-stdvector-into-stdarray.
				std::copy_n(std::make_move_iterator(v.begin()), static_cast<int>(SupportedLanguages::Count),
					entry.begin());

				MapUtilities::Add(entryMap, pair.first, entry);
			}
		}

		targetList.Add(std::make_pair(target, key));

		return entryMap.at(key)[language];
	}

	void Localizer::Unregister(ILocalizable* target)
	{
		TargetNode* node = targetList.GetHead();

		while (node != nullptr)
		{
			if (node->data.first == target)
			{
				targetList.Remove(node);

				return;
			}

			node = node->next;
		}
	}

	void Localizer::SetLanguage(const SupportedLanguages language)
	{
		Localizer::language = static_cast<int>(language);

		if (entryMap.empty())
		{
			return;
		}

		TargetNode* node = targetList.GetHead();

		while (node != nullptr)
		{
			const TargetPair& pair = node->data;

			pair.first->OnLocalize(pair.second, entryMap.at(pair.second)[Localizer::language]);
			node = node->next;
		}
	}
}
