#pragma once
#include "Sprite.h"
#include "JumperData.h"
#include <glm/vec2.hpp>
#include "ComplexEntity.h"

namespace Essence
{
	class Player;
	class Jumper : public ComplexEntity
	{
	private:

		static JumperData data;

		Player* player = nullptr;
		Sprite sprite;

		void OnPlayerJump(float speed);
		void Jump();

	protected:

		bool OnCollision(const glm::vec2& normal, Entity* entity) override;

	public:

		Jumper();

		void Initialize(Scene* scene, EntityLayer* layer) override;
	};
}
