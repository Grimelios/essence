#pragma once
#include <glad.h>
#include <vector>

namespace Essence::GLUtilities
{
	template<class T>
	void BufferData(const GLuint buffer, std::vector<T> data)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(T), &data[0], GL_STATIC_DRAW);
	}
}
