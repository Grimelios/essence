#pragma once
#include <glm/vec2.hpp>
#include "IDisposable.h"
#include <functional>
#include <optional>

namespace Essence
{
	enum class BodyTypes
	{
		Dynamic,
		Static
	};

	class World;
	class Shape;
	class Entity;
	class Body : public IDisposable
	{
	private:

		using CollisionFunction = std::function<bool(const glm::vec2& normal, Entity* entity)>;

		World* world;
		Shape* shape;

	public:

		Body(World* world, Shape* shape, BodyTypes bodyType);

		glm::vec2 position = glm::vec2(0);
		glm::vec2 velocity = glm::vec2(0);

		Shape* GetShape() const;
		BodyTypes bodyType;

		std::optional<CollisionFunction> onCollision = std::nullopt;

		float rotation = 0;
		float restitution = 0;
		float friction = 0;

		bool affectedbyGravity = true;

		Entity* owner = nullptr;

		void Dispose() override;
	};
}
