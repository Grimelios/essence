#pragma once
#include <string>

namespace Essence
{
	class SaveData
	{
	private:

		std::string name;

	public:

		const std::string& GetName() const;
	};
}
