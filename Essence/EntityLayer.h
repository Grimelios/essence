#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <memory>
#include "Entity.h"
#include "DoublyLinkedList.h"

namespace Essence
{
	class Scene;
	class EntityLayer : public IDynamic, public IRenderable
	{
	private:

		using EntityPointer = std::unique_ptr<Entity>;
		using EntityList = DoublyLinkedList<EntityPointer>;
		using EntityNode = DoublyLinkedListNode<EntityPointer>;
		using EntityArray = std::array<EntityList, static_cast<int>(EntityGroups::Unassigned)>;

		std::vector<EntityGroups> updateOrder;
		std::vector<EntityGroups> drawOrder;

		EntityArray entities;
		Scene* parent;

		// This function is used for both fragment loading and on-the-fly entity creation. Note that the return value
		// isn't used for fragment loading.
		Entity* AddInternal(EntityPointer& e);

	public:

		EntityLayer(Scene* parent, std::vector<EntityGroups> updateOrder, std::vector<EntityGroups> drawOrder);

		// Note that this function isn't used when loading fragments from Json files. It's only used for on-the-fly
		// entity creation.
		template<class T, class... Args>
		T* Add(Args&&... args);

		EntityList& GetEntities(EntityGroups group);

		void Add(EntityPointer& entity);
		void Remove(Entity* entity);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T, class... Args>
	T* EntityLayer::Add(Args&&... args)
	{
		// Nodes store unique pointers to generic entities. As such, the pointer passed into the node constructor must
		// be explicitly marked as std::unique_ptr<Entity> rather than std::unique_ptr<T>.
		EntityPointer e = std::make_unique<T>(args...);

		return static_cast<T*>(AddInternal(e));
	}
}
