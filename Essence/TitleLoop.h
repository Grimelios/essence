#pragma once
#include "GameLoop.h"
#include "MenuManager.h"
#include "PressStart.h"

namespace Essence
{
	class ScreenManager;
	class TitleLoop : public GameLoop
	{
	private:

		PressStart pressStart;
		MenuManager menuManager;
		ScreenManager& screenManager;

	public:

		TitleLoop(Camera& camera, ScreenManager& screenManager, SpriteBatch& sb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
