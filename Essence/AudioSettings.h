#pragma once

namespace Essence
{
	class AudioSettings
	{
	public:

		int masterVolume;
		int musicVolume;
		int voiceVolume;
		int sfxVolume;

		bool masterMuted;
		bool musicMuted;
		bool voiceMuted;
		bool sfxMuted;
	};
}
