#pragma once
#include <nlohmann/json.hpp>

namespace Essence::JsonUtilities
{
	nlohmann::json Load(const std::string& filename);
}
