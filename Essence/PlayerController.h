#pragma once
#include "PlayerControls.h"
#include "PlayerSkills.h"

namespace Essence
{
	class Player;
	class PlayerData;
	class AggregateData;
	class PlayerController
	{
	private:

		// These values are copied directly from the Player class.
		static const int RunIndex = static_cast<int>(PlayerSkills::Run);
		static const int JumpIndex = static_cast<int>(PlayerSkills::Jump);
		static const int AscendIndex = static_cast<int>(PlayerSkills::Ascend);
		static const int DivebombIndex = static_cast<int>(PlayerSkills::Divebomb);
		static const int DoubleJumpIndex = static_cast<int>(PlayerSkills::DoubleJump);

		Player& player;
		PlayerData& playerData;
		PlayerControls controls;

		void ProcessInput(const AggregateData& data, float dt);
		void ProcessRunning(const AggregateData& data, float dt) const;
		void ProcessJumping(const AggregateData& data) const;
		void Jump() const;
		void LimitJump() const;
		void Ascend() const;

	public:

		PlayerController(Player& player, PlayerData& playerDAta);
	};
}
