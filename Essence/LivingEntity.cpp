#include "LivingEntity.h"

namespace Essence
{
	LivingEntity::LivingEntity(const EntityGroups group) : ComplexEntity(group)
	{
	}

	void LivingEntity::ApplyDamage(const int damage)
	{
		health -= damage;
		
		if (health <= 0)
		{
			health = 0;
			OnDeath();
		}
	}

	void LivingEntity::OnDeath()
	{
	}

	void LivingEntity::Kill()
	{
		health = 0;
		OnDeath();
	}
}
