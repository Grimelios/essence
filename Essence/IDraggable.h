#pragma once
#include "IClickable.h"
#include <glm/vec2.hpp>

namespace Essence
{
	class IDraggable : public IClickable
	{
	public:

		virtual ~IDraggable() = default;
		virtual glm::vec2 GetPosition() const = 0;
		virtual glm::vec2 ConstrainDrag(const glm::vec2& mousePosition) const = 0;
	};
}
