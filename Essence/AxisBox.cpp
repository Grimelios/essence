#include "AxisBox.h"

namespace Essence
{
	AxisBox::AxisBox() : AxisBox(glm::vec2(0), 0, 0)
	{
	}

	AxisBox::AxisBox(const float size) : AxisBox(glm::vec2(0), size, size)
	{
	}

	AxisBox::AxisBox(const int width, const int height) :
		AxisBox(glm::vec2(0), static_cast<float>(width), static_cast<float>(height))
	{
	}

	AxisBox::AxisBox(const int x, const int y, const int width, const int height) :
		AxisBox(glm::vec2(x, y), static_cast<float>(width), static_cast<float>(height))
	{
		
	}

	AxisBox::AxisBox(const float width, const float height) : AxisBox(glm::vec2(0), width, height)
	{
	}

	AxisBox::AxisBox(const float x, const float y, const float width, const float height) :
		AxisBox(glm::vec2(x + width / 2, y + height / 2), width, height)
	{
	}

	AxisBox::AxisBox(const glm::vec2& center, const float width, const float height) :
		Shape(center, 0, ShapeTypes::AxisBox),

		halfWidth(width / 2),
		halfHeight(height / 2)
	{
	}

	float AxisBox::GetWidth() const
	{
		return halfWidth * 2;
	}

	float AxisBox::GetHeight() const
	{
		return halfHeight * 2;
	}

	float AxisBox::GetHalfWidth() const
	{
		return halfWidth;
	}

	float AxisBox::GetHalfHeight() const
	{
		return halfHeight;
	}

	void AxisBox::SetWidth(const float width)
	{
		halfWidth = width / 2;
	}

	void AxisBox::SetHeight(const float height)
	{
		halfHeight = height / 2;
	}

	Bounds AxisBox::ToBounds() const
	{
		const int iHalfWidth = static_cast<int>(halfWidth);
		const int iHalfHeight = static_cast<int>(halfHeight);

		return Bounds(static_cast<int>(position.x) - iHalfWidth, static_cast<int>(position.y) - iHalfHeight,
			iHalfWidth * 2, iHalfHeight * 2);
	}
}
