#include "PlayerControls.h"
#include "JsonUtilities.h"

namespace Essence
{
	PlayerControls::PlayerControls()
	{
		const Json j = JsonUtilities::Load("Controls.json").at("Player");

		runLeft = ParseBinds(j, "RunLeft");
		runRight = ParseBinds(j, "RunRight");
		jump = ParseBinds(j, "Jump");
		ascend = ParseBinds(j, "Ascend");
		divebomb = ParseBinds(j, "Divebomb");
		interact = ParseBinds(j, "Interact");
		attack = ParseBinds(j, "Attack");
	}
}
