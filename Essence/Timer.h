#pragma once
#include "IDynamic.h"
#include <functional>
#include <optional>

namespace Essence
{
	class TimerCollection;
	class Timer : public IDynamic
	{
	private:

		using TickFunction = std::function<void(float)>;

	protected:

		bool completed = false;

		explicit Timer(int duration, float elapsed = 0);

	public:

		float duration;
		float elapsed;

		std::optional<TickFunction> tick = std::nullopt;

		bool paused = false;
		bool HasCompleted() const;
	};
}
