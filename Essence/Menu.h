#pragma once
#include "Container2D.h"
#include "MenuItem.h"
#include "ClickableSet.h"
#include "MenuControls.h"

namespace Essence
{
	class AggregateData;
	class Menu : public Container2D
	{
	protected:

		// Making these aliases protected allows derived menus to more easily create their item lists.
		using ItemPointer = std::unique_ptr<MenuItem>;
		using ItemList = std::vector<ItemPointer>;

	private:

		static MenuControls controls;

		int selectedIndex = -1;
		int spacing;

		ItemList items;
		ClickableSet itemSet;

	protected:

		explicit Menu(int spacing);

		// The item list must be passed by reference since unique pointers can't be copied.
		void Initialize(ItemList& items, int selectedIndex);

	public:

		virtual void Submit(int index) = 0;

		void SetLocation(const glm::ivec2& location) override;
		void SetSelectedIndex(int index);
		void ProcessInput(const AggregateData& data);
	};
}
