#pragma once

namespace Essence
{
	enum class ShaderTypes
	{
		Vertex,
		TesselationControl,
		TesselationEvaluation,
		Geometry,
		Fragment
	};
}
