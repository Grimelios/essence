#pragma once
#include "Timer.h"
#include "DoublyLinkedList.h"

namespace Essence
{
	class TimerCollection : public IDynamic
	{
	private:

		DoublyLinkedList<std::unique_ptr<Timer>> timers;

	public:

		template<class T, class... Args>
		T* CreateTimer(Args&&... args);

		void Update(float dt) override;
	};

	template<class T, class... Args>
	T* TimerCollection::CreateTimer(Args&&... args)
	{
		std::unique_ptr<Timer> timer = std::make_unique<T>(args...);

		return static_cast<T*>(timers.Add(std::move(timer))->get());
	}
}
