#pragma once
#include "Sensor.h"
#include "DoublyLinkedList.h"

namespace Essence
{
	class Shape;
	class Space
	{
	private:

		using SensorList = DoublyLinkedList<Sensor>;
		using SensorNode = DoublyLinkedListNode<Sensor>;

		SensorList sensors;

	public:

		Sensor* CreateSensor(Shape* shape, void* owner, SensorTypes sensorType);

		void Remove(Sensor* sensor);
		void Advance() const;
	};
}
