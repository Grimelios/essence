#include "Shape.h"
#include <glm/vec2.hpp>

namespace Essence
{
	ShapeHelper Shape::helper;

	Shape::Shape(const ShapeTypes shapeType) : Shape(glm::vec2(0), 0, shapeType)
	{
	}

	Shape::Shape(const float rotation, const ShapeTypes shapeType) : Shape(glm::vec2(0), rotation, shapeType)
	{
	}

	Shape::Shape(const glm::vec2& position, const float rotation, const ShapeTypes shapeType) :
		shapeType(shapeType),
		rotation(rotation),
		position(position)
	{
	}

	ShapeTypes Shape::GetShapeType() const
	{
		return shapeType;
	}

	float Shape::GetRotation() const
	{
		return rotation;
	}

	void Shape::SetRotation(const float rotation)
	{
		this->rotation = rotation;
	}

	bool Shape::Intersects(const Shape* other) const
	{
		return helper.CheckIntersection(this, other);
	}
}
