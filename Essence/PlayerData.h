#pragma once

namespace Essence
{
	class PlayerData
	{
	public:

		PlayerData();

		float acceleration;
		float deceleration;
		float maxSpeed;
		float jumpSpeedInitial;
		float jumpSpeedLimited;
		float jumpDeceleration;
	};
}
