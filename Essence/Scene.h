#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include <map>
#include "EntityLayer.h"

namespace Essence
{
	class World;
	class Space;
	class Camera;
	class Scene : public IDynamic, public IRenderable
	{
	private:

		using Json = nlohmann::json;
		using GroupMap = std::map<std::string, EntityGroups>;

		static std::vector<EntityGroups> ParseOrderingVector(const Json& j, const GroupMap& map);

		World* world;
		Space* space;
		Camera* camera;

		std::vector<EntityLayer> layers;

	public:

		Scene(World* world, Space* space, Camera* camera);

		// Note that this function isn't used when loading fragments from Json files. It's only used for on-the-fly
		// entity creation.
		template<class T, class... Args>
		T* Add(int layer, Args&&... args);

		World* GetWorld() const;
		Space* GetSpace() const;
		Camera* GetCamera() const;

		void LoadFragment(const std::string& filename);
		void Remove(int layer, Entity* entity);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T, class... Args>
	T* Scene::Add(const int layer, Args&&... args)
	{
		return layers[layer].Add<T>(args...);
	}
}
