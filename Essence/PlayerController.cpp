#include "PlayerController.h"
#include "Player.h"
#include "AggregateData.h"
#include "Messaging.h"
#include "Body.h"
#include "GameFunctions.h"

namespace Essence
{
	PlayerController::PlayerController(Player& player, PlayerData& playerData) :
		player(player),
		playerData(playerData)
	{
		Messaging::Subscribe(MessageTypes::Input, [this](const std::any& data, const float dt)
		{
			ProcessInput(std::any_cast<AggregateData>(data), dt);
		});
	}

	void PlayerController::ProcessInput(const AggregateData& data, const float dt)
	{
		ProcessRunning(data, dt);
		ProcessJumping(data);
	}

	void PlayerController::ProcessRunning(const AggregateData& data, const float dt) const
	{
		const bool left = data.Query(controls.runLeft, InputStates::Held);
		const bool right = data.Query(controls.runRight, InputStates::Held);

		Body* body = player.body;
		glm::vec2& velocity = body->velocity;

		if (left ^ right)
		{
			velocity.x += playerData.acceleration * dt * (left ? -1 : 1);
			velocity.x = std::clamp(velocity.x, -playerData.maxSpeed, playerData.maxSpeed);
		}
		else if (velocity.x != 0)
		{
			const int oldSign = GameFunctions::ComputeSign(velocity.x);

			velocity.x -= playerData.deceleration * dt * oldSign;

			if (GameFunctions::ComputeSign(velocity.x) != oldSign)
			{
				velocity.x = 0;
			}
		}
	}

	void PlayerController::ProcessJumping(const AggregateData& data) const
	{
		auto& jumpBindUsed = player.jumpBindUsed;

		if (player.jumpActive && player.body->velocity.y < -playerData.jumpSpeedLimited &&
			data.Query(player.jumpBindUsed.value(),	InputStates::ReleasedThisFrame))
		{
			LimitJump();
		}
		else if (player.skillsEnabled[JumpIndex])
		{
			jumpBindUsed = data.QueryBind(controls.jump, InputStates::PressedThisFrame);

			if (jumpBindUsed.has_value())
			{
				Jump();
			}
		}
	}

	void PlayerController::Jump() const
	{
		player.body->velocity.y = -playerData.jumpSpeedInitial;
		player.onGround = false;
		player.jumpsRemaining--;
		player.jumpActive = true;
		player.skillsEnabled[JumpIndex] = false;
		player.onJump.Invoke(-playerData.jumpSpeedInitial);
	}

	void PlayerController::LimitJump() const
	{
		player.jumpActive = false;
		player.jumpDecelerating = true;
	}

	void PlayerController::Ascend() const
	{
	}
}
