#include "SpriteGroup.h"

namespace Essence
{
	void SpriteGroup::Clear()
	{
		vertices.clear();
		texCoords.clear();
		quadCoords.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
