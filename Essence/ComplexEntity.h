#pragma once
#include <glm/vec2.hpp>
#include "nlohmann/json.hpp"
#include "Shape.h"
#include "TimerCollection.h"
#include "Entity.h"

namespace Essence
{
	template<class T>
	class DoublyLinkedListNode;

	class Body;
	class World;
	class Space;
	class Sensor;
	class ComplexEntity : public Entity
	{
	protected:

		explicit ComplexEntity(EntityGroups group);

		bool usesBody = true;
		bool usesSensor = true;
		bool onGround = false;

		Body* body = nullptr;
		Sensor* sensor = nullptr;

		std::unique_ptr<Shape> shape = nullptr;

		TimerCollection timers;

		virtual bool OnCollision(const glm::vec2& normal, Entity* entity);

	public:

		virtual ~ComplexEntity() = 0;

		void Initialize(Scene* scene, EntityLayer* layer) override;
		void SetPosition(const glm::vec2& position) override;
		void SetRotation(float rotation) override;
		void Dispose() override;
		void Update(float dt) override;
	};

	inline ComplexEntity::~ComplexEntity() = default;
}
