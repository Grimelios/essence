#pragma once

namespace Essence
{
	enum class EntityGroups
	{
		Player,
		Enemies,
		Creatures,
		Characters,
		Objects,
		World,
		Unassigned = 6
	};
}
