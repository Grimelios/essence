#pragma once
#include "Shape.h"
#include "LineRestrictions.h"

namespace Essence
{
	class Line : public Shape
	{
	private:

		float length;

		glm::vec2 halfVector;

		void RecomputeHalfVector();

	public:

		Line();
		explicit Line(float length);
		Line(float length, float rotation);
		Line(const glm::vec2& start, const glm::vec2& end);

		const glm::vec2& GetHalfVector() const;

		float GetLength() const;
		float GetHalfLength() const;

		void SetLength(float length);
		void SetRotation(float rotation) override;
		void SetEndpoints(const glm::vec2& start, const glm::vec2& end);

		LineRestrictions restriction = LineRestrictions::None;
	};
}
