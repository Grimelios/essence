#include "PhysicsHelper.h"
#include "Body.h"
#include "AxisBox.h"
#include "GameFunctions.h"

namespace Essence
{
	void PhysicsHelper::Compare(Body& body1, Body& body2) const
	{
		AxisBox& box1 = *static_cast<AxisBox*>(body1.GetShape());
		AxisBox& box2 = *static_cast<AxisBox*>(body2.GetShape());

		Resolve(body1, body2, box1, box2);
	}

	void PhysicsHelper::Resolve(Body& body1, Body& body2, AxisBox& box1, AxisBox& box2) const
	{
		const float dX = body1.position.x - body2.position.x;
		const float dY = body1.position.y - body2.position.y;
		const float sumWidth = static_cast<float>(box1.GetWidth() + box2.GetWidth());
		const float sumHeight = static_cast<float>(box1.GetHeight() + box2.GetHeight());

		float overlapX = sumWidth / 2 - std::abs(dX);
		float overlapY = sumHeight / 2 - std::abs(dY);

		if (overlapX > 0 && overlapY > 0)
		{
			glm::vec2& velocity = body1.velocity;

			int xSign = GameFunctions::ComputeSign(dX);
			int ySign = GameFunctions::ComputeSign(dY);

			if (GameFunctions::ComputeSign(velocity.x * xSign) == 1)
			{
				overlapX = sumWidth - overlapX;
				xSign *= -1;
			}

			if (GameFunctions::ComputeSign(velocity.y * ySign) == 1)
			{
				overlapY = sumHeight - overlapY;
				ySign *= -1;
			}

			float weightX = 0;
			float weightY = 0;

			bool xZeroFlag;
			bool yZeroFlag;

			if (velocity.x == 0)
			{
				xZeroFlag = true;
			}
			else
			{
				xZeroFlag = false;
				weightX = overlapX / std::abs(velocity.x);
			}

			if (velocity.y == 0)
			{
				yZeroFlag = true;
			}
			else
			{
				yZeroFlag = false;
				weightY = overlapY / std::abs(velocity.y);
			}

			glm::vec2 normal = glm::vec2(0);

			if (yZeroFlag || (!xZeroFlag && weightX < weightY))
			{
				normal.x = static_cast<float>(xSign);
				velocity.x = 0;
			}
			else
			{
				normal.y = static_cast<float>(ySign);
				velocity.y = 0;
			}

			// Returning false negates the collision.
			if (body1.onCollision->operator()(normal, body2.owner))
			{
				body1.position += glm::vec2(normal.x * overlapX, normal.y * overlapY);
			}
		}
	}
}
