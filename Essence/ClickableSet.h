#pragma once
#include <vector>
#include "IClickable.h"

namespace Essence
{
	class MouseData;
	class ClickableSet
	{
	private:

		using ItemList = std::vector<IClickable*>;

		bool itemHoveredThisFrame = false;
		bool itemClickedThisFrame = false;

	public:

		ItemList items;

		// This variable is public so that it can be set externally when items are changed through other input methods
		// (like the keyboard).
		IClickable* hoveredItem = nullptr;

		// These functions are useful when managing multiple input devices (since the mouse will usually override other sources).
		bool WasItemHoveredThisFrame() const;
		bool WasItemClickedThisFrame() const;

		void ProcessMouse(const MouseData& data);
	};
}
