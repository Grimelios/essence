#pragma once
#include "GameLoop.h"
#include "Scene.h"
#include "HeadsUpDisplay.h"
#include "PhysicsTester.h"
#include "Letterbox.h"
#include "World.h"
#include "Space.h"
#include "PhysicsAccumulator.h"
#include "ScreenManager.h"
#include "RopeTester.h"
#include "CurveTester.h"

namespace Essence
{
	class GameplayLoop : public GameLoop
	{
	private:

		Scene scene;
		World world;
		Space space;
		Letterbox letterbox;
		RopeTester ropeTester;
		CurveTester curveTester;
		ScreenManager screenManager;
		PhysicsTester physicsTester;
		PhysicsAccumulator accumulator;
		HeadsUpDisplay headsUpDisplay;

	public:

		GameplayLoop(Camera& camera, SpriteBatch& sb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}

