#include "CurveTester.h"
#include "Messaging.h"
#include "KeyboardData.h"
#include <glfw3.h>
#include <glm/vec4.hpp>

namespace Essence
{
	CurveTester::CurveTester() :
		sprite("Ori.png", Alignments::Left | Alignments::Top),
		spriteText("Debug")
	{
		controlPoints = &curve.GetControlPoints();
		controlPoints->emplace_back(30);
		controlPoints->emplace_back(100, 400);
		controlPoints->emplace_back(500, 350);
		controlPoints->emplace_back(600, 200);
		controlPoints->emplace_back(700, 175);
		controlPoints->emplace_back(650, 500);
		controlPoints->emplace_back(350, 450);
		controlPoints->emplace_back(300, 460);
		controlPoints->emplace_back(280, 500);
		controlPoints->emplace_back(300, 540);
		controlPoints->emplace_back(330, 550);
		controlPoints->emplace_back(370, 530);

		sprite.SetPosition(200, 20);

		spriteText.SetPosition(400, 60);
		spriteText.SetColor(255, 255, 0, 255);

		Messaging::Subscribe(MessageTypes::Keyboard, [this](const std::any& data, const float dt)
		{
			const KeyboardData& kbData = std::any_cast<KeyboardData>(data);

			if (kbData.Query(GLFW_KEY_UP, InputStates::PressedThisFrame))
			{
				segmentCount = segmentCount == 1 ? 5 : segmentCount + 5;
			}
			else if (kbData.Query(GLFW_KEY_DOWN, InputStates::PressedThisFrame))
			{
				segmentCount = segmentCount <= 5 ? 1 : segmentCount - 5;
			}
		});
	}

	void CurveTester::Update(const float dt)
	{
	}

	void CurveTester::Draw(SpriteBatch& sb)
	{
		//sb.DrawLines(*controlPoints, glm::vec4(0.4f, 0.4f, 0.4f, 1));
		//sb.DrawLines(curve.ComputePoints(segmentCount), glm::vec4(0, 1, 1, 1));

		sprite.Draw(sb);
		spriteText.SetValue("Segment count: " + std::to_string(segmentCount));
		spriteText.Draw(sb);
	}
}
