#include "TitleLoop.h"
#include "SpriteBatch.h"
#include "ScreenManager.h"

namespace Essence
{
	TitleLoop::TitleLoop(Camera& camera, ScreenManager& screenManager, SpriteBatch& sb) : GameLoop(camera, sb),
		pressStart(this),
		screenManager(screenManager)
	{
		screenManager.CreateScreens(true);
	}

	void TitleLoop::Initialize()
	{
	}

	void TitleLoop::Update(const float dt)
	{
		menuManager.Update(dt);

		Screen* activeScreen = screenManager.GetActiveScreen();

		if (activeScreen != nullptr)
		{
			activeScreen->Update(dt);
		}
	}

	void TitleLoop::Draw(SpriteBatch& sb)
	{
		menuManager.Draw(sb);

		Screen* activeScreen = screenManager.GetActiveScreen();

		if (activeScreen != nullptr)
		{
			activeScreen->Draw(sb);
		}
	}
}
