#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include <memory>
#include <nlohmann/json.hpp>
#include "HudElement.h"
#include "Alignments.h"

namespace Essence
{
	class HeadsUpDisplay : public IDynamic, public IRenderable
	{
	private:

		using Json = nlohmann::json;

		std::vector<std::unique_ptr<HudElement>> elements;

		std::unique_ptr<HudElement> CreateElement(const std::string& type, int offsetX,	int offsetY,
			Alignments alignment) const;

		void OnResize();

	public:

		HeadsUpDisplay();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
