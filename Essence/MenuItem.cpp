#include "MenuItem.h"
#include "Menu.h"

namespace Essence
{
	MenuItem::MenuItem(const int index, Menu* parent) :
		index(index),
		parent(parent)
	{
	}

	bool MenuItem::IsEnabled() const
	{
		return enabled;
	}

	void MenuItem::SetEnabled(const bool enabled)
	{
		this->enabled = enabled;
	}

	bool MenuItem::Contains(const glm::vec2& mousePosition)
	{
		return bounds.Contains(mousePosition);
	}

	void MenuItem::OnMouseMove(const glm::vec2& mousePosition)
	{
	}

	void MenuItem::OnHover(const glm::vec2& mousePosition)
	{
		parent->SetSelectedIndex(index);
	}

	void MenuItem::OnUnhover()
	{
	}

	void MenuItem::OnClick(const glm::vec2& mousePosition)
	{
		parent->Submit(index);
	}
}
