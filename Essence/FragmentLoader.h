#pragma once
#include <vector>
#include <string>
#include "Entity.h"
#include <nlohmann/json.hpp>

namespace Essence
{
	class FragmentLoader
	{
	private:

		using EntityPointer = std::unique_ptr<Entity>;
		using EntityList = std::vector<EntityPointer>;
		using Json = nlohmann::json;

		static EntityList CreateEntities(const Json& block);
		static EntityPointer CreateEntity(const Json& j);

	public:

		static EntityList Load(const std::string& filename);
	};
}
