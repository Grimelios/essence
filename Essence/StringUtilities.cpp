#include "StringUtilities.h"
#include <sstream>

namespace Essence
{
	std::vector<std::string> StringUtilities::Split(const std::string& s, const char delimeter, const bool removeEmpty)
	{
		std::stringstream stream(s);
		std::string token;
		std::vector<std::string> list;

		while (std::getline(stream, token, delimeter))
		{
			if (!removeEmpty || !token.empty())
			{
				list.push_back(std::move(token));
			}
		}

		return list;
	}

	std::string StringUtilities::RemoveExtension(const std::string& s)
	{
		return s.substr(0, LastIndexOf(s, '.'));
	}

	int StringUtilities::IndexOf(const std::string& s, const char c)
	{
		return static_cast<int>(s.find(c));
	}

	int StringUtilities::IndexOf(const std::string& s, const char c, const int start)
	{
		return static_cast<int>(s.find(c, start));
	}

	int StringUtilities::IndexOf(const std::string& s, const std::string& value)
	{
		return static_cast<int>(s.find(value));
	}

	int StringUtilities::IndexOf(const std::string& s, const std::string& value, const int start)
	{
		return static_cast<int>(s.find(value, start));
	}

	int StringUtilities::LastIndexOf(const std::string& s, const char c)
	{
		return static_cast<int>(s.find_last_of(c));
	}

	bool StringUtilities::EndsWith(const std::string& s, const std::string& end)
	{
		const int stringLength = static_cast<int>(s.length());
		const int endLength = static_cast<int>(end.length());

		// See https://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c.
		if (stringLength < endLength)
		{
			return false;
		}

		return s.compare(stringLength - endLength, endLength, end) == 0;
	}
}
