#pragma once

namespace Essence
{
	class ITargetable
	{
	public:

		virtual ~ITargetable() = default;
		virtual void ApplyDamage(int damage) = 0;
	};
}
