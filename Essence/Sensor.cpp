#include "Sensor.h"
#include "Space.h"

namespace Essence
{
	Sensor::Sensor(Shape* shape, Space* space, void* owner, const SensorTypes type) :
		shape(shape),
		space(space),
		owner(owner),
		sensorType(type)
	{
	}

	Shape* Sensor::GetShape() const
	{
		return shape;
	}

	void* Sensor::GetOwner() const
	{
		return owner;
	}

	SensorTypes Sensor::GetSensorType() const
	{
		return sensorType;
	}

	void Sensor::Dispose()
	{
		space->Remove(this);
	}
}
