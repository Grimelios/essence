#include "Blackout.h"
#include "Resolution.h"

namespace Essence
{
	Blackout::Blackout() :
		timer(500, [this](const float time)
		{
			visible = false;
			timer.elapsed = 0;
			timer.paused = true;
		}),

		fullscreenBounds(Resolution::Width, Resolution::Height)
	{
		timer.paused = true;
		timer.tick = [this](const float progress)
		{
			Tick(progress);
		};
	}

	void Blackout::Tick(const float progress)
	{
		color.a = progress;
	}

	void Blackout::Trigger()
	{
		visible = true;
		fadingOut = true;
		timer.paused = false;
	}

	void Blackout::Update(const float dt)
	{
		timer.Update(dt);
	}

	void Blackout::Draw(SpriteBatch& sb)
	{
		if (fadingOut)
		{
			//sb.FillBounds(fullscreenBounds, color);
		}
	}
}
