#include "Curve.h"
#include <valarray>

namespace Essence
{
	Curve::BinomialTerms Curve::binomialTerms =
	{
		// The number of lists initalized here is pretty arbitrary. Higher-order curves will compute new points on the
		// fly anyway.
		{1},
		{1,1},
		{1,2,1},
		{1,3,3,1},
		{1,4,6,4,1},
		{1,5,10,10,5,1},
		{1,6,15,20,15,6,1}
	};

	const Curve::BinomialList& Curve::GetTerms(const int n)
	{
		// See https://pomax.github.io/bezierinfo/.
		while (n >= static_cast<int>(binomialTerms.size()))
		{
			const int s = static_cast<int>(binomialTerms.size());

			BinomialList newList;
			newList.reserve(s + 1);
			newList.push_back(0);

			const BinomialList& previous = binomialTerms[s - 1];

			for (int i = 1; i < s; i++)
			{
				newList.push_back(previous[i - 1] + previous[i]);
			}

			newList.push_back(1);
			binomialTerms.push_back(std::move(newList));
		}
		
		return binomialTerms[n];
	}

	Curve::Curve(std::vector<glm::vec2> controlPoints) : controlPoints(std::move(controlPoints))
	{
	}

	std::vector<glm::vec2>& Curve::GetControlPoints()
	{
		return controlPoints;
	}

	std::vector<glm::vec2> Curve::ComputePoints(const int segmentCount) const
	{
		// This uses De Casteljau's algorithm (see https://pomax.github.io/bezierinfo/).
		std::vector<glm::vec2> points;
		points.reserve(segmentCount + 1);
		points.push_back(controlPoints[0]);

		const float dt = 1.0f / static_cast<float>(segmentCount);

		const BinomialList& binomials = GetTerms(static_cast<int>(controlPoints.size()) - 1);

		for (int i = 1; i <= segmentCount - 1; i++)
		{
			points.push_back(ComputePoint(dt * i, binomials));
		}

		points.push_back(controlPoints.back());

		return points;
	}

	glm::vec2 Curve::ComputePoint(const float t, const BinomialList& binomials) const
	{
		glm::vec2 sum(0);

		const int n = static_cast<int>(controlPoints.size()) - 1;

		for (int i = 0; i < static_cast<int>(controlPoints.size()); i++)
		{
			sum += controlPoints[i] * static_cast<float>(binomials[i]) * std::pow(1 - t, n - i) * std::pow(t, i);
		}

		return sum;
	}

	float Curve::ComputeDistanceToCurve(const glm::vec2& p) const
	{
		return 0.0f;
	}
}
