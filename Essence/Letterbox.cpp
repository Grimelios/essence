#include "Letterbox.h"
#include "Messaging.h"
#include "Resolution.h"
#include "KeyboardData.h"
#include "Ease.h"
#include "Color.h"
#include "SpriteBatch.h"

namespace Essence
{
	Letterbox::Letterbox()
	{
		OnResize();

		Messaging::Subscribe(MessageTypes::Resize, [this](const std::any& data, const float dt)
		{
			OnResize();
		});

		Messaging::Subscribe(MessageTypes::Keyboard, [this](const std::any& data, const float dt)
		{
			const KeyboardData& keyboardData = std::any_cast<KeyboardData>(data);

			if (keyboardData.Query(GLFW_KEY_L, InputStates::PressedThisFrame))
			{
				Toggle();
			}
		});
	}

	void Letterbox::OnResize()
	{
		upperBounds.width = Resolution::Width;
		lowerBounds.width = Resolution::Width;
		lowerBounds.y = Resolution::Height - lowerBounds.height;
	}

	void Letterbox::Toggle()
	{
		appearing = !visible;
		visible = true;

		if (timer == nullptr)
		{
			timer = std::make_unique<SingleTimer>(RevealTime, [this](const float time)
			{
				visible = appearing;
				timer->elapsed = 0;
				timer->paused = true;
			});

			timer->tick = [this](const float progress)
			{
				Tick(progress);
			};
		}
		else
		{
			timer->paused = false;
		}
	}

	void Letterbox::Tick(const float progress)
	{
		const EaseTypes easeType = appearing ? EaseTypes::QuadraticOut : EaseTypes::QuadraticIn;

		int height = static_cast<int>(Ease::Compute(easeType, progress) * BarHeight);

		if (!appearing)
		{
			height = BarHeight - height;
		}

		upperBounds.height = height;
		lowerBounds.height = height;
		lowerBounds.y = Resolution::Height - height;
	}

	bool Letterbox::IsVisible() const
	{
		return visible;
	}

	void Letterbox::Update(const float dt)
	{
		timer->Update(dt);
	}

	void Letterbox::Draw(SpriteBatch& sb)
	{
		//sb.FillBounds(upperBounds, Color::Black);
		//sb.FillBounds(lowerBounds, Color::Black);
	}
}
