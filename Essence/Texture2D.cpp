#include "Texture2D.h"

namespace Essence
{
	Texture2D::Texture2D(const int width, const int height, const GLuint textureId) :
		width(width),
		height(height),
		textureId(textureId)
	{
	}

	int Texture2D::GetWidth() const
	{
		return width;
	}

	int Texture2D::GetHeight() const
	{
		return height;
	}

	GLuint Texture2D::GetTextureId() const
	{
		return textureId;
	}
}
