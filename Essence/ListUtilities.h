#pragma once
#include "DoublyLinkedList.h"

namespace Essence::ListUtilities
{
	template<class T>
	DoublyLinkedListNode<T>* Contains(DoublyLinkedList<T>& list, const T value)
	{
		for (DoublyLinkedListNode<T>* node = list.GetHead(); node != nullptr; node = node->next)
		{
			if (node->data == value)
			{
				return node;
			}
		}

		return nullptr;
	}
}
