#include "Resolution.h"

namespace Essence
{
	const int Resolution::Width = 1920;
	const int Resolution::Height = 1080;

	int Resolution::WindowWidth;
	int Resolution::WindowHeight;

	glm::ivec2 Resolution::GetDimensions()
	{
		return glm::ivec2(Width, Height);
	}

	glm::ivec2 Resolution::GetWindowDimensions()
	{
		return glm::ivec2(WindowWidth, WindowHeight);
	}
}
