#include "Sprite.h"
#include "GameFunctions.h"
#include "ContentCache.h"
#include "SpriteBatch.h"
#include "Texture2D.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Essence
{
	Sprite::Sprite(const std::string& filename, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const Alignments alignment) :
		Sprite(texture, std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const std::string& filename, const Bounds& source, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), std::optional<Bounds>(source), alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const Bounds& source, const Alignments alignment) :
		Sprite(texture, std::optional<Bounds>(source), alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const std::optional<Bounds>& source, const Alignments alignment) :
		Component2D(alignment),
		
		texture(texture),
		source(source)
	{
		RecomputeOrigin();
	}

	void Sprite::RecomputeOrigin()
	{
		origin = source == std::nullopt
			? GameFunctions::ComputeOrigin(texture.GetWidth(), texture.GetHeight(), alignment)
			: GameFunctions::ComputeOrigin(source->width, source->height, alignment);
	}

	const Texture2D& Sprite::GetTexture() const
	{
		return texture;
	}

	void Sprite::SetSource(const std::optional<Bounds>& source)
	{
		this->source = source;

		RecomputeOrigin();

		sourceChanged = true;
		positionChanged = true;
	}

	void Sprite::Draw(SpriteBatch& sb)
	{
		if (positionChanged)
		{
			int width;
			int height;

			if (source.has_value())
			{
				width = source.value().width;
				height = source.value().height;
			}
			else
			{
				width = texture.GetWidth();
				height = texture.GetHeight();
			}

			glm::vec2 corners[4] =
			{
				glm::vec2(0),
				glm::vec2(0, height),
				glm::vec2(width, 0),
				glm::vec2(width, height)
			};

			for (glm::vec2& p : corners)
			{
				p = (p - origin) * scale;
			}

			if (rotation != 0)
			{
				const glm::mat4 rotationMatrix = rotate(glm::mat4(1), rotation, glm::vec3(0, 0, 1));

				for (glm::vec2& p : corners)
				{
					const glm::vec4 v = rotationMatrix * glm::vec4(p, 0, 1);

					p = glm::vec2(v.x, v.y);
				}
			}

			for (unsigned i = 0; i < 4; i++)
			{
				const glm::vec2 p = corners[i] + position;
				const int index = i * FloatsPerVertex;

				vertexData[index] = p.x;
				vertexData[index + 1] = p.y;
			}

			positionChanged = false;
		}

		if (sourceChanged)
		{
			std::array<glm::vec2, 4> sourceCoords = { };

			if (source.has_value())
			{
				const Bounds& bounds = source.value();

				const int right = bounds.x + bounds.width;
				const int bottom = bounds.y + bounds.height;

				sourceCoords =
				{
					glm::vec2(bounds.x, bounds.y),
					glm::vec2(bounds.x, bottom),
					glm::vec2(right, bounds.y),
					glm::vec2(right, bottom)
				};

				for (glm::vec2& v : sourceCoords)
				{
					v.x /= bounds.width;
					v.y /= bounds.height;
				}
			}
			else
			{
				sourceCoords =
				{
					glm::vec2(0),
					glm::vec2(0, 1),
					glm::vec2(1, 0),
					glm::vec2(1)
				};
			}

			for (unsigned i = 0; i < 4; i++)
			{
				const glm::vec2& v = sourceCoords[i];
				const int index = i * FloatsPerVertex + SourceIndex;

				vertexData[index] = v.x;
				vertexData[index + 1] = v.y;
			}
			
			sourceChanged = false;
		}

		if (colorChanged)
		{
			const float f = color.ToFloat();

			for (unsigned i = ColorIndex; i < vertexData.size(); i += FloatsPerVertex)
			{
				vertexData[i] = f;
			}

			colorChanged = false;
		}

		sb.SetMode(GL_TRIANGLE_STRIP);
		sb.BindTexture(texture);
		sb.Buffer(vertexData);
	}
}
