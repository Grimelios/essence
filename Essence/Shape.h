#pragma once
#include <glm/vec2.hpp>
#include "ShapeHelper.h"
#include "ShapeTypes.h"

namespace Essence
{
	class Shape
	{
	private:

		static ShapeHelper helper;

		ShapeTypes shapeType;

	protected:

		float rotation;

		explicit Shape(ShapeTypes shapeType);
		Shape(float rotation, ShapeTypes shapeType);
		Shape(const glm::vec2& position, float rotation, ShapeTypes shapeType);

	public:

		virtual ~Shape() = default;

		ShapeTypes GetShapeType() const;

		glm::vec2 position;

		float GetRotation() const;
		virtual void SetRotation(float rotation);

		bool Intersects(const Shape* other) const;
	};
}
