#include "EntityLayer.h"
#include <utility>
#include "Scene.h"

namespace Essence
{
	EntityLayer::EntityLayer(Scene* parent, std::vector<EntityGroups> updateOrder,
		std::vector<EntityGroups> drawOrder) :

		updateOrder(std::move(updateOrder)),
		drawOrder(std::move(drawOrder)),
		parent(parent)
	{
	}

	Entity* EntityLayer::AddInternal(EntityPointer& e)
	{
		const EntityGroups group = e->GetGroup();

		if (group == EntityGroups::Unassigned)
		{
			throw std::runtime_error("Entity group must be assigned before adding to a layer.");
		}

		Entity* entity = entities[static_cast<int>(group)].Add(e)->get();
		entity->Initialize(parent, this);

		return entity;
	}

	EntityLayer::EntityList& EntityLayer::GetEntities(const EntityGroups group)
	{
		return entities[static_cast<int>(group)];
	}

	void EntityLayer::Add(EntityPointer& entity)
	{
		AddInternal(entity);
	}

	void EntityLayer::Remove(Entity* entity)
	{
		EntityList& list = entities[static_cast<int>(entity->GetGroup())];
		EntityNode* node = list.GetHead();

		while (node != nullptr)
		{
			if (node->data.get() == entity)
			{
				node->data.reset();
				list.Remove(node);

				return;
			}
		}
	}

	void EntityLayer::Update(const float dt)
	{
		for (auto group : updateOrder)
		{
			EntityNode* node = entities[static_cast<int>(group)].GetHead();

			while (node != nullptr)
			{
				node->data->Update(dt);
				node = node->next;
			}
		}
	}

	void EntityLayer::Draw(SpriteBatch& sb)
	{
		for (auto group : drawOrder)
		{
			EntityNode* node = entities[static_cast<int>(group)].GetHead();

			while (node != nullptr)
			{
				node->data->Draw(sb);
				node = node->next;
			}
		}
	}
}
