#pragma once
#include "IDynamic.h"
#include "VerletCloth.h"
#include "IRenderable.h"

namespace Essence
{
	class MouseData;
	class ClothTester : public IDynamic, public IRenderable
	{
	private:

		VerletCloth cloth;

		void HandleMouse(const MouseData& data);

	public:

		ClothTester();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
