#include "Scene.h"
#include "JsonUtilities.h"
#include <map>
#include "FragmentLoader.h"

namespace Essence
{
	Scene::Scene(World* world, Space* space, Camera* camera) :
		world(world),
		space(space),
		camera(camera)
	{
		const std::map<std::string, EntityGroups> map =
		{
			{ "Player", EntityGroups::Player },
			{ "Enemies", EntityGroups::Enemies },
			{ "Creatures", EntityGroups::Creatures },
			{ "Characters", EntityGroups::Characters },
			{ "Objects", EntityGroups::Objects },
			{ "World", EntityGroups::World }
		};

		std::vector<Json> blocks = JsonUtilities::Load("Layers.json");

		for (const Json& j : blocks)
		{
			std::vector<EntityGroups> updateOrder = ParseOrderingVector(j.at("Update"), map);
			std::vector<EntityGroups> drawOrder = ParseOrderingVector(j.at("Draw"), map);

			layers.emplace_back(this, updateOrder, drawOrder);
		}
	}

	std::vector<EntityGroups> Scene::ParseOrderingVector(const Json& j, const GroupMap& map)
	{
		std::vector<std::string> names = j;
		std::vector<EntityGroups> groups;
		groups.reserve(names.size());

		for (const std::string& name : names)
		{
			groups.push_back(map.at(name));
		}

		return groups;
	}

	World* Scene::GetWorld() const
	{
		return world;
	}

	Space* Scene::GetSpace() const
	{
		return space;
	}

	Camera* Scene::GetCamera() const
	{
		return camera;
	}

	void Scene::LoadFragment(const std::string& filename)
	{
		auto entities = FragmentLoader::Load(filename);
		
		EntityLayer* layer = &layers[0];

		for (std::unique_ptr<Entity>& e : entities)
		{
			layer->Add(e);
		}
	}

	void Scene::Remove(const int layer, Entity* entity)
	{
		layers[layer].Remove(entity);
	}

	void Scene::Update(const float dt)
	{
		for (auto& layer : layers)
		{
			layer.Update(dt);
		}
	}

	void Scene::Draw(SpriteBatch& sb)
	{
		for (auto& layer : layers)
		{
			layer.Draw(sb);
		}
	}
}
