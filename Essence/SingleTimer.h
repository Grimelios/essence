#pragma once
#include "Timer.h"

namespace Essence
{
	class SingleTimer : public Timer
	{
	private:

		using SingleFunction = std::function<void(float)>;

		std::optional<SingleFunction> trigger;

	public:

		SingleTimer(int duration, std::optional<SingleFunction> trigger, float elapsed = 0);

		void Update(float dt) override;
	};
}
