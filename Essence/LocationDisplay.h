#pragma once
#include "HudElement.h"
#include "SpriteText.h"

namespace Essence
{
	class LocationDisplay : public HudElement
	{
	private:

		SpriteText spriteText;

	public:

		LocationDisplay(int offsetX, int offsetY, Alignments alignment);

		void SetLocation(const glm::ivec2& location) override;
	};
}
