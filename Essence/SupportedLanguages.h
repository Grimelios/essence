#pragma once

namespace Essence
{
	enum class SupportedLanguages
	{
		English,
		Spanish,
		Count = 2
	};
}
