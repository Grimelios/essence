#pragma once
#include "UIElement.h"

namespace Essence
{
	class TitleLoop;
	class AggregateData;
	class PressStart : public UIElement
	{
	private:

		TitleLoop* parent;

		int subIndex = -1;

		void ProcessInput(const AggregateData& data);

	public:

		explicit PressStart(TitleLoop* parent);

		void Dispose() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
