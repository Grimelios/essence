#include "Tilemap.h"
#include "World.h"
#include "AxisBox.h"
#include "Bounds.h"
#include "Scene.h"

namespace Essence
{
	Tilemap::Tilemap(const Json& j) : Tilemap(TilemapData(j))
	{
	}

	Tilemap::Tilemap(const TilemapData& data) :
		Entity(EntityGroups::World),
		
		render(data)
	{
		const auto& boxes = data.boxes;

		this->boxes.reserve(boxes.size());

		for (const Bounds& box : boxes)
		{
			this->boxes.emplace_back(box.x, box.y, box.width, box.height);
		}
	}

	void Tilemap::Initialize(Scene* scene, EntityLayer* layer)
	{
		bodies.reserve(boxes.size());

		World* world = scene->GetWorld();

		for (AxisBox& box : boxes)
		{
			bodies.push_back(world->CreateBody(&box, BodyTypes::Static, this));
			bodies.back()->position = position + box.position;
		}

		Entity::Initialize(scene, layer);
	}

	void Tilemap::SetPosition(const glm::vec2& position)
	{
		// Note that not calling the base function is intentional here on the assumption that tilemaps contain no
		// components.
		this->position = position;

		for (int i = 0; i < static_cast<int>(bodies.size()); i++)
		{
			bodies[i]->position = position + boxes[i].position;;
		}

		render.position = position;
	}

	void Tilemap::Dispose()
	{
		World* world = scene->GetWorld();

		for (Body* body : bodies)
		{
			world->Remove(body);
		}

		Entity::Dispose();
	}

	void Tilemap::Draw(SpriteBatch& sb)
	{
		render.Draw(sb);
	}
}
