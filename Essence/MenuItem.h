#pragma once
#include "Container2D.h"
#include "ISelectable.h"

namespace Essence
{
	class Menu;
	class MenuItem : public Container2D, public ISelectable
	{
	private:

		int index;

		Menu* parent;

	protected:

		bool enabled;

		MenuItem(int index, Menu* parent);

	public:

		virtual ~MenuItem() = 0;
		virtual void SetEnabled(bool enabled);

		bool IsEnabled() const override;
		bool Contains(const glm::vec2& mousePosition) override;

		void OnHover(const glm::vec2& mousePosition) override;
		void OnUnhover() override;
		void OnMouseMove(const glm::vec2& mousePosition) override;
		void OnClick(const glm::vec2& mousePosition) override;
	};

	inline MenuItem::~MenuItem() = default;
}
