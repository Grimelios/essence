#pragma once

namespace Essence
{
	class Player;
	class IInteractive
	{
	public:

		virtual ~IInteractive() = default;
		virtual void OnInteract(Player* player) = 0;
	};
}
