#pragma once
#include "IDisposable.h"
#include "DoublyLinkedList.h"
#include "SensorTypes.h"
#include "SensorFlags.h"
#include <functional>

namespace Essence
{
	class Shape;
	class Space;
	class Sensor : public IDisposable
	{
	private:

		using ContactFunction = std::function<void(void*)>;
		using SeparationFunction = std::function<void(void*)>;

		Shape* shape;
		Space* space;

		void* owner;

		// Sensor type is assumed to not change after the sensor is created. Even if an object is stationary at first and activates
		// later, the sensor is still marked as dynamic.
		SensorTypes sensorType;

	public:

		// The Space class also uses these aliases.
		using ContactList = DoublyLinkedList<Sensor*>;
		using ContactNode = DoublyLinkedListNode<Sensor*>;

		Sensor(Shape* shape, Space* space, void* owner, SensorTypes type);

		ContactList contactList;
		Shape* GetShape() const;
		SensorTypes GetSensorType() const;
		SensorFlags flags = SensorFlags::None;

		ContactFunction onContact = nullptr;
		SeparationFunction onSeparation = nullptr;

		void* GetOwner() const;
		void Dispose() override;
	};
}
