#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include "TitleMenu.h"

namespace Essence
{
	class AggregateData;
	class MenuManager : public IDynamic, public IRenderable
	{
	private:

		//TitleMenu titleMenu;

	public:

		MenuManager();

		void ProcessInput(const AggregateData& data);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
