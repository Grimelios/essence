#include "MenuControls.h"
#include "JsonUtilities.h"

namespace Essence
{
	MenuControls::MenuControls()
	{
		const Json j = JsonUtilities::Load("Controls.json").at("Menus");

		up = ParseBinds(j, "Up");
		down = ParseBinds(j, "Down");
		left = ParseBinds(j, "Left");
		right = ParseBinds(j, "Right");
		submit = ParseBinds(j, "Submit");
		back = ParseBinds(j, "Back");
	}
}
