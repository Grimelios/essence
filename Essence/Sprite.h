#pragma once
#include "Component2D.h"
#include <optional>
#include <string>
#include "Bounds.h"

namespace Essence
{
	class Texture2D;
	class Sprite : public Component2D
	{
	private:

		const Texture2D& texture;

		// The vertex structure is interleaved (position, texture coordinates, and color).
		std::array<float, FloatsPerQuad> vertexData { };
		std::optional<Bounds> source;

		Sprite(const Texture2D& texture, const std::optional<Bounds>& source, Alignments alignment);

		void RecomputeOrigin();

	public:

		explicit Sprite(const std::string& filename, Alignments alignment = Alignments::Center);
		explicit Sprite(const Texture2D& texture, Alignments alignment = Alignments::Center);

		Sprite(const std::string& filename, const Bounds& source, Alignments alignment = Alignments::Center);
		Sprite(const Texture2D& texture, const Bounds& source, Alignments alignment = Alignments::Center);

		const Texture2D& GetTexture() const;

		void SetSource(const std::optional<Bounds>& source);
		void Draw(SpriteBatch& sb) override;
	};
}
