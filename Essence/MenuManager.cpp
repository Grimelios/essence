#include "MenuManager.h"
#include "Messaging.h"
#include "AggregateData.h"

namespace Essence
{
	MenuManager::MenuManager()
	{
		//titleMenu.SetLocation(glm::ivec2(20));

		Messaging::Subscribe(MessageTypes::Input, [this](const std::any& data, const float dt)
		{
			ProcessInput(std::any_cast<AggregateData>(data));
		});
	}

	void MenuManager::ProcessInput(const AggregateData& data)
	{
		//titleMenu.ProcessInput(data);
	}

	void MenuManager::Update(const float dt)
	{
		//titleMenu.Update(dt);
	}

	void MenuManager::Draw(SpriteBatch& sb)
	{
		//titleMenu.Draw(sb, pb);
	}
}
