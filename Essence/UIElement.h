#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include "IDisposable.h"

namespace Essence
{
	class UIElement : public IDynamic, public IRenderable, public IDisposable
	{
	protected:

		bool visible = false;

	public:

		bool IsVisible() const;

		void Dispose() override;
	};
}
