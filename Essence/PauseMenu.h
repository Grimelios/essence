#pragma once
#include "Menu.h"

namespace Essence
{
	class PauseScreen;
	class PauseMenu : public Menu
	{
	private:

		PauseScreen* parent;

	public:

		explicit PauseMenu(PauseScreen* parent);

		void Submit(int index) override;
	};
}
