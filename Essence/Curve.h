#pragma once
#include <vector>
#include <glm/vec2.hpp>

namespace Essence
{
	class Curve
	{
	private:

		using BinomialList = std::vector<int>;
		using BinomialTerms = std::vector<BinomialList>;

		static BinomialTerms binomialTerms;
		static const BinomialList& GetTerms(int n);

		std::vector<glm::vec2> controlPoints;

		glm::vec2 ComputePoint(float t, const BinomialList& binomials) const;

	public:

		Curve() = default;
		explicit Curve(std::vector<glm::vec2> controlPoints);

		std::vector<glm::vec2>& GetControlPoints();
		std::vector<glm::vec2> ComputePoints(int segmentCount) const;
		
		float ComputeDistanceToCurve(const glm::vec2& p) const;
	};
}
