#include "ClickableSet.h"
#include "MouseData.h"

namespace Essence
{
	bool ClickableSet::WasItemHoveredThisFrame() const
	{
		return itemHoveredThisFrame;
	}

	bool ClickableSet::WasItemClickedThisFrame() const
	{
		return itemClickedThisFrame;
	}

	void ClickableSet::ProcessMouse(const MouseData& data)
	{
		const glm::vec2& mousePosition = data.GetScreenPosition();

		// When multiple input devices are in play (which will nearly always be the case given mouse, keyboard, and
		// possibly controller), the mouse should only affect items when it moves. The exception is clicking to select
		// something, where the mouse can click an item that's technically unhovered (if the selection was modified
		// using a different input device).
		const bool mouseMoved = mousePosition != data.GetOldScreenPosition();

		if (mouseMoved)
		{
			if (hoveredItem != nullptr)
			{
				if (!hoveredItem->Contains(mousePosition))
				{
					hoveredItem->OnUnhover();
					hoveredItem = nullptr;
					itemHoveredThisFrame = false;
				}
				else if (mouseMoved)
				{
					hoveredItem->OnMouseMove(mousePosition);
					itemHoveredThisFrame = true;
				}
			}

			// If an item is already hovered (and confirmed to still be hovered this frame), there's no need to check other
			// items (under the assumption that no two items will ever overlap).
			if (hoveredItem == nullptr)
			{
				for (IClickable* item : items)
				{
					if (item->IsEnabled() && item->Contains(mousePosition))
					{
						hoveredItem = item;
						hoveredItem->OnHover(mousePosition);
						itemHoveredThisFrame = true;

						break;
					}
				}
			}
		}

		// The action of submitting menus with left click cannot be rebound. If menu functionality could be rebound,
		// players might accidentally rebind themselves into a position that would be difficult to fix.
		if (data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::PressedThisFrame))
		{
			// Looping allows clicking on items even if the active item was changed through another input method.
			for (IClickable* item : items)
			{
				if (item->Contains(mousePosition))
				{
					item->OnClick(mousePosition);
				}
			}

			itemClickedThisFrame = true;
		}
		else
		{
			itemClickedThisFrame = false;
		}
	}
}
