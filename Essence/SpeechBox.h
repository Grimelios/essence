#pragma once
#include <vector>
#include <string>
#include "Container2D.h"
#include "SpeechGlyph.h"
#include <glm/vec2.hpp>

namespace Essence
{
	class SpriteFont;
	class SpeechBox : public Container2D
	{
	private:

		using GlyphPointer = std::unique_ptr<SpeechGlyph>;
		using GlyphList = std::vector<GlyphPointer>;

		GlyphList glyphs;
		//RepeatingTimer glyphTimer;
		
		const SpriteFont& font;

		std::vector<std::string> lines;
		glm::vec2 linePosition;

		int glyphIndex = 0;
		int lineIndex = 0;
		int spaceWidth = 0;
		int revealedWidth = 0;

		bool RevealGlyph(float time);

	public:

		explicit SpeechBox(const std::string& font);

		void Refresh(const std::string& value);
		void Draw(SpriteBatch& sb) override;
	};
}
