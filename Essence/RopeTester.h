#pragma once
#include "IDynamic.h"
#include "VerletRope.h"
#include "IRenderable.h"
#include "Line.h"

namespace Essence
{
	class MouseData;
	class RopeTester : public IDynamic, public IRenderable
	{
	private:

		VerletRope rope;
		Line resultLine;

	public:

		RopeTester();

		void ProcessMouse(const MouseData& data);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
