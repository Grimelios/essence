#include "ShapeHelper.h"
#include "Shape.h"
#include "AxisBox.h"

namespace Essence
{
	ShapeHelper::ShapeHelper()
	{
		const int count = static_cast<int>(indexBases.size());
		int current = 0;

		for (int i = 0; i < count; i++)
		{
			indexBases[i] = current;
			current += count - i + 1;
		}
	}

	bool ShapeHelper::CheckIntersection(const Shape* shape1, const Shape* shape2)
	{
		const int type1 = static_cast<int>(shape1->GetShapeType());
		const int type2 = static_cast<int>(shape2->GetShapeType());
		const int index = indexBases[type1] + (type2 - type1);

		if (type1 > type2)
		{
			std::swap(shape1, shape2);
		}

		switch (index)
		{
			case 0: return CheckIntersection(static_cast<const AxisBox*>(shape1), static_cast<const AxisBox*>(shape2));
		}

		return false;
	}

	bool ShapeHelper::CheckIntersection(const AxisBox* box1, const AxisBox* box2)
	{
		const float dX = box1->position.x - box2->position.x;

		if (dX > box1->GetHalfWidth() + box2->GetHalfWidth())
		{
			return false;
		}

		const float dY = box1->position.y - box2->position.y;

		return dY <= box1->GetHalfHeight() + box2->GetHalfHeight();
	}
}
