#pragma once
#include "ControlClass.h"

namespace Essence
{
	// Most menu functionality isn't rebindable, but using a control class still simplifies handling multiple devices (such as KBM and
	// controller).
	class MenuControls : public ControlClass
	{
	public:

		MenuControls();

		std::vector<InputBind> up;
		std::vector<InputBind> down;
		std::vector<InputBind> left;
		std::vector<InputBind> right;
		std::vector<InputBind> submit;
		std::vector<InputBind> back;
	};
}
