#pragma once
#include <array>
#include "ShapeTypes.h"

namespace Essence
{
	class Shape;
	class AxisBox;
	class ShapeHelper
	{
	private:

		std::array<int, static_cast<int>(ShapeTypes::Count)> indexBases;

		static bool CheckIntersection(const AxisBox* box1, const AxisBox* box2);

	public:

		ShapeHelper();

		bool CheckIntersection(const Shape* shape1, const Shape* shape2);
	};
}
