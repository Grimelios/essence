#pragma once
#include <glad/glad.h>
#include "IDisposable.h"
#include "RenderTargetFlags.h"

namespace Essence
{
	class RenderTarget : public IDisposable
	{
	private:

		int width;
		int height;

		GLuint textureId = 0;
		GLuint frameBuffer = 0;
		GLuint renderBuffer = 0;

		bool depthEnabled = false;
		bool stencilEnabled = false;

	public:

		RenderTarget() = default;
		RenderTarget(int width, int height, RenderTargetFlags flags);

		void Initialize(int width, int height, RenderTargetFlags flags);

		int GetWidth() const;
		int GetHeight() const;

		GLuint GetTextureId() const;
		GLuint GetFrameBuffer() const;
		GLuint GetRenderBuffer() const;

		bool IsDepthEnabled() const;
		bool IsStencilEnabled() const;

		void Use() const;
		void Dispose() override;
	};
}
