#include "PressStart.h"
#include "Messaging.h"
#include "AggregateData.h"

namespace Essence
{
	PressStart::PressStart(TitleLoop* parent) : parent(parent)
	{
		subIndex = Messaging::Subscribe(MessageTypes::Input, [this](const std::any& data, const float dt)
		{
			ProcessInput(std::any_cast<AggregateData>(data));
		});
	}

	void PressStart::ProcessInput(const AggregateData& data)
	{
		if (data.AnyButtonPressed())
		{
		}
	}

	void PressStart::Dispose()
	{
		Messaging::Unsubscribe(MessageTypes::Input, subIndex);
	}

	void PressStart::Update(const float dt)
	{
	}

	void PressStart::Draw(SpriteBatch& sb)
	{
	}
}
