#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include <glm/vec2.hpp>
#include "RenderTarget.h"

namespace Essence
{
	class Rain : public IDynamic, public IRenderable
	{
	private:

		std::vector<glm::vec2> raindrops;

		glm::vec2 direction = glm::vec2(0, 1);
		glm::vec2 velocity = glm::vec2(0);

		RenderTarget target;

	public:

		Rain();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
