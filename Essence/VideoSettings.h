#pragma once
#include "WindowModes.h"

namespace Essence
{
	class VideoSettings
	{
	public:

		WindowModes windowMode;

		int windowWidth;
		int windowHeight;

		bool vSyncEnabled;
		bool motionBlurEnabled;
	};
}
