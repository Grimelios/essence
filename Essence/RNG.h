#pragma once
#include <random>

namespace Essence
{
	class RNG
	{
	private:

		using Generator = std::mt19937;
		using FloatDistribution = std::uniform_real_distribution<float>;
		using BoolDistribution = std::uniform_int_distribution<int>;

		static Generator generator;
		static FloatDistribution floatDistribution;
		static BoolDistribution boolDistribution;

	public:

		static void Initialize();

		static int GetInt(int limit);
		static int GetInt(int min, int max);

		static bool GetBool();
	};
}
