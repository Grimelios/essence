#include "AngleSpring.h"

namespace Essence
{
	AngleSpring::AngleSpring(const float k, const float damping) :
		k(k),
		damping(damping)
	{
	}

	void AngleSpring::Update(const float dt)
	{
		// See https://en.wikipedia.org/wiki/Hooke%27s_law.
		const float force = (targetAngle - angle) * k;

		angularVelocity += force;
		angle += angularVelocity * dt;
		angularVelocity *= damping;
	}
}
