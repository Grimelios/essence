#pragma once
#include <glad/glad.h>

namespace Essence
{
	class Texture2D
	{
	private:

		int width;
		int height;

		GLuint textureId;

	public:

		Texture2D(int width, int height, GLuint textureId);

		int GetWidth() const;
		int GetHeight() const;

		GLuint GetTextureId() const;
	};
}
