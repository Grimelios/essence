#include "PhysicsTester.h"
#include "Messaging.h"
#include "MouseData.h"
#include "Line.h"
#include "GameFunctions.h"
#include <glm/gtc/constants.hpp>

namespace Essence
{
	PhysicsTester::PhysicsTester() :
		line1(100, glm::half_pi<float>()),
		line2(glm::vec2(100, 350), glm::vec2(700, 350)),
		result(line1)
	{
		line1.restriction = LineRestrictions::RestrictToY;
		line2.SetRotation(0.1f);

		Messaging::Subscribe(MessageTypes::Mouse, [this](const std::any& data, const float dt)
		{
			//ProcessMouse(std::any_cast<MouseData>(data), dt);
		});

		Messaging::Subscribe(MessageTypes::Keyboard, [this](const std::any& data, const float dt)
		{
			ProcessKeyboard(std::any_cast<KeyboardData>(data), dt);
		});
	}

	void PhysicsTester::ProcessMouse(const MouseData& data, float dt)
	{
		const glm::vec2 mousePosition = data.GetScreenPosition();

		if (data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::Held))
		{
			line1.position = mousePosition;
			vLine.SetEndpoints(line1.position, line1.position + velocity);
		}
		else if (data.Query(GLFW_MOUSE_BUTTON_RIGHT, InputStates::Held))
		{
			velocity = mousePosition - line1.position;
			vLine.SetEndpoints(line1.position, mousePosition);
		}
	}

	void PhysicsTester::ProcessKeyboard(const KeyboardData& data, const float dt)
	{
		const bool left = data.Query(GLFW_KEY_A, InputStates::Held);
		const bool right = data.Query(GLFW_KEY_D, InputStates::Held);
		const bool up = data.Query(GLFW_KEY_W, InputStates::Held);
		const bool down = data.Query(GLFW_KEY_S, InputStates::Held);

		const float acceleration = 1000;
		const float deceleration = 1000;
		const float maxSpeed = 2000;

		if (left ^ right)
		{
			velocity.x += (left ? -acceleration : acceleration) * dt;
			velocity.x = std::clamp(velocity.x, -maxSpeed, maxSpeed);
		}
		else if (velocity.x != 0)
		{
			const int previousSign = GameFunctions::ComputeSign(velocity.x);

			velocity.x -= deceleration * previousSign * dt;

			if (GameFunctions::ComputeSign(velocity.x) != previousSign)
			{
				velocity.x = 0;
			}
		}

		if (up ^ down)
		{
			velocity.y += (up ? -acceleration : acceleration) * dt;
			velocity.y = std::clamp(velocity.y, -maxSpeed, maxSpeed);
		}
		else if (velocity.y != 0)
		{
			const int previousSign = GameFunctions::ComputeSign(velocity.y);

			velocity.y -= deceleration * previousSign * dt;

			if (GameFunctions::ComputeSign(velocity.y) != previousSign)
			{
				velocity.y = 0;
			}
		}
	}

	void PhysicsTester::Update(const float dt)
	{
		line1.position += velocity * dt;
		Resolve(line1, line2, dt);
	}

	void PhysicsTester::Resolve(Line& line1, Line& line2, const float dt)
	{
		if (velocity.y == 0)
		{
			return;
		}

		const glm::vec2& p1 = line1.position;
		const glm::vec2& p2 = line2.position;
		const glm::vec2& hv1 = line1.GetHalfVector();
		const glm::vec2& hv2 = line2.GetHalfVector();

		const float dX = p1.x - p2.x;

		overlapping = false;

		if (std::abs(dX) > hv2.x)
		{
			return;
		}

		const float y = (p2 + hv2 * (dX / hv2.x)).y;
		const float dY = p1.y - y;
		
		float overlap = hv1.y - std::abs(dY);

		if (overlap > 0)
		{
			if (GameFunctions::ComputeSign(dY * velocity.y) == 1)
			{
				overlap = hv1.y * 2 - overlap;
			}

			line1.position.y -= overlap * GameFunctions::ComputeSign(velocity.y);
			velocity.y = 0;
			overlapping = true;
		}
	}

	void PhysicsTester::Draw(SpriteBatch& sb)
	{
		//sb.DrawLine(line2, glm::vec4(0.2f, 0.2f, 0.2f, 1));
		//sb.DrawLine(line1, glm::vec4(0.5f, 0.5f, 0.5f, 1));
		//pb.DrawLine(vLine, glm::vec4(0, 1, 1, 1));

		if (overlapping)
		{
			//pb.DrawLine(result, glm::vec4(1, 1, 0, 1));
		}
	}
}
