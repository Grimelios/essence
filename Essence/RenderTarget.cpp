#include "RenderTarget.h"
#include <exception>

namespace Essence
{
	RenderTarget::RenderTarget(const int width, const int height, const RenderTargetFlags flags) :
		width(width),
		height(height)
	{
		Initialize(width, height, flags);
	}

	void RenderTarget::Initialize(const int width, const int height, const RenderTargetFlags flags)
	{
		// See http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/ and
		// https://learnopengl.com/Advanced-OpenGL/Framebuffers.
		glGenFramebuffers(1, &frameBuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

		// Texture
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		if (flags != RenderTargetFlags::None)
		{
			const GLenum format = flags == RenderTargetFlags::Depth ? GL_DEPTH_COMPONENT : GL_DEPTH24_STENCIL8;
			const GLenum attachment = flags == RenderTargetFlags::Depth ? GL_DEPTH_ATTACHMENT : GL_DEPTH_STENCIL_ATTACHMENT;

			// Render buffer
			glGenRenderbuffers(1, &renderBuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
			glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderBuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		}

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			throw std::exception("Error creating render target.");
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		depthEnabled = flags == RenderTargetFlags::Depth || flags == RenderTargetFlags::DepthStencil;
		stencilEnabled = flags == RenderTargetFlags::Stencil || flags == RenderTargetFlags::DepthStencil;
	}

	int RenderTarget::GetWidth() const
	{
		return width;
	}

	int RenderTarget::GetHeight() const
	{
		return height;
	}

	GLuint RenderTarget::GetTextureId() const
	{
		return textureId;
	}

	GLuint RenderTarget::GetFrameBuffer() const
	{
		return frameBuffer;
	}

	GLuint RenderTarget::GetRenderBuffer() const
	{
		return renderBuffer;
	}

	bool RenderTarget::IsDepthEnabled() const
	{
		return depthEnabled;
	}

	bool RenderTarget::IsStencilEnabled() const
	{
		return stencilEnabled;
	}

	void RenderTarget::Use() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	}

	void RenderTarget::Dispose()
	{
		glDeleteTextures(1, &textureId);
		glDeleteFramebuffers(1, &frameBuffer);
		glDeleteRenderbuffers(1, &renderBuffer);
	}
}
