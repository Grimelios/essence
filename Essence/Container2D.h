#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <glm/vec2.hpp>
#include "Bounds.h"
#include <vector>
#include "IDisposable.h"

namespace Essence
{
	class Component2D;
	class Container2D : public IDynamic, public IRenderable, public IDisposable
	{
	protected:

		Bounds bounds;

		bool centered = false;

		std::vector<Container2D*> containers;
		std::vector<Component2D*> components;

	public:

		virtual ~Container2D() = 0;

		glm::ivec2 GetLocation() const;

		// The reason that bounds aren't simply exposed publicly is that bounds functions (width, height, and location)
		// can be overridden. To allow direct modification of the bounds might risk accidentally changing the bounds
		// without updating the container as a whole.
		const Bounds& GetBounds() const;

		bool visible = true;

		virtual void SetWidth(int width);
		virtual void SetHeight(int height);
		virtual void SetLocation(const glm::ivec2& location);

		void Dispose() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	inline Container2D::~Container2D() = default;
}
