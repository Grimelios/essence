#version 330 core

in vec2 fPosition;
in vec2 fTexCoords;

uniform sampler2D sampler;
uniform vec2 cameraPosition;
uniform vec2 screenSize;

void main()
{
	vec4 color = texture2D(sampler, fTexCoords);

	if (color.r < 1)
	{
		gl_FragColor = color;

		return;
	}

	// "P" stands for "percentage" (ranging from 0 to 1 across the screen).
	float pX = (fPosition.x - cameraPosition.x) / screenSize.x;

	gl_FragColor = vec4(ToRGB(pX));
}

// See https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion (for this and the function below).
vec3 ToRGB(float h)
{
	// Changing hue alone is enough to smoothly cycle moss color along the wall.
	const float s = 1;
	const float v = 1;

	float q = 1;
	float p = 1;

	vec3 color;
	color.r = ToRGB(p, q, h + 1/3);
	color.g = ToRGB(p, q, h);
	color.b = ToRGB(p, q, h - 1/3);

	return color;
}

float ToRGB(float p, float q, float t)
{
	if (t < 0) t++;
    if (t > 1) t--;
    if (t < 1/6) return p + (q - p) * 6 * t;
    if (t < 1/2) return q;
    if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;

    return p;
}
