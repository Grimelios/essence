#version 440 core

layout (triangles, equal_spacing, ccw) in;

in vec3 tePositions[];
in vec2 teSources[];
in vec3 teNormals[];

uniform mat4 viewProjection;
uniform sampler2D displacementMap;
uniform float displacementFactor;

out vec3 fNormal;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

void main()
{
	vec3 fPosition = interpolate3D(tePositions[0], tePositions[1], tePositions[2]);
	vec2 fSource = interpolate2D(teSources[0], teSources[1], teSources[2]);

	fNormal = normalize(interpolate3D(teNormals[0], teNormals[1], teNormals[2]));

	float displacement = texture(displacementMap, fSource).x;

	fPosition += fNormal * displacement * displacementFactor;

	gl_Position = viewProjection * vec4(fPosition, 1);
}
