#version 330 core

layout (location = 0) in vec2 vPosition;
layout (location = 1) in vec4 vColor;

out vec4 fColor;

void main()
{
	gl_Position = vec4(vPosition.x, -vPosition.y, 0, 1);
	fColor = vColor;
}
