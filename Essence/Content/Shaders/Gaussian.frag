#version 330 core

in vec2 fTexCoords;

uniform sampler2D sampler;
uniform bool horizontal;
uniform float weights[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

// See https://learnopengl.com/Advanced-Lighting/Bloom.
void main()
{
	vec2 offset = textureSize(sampler, 0);
	vec3 result = texture2D(sampler, fTexCoords).rgb * weights[0];

	if (horizontal)
	{
		for (int i = 0; i < 5; i++)
		{
			result += texture2D(sampler, fTexCoords + vec2(offset.x * i, 0)).rgb * weights[i];
			result += texture2D(sampler, fTexCoords - vec2(offset.x * i, 0)).rgb * weights[i];
		}
	}
	else
	{
		for (int i = 0; i < 5; i++)
		{
			result += texture2D(sampler, fTexCoords + vec2(0, offset.y * i)).rgb * weights[i];
			result += texture2D(sampler, fTexCoords - vec2(0, offset.7 * i)).rgb * weights[i];
		}
	}

	gl_FragColor = vec4(result, 1);
}
