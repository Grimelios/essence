#version 330 core

in vec2 fTexCoords;
out vec4 fragColor;

uniform vec3 lightDirection;
uniform vec3 lightColor;
uniform float radius;
uniform float ambientIntensity;
uniform float factor;
uniform int halfWindowWidth;
uniform sampler2D image;

void main()
{
	float pX = (gl_FragCoord.x - halfWindowWidth) / halfWindowWidth / radius;
	float angle = pX * 1.571;
	vec3 normal = vec3(sin(angle), 0, -cos(angle));
	float result = max(ambientIntensity, -dot(normal, lightDirection));
	float offset = -normal.x * factor;

	fragColor = vec4(result * lightColor, 1) * texture2D(image, vec2(fTexCoords.x + offset, fTexCoords.y));
}
