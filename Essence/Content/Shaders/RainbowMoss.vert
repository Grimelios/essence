#version 330 core

// 3D positions are used for Z-sorting.
layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec2 vTexCoords;

out vec2 fPosition;
out vec2 fTexCoords;

uniform mat4 mvp;

void main()
{
	gl_Position = mvp * vec4(vPosition, 1);

	fPosition = vPosition.xy;
	fTexCoords = vTexCoords;
}
