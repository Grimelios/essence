#version 440 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec2 vSource;
layout (location = 2) in vec3 vNormal;

uniform mat4 world;

out vec3 tcPosition;
out vec2 tcSource;
out vec3 tcNormal;

void main()

{
	tcPosition = (world * vec4(vPosition, 1)).xyz;
	tcSource = vSource;
	tcNormal = (world * vec4(vNormal, 1)).xyz; 
}
