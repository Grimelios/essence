#version 440 core

in vec3 fNormal;

out vec4 fragColor;

void main()
{
	vec3 lightDirection = vec3(0, -1, 0);
	float ambient = 0.1f;
	float value = max(ambient, dot(lightDirection, fNormal));

	fragColor = vec4(value);
}
