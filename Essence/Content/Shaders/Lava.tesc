#version 410 core

layout (vertices = 3) out;

in vec3 tcPositions[];
in vec2 tcSources[];
in vec3 tcNormals[];

uniform vec3 eyePosition;

out vec3 tePositions[];
out vec3 teSources[];
out vec3 teNormals[];

float computeTL(float d1, float d2)
{
	float average = (d1 + d2) / 2;

	if (average <= 2)
	{
		return 10;
	}
	
	if (average <= 5)
	{
		return 7;
	}

	return 3;
}

void main()
{
	float distances[3] = float[]
	(
		distance(eyePosition, tcPositions[0]),
		distance(eyePosition, tcPositions[1]),
		distance(eyePosition, tcPositions[2])
	);

	for (int i = 0; i < 3; i++)
	{
		gl_TessLevelOuter[i] = computeTL(distances[(i + 1) % 3], distances[(i + 2) % 3]);
	}

	gl_TessLevelInner[0] = gl_TessLevelOuter[2];

	tePositions[gl_InvocationID] = tcPositions[gl_InvocationID];
	teSources[gl_InvocationID] = teSources[gl_InvocationID];
	teNormals[gl_InvocationID] = teNormals[gl_InvocationID];
}
