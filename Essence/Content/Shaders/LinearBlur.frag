#version 330 core

in vec2 fTexCoords;

layout (location = 0) out vec4 fragColor;

uniform int samples;
uniform vec2 blurVector;
uniform sampler2D image;

void main()
{
	vec4 color = vec4(0);
	vec2 start = fTexCoords - blurVector / 2;
	vec2 increment = blurVector / samples;

	for (int i = 0; i < samples; i++)
	{
		color += texture2D(image, start + increment * i);
	}

	fragColor = color / samples;
}
