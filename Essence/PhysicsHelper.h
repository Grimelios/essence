#pragma once

namespace Essence
{
	class Body;
	class AxisBox;
	class PhysicsHelper
	{
	private:

		void Resolve(Body& body1, Body& body2, AxisBox& box1, AxisBox& box2) const;

	public:

		void Compare(Body& body1, Body& body2) const;
	};
}
