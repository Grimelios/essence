#pragma once
#include <nlohmann/json.hpp>
#include <vector>
#include "InputBind.h"

namespace Essence
{
	class ControlClass
	{
	protected:

		using Json = nlohmann::json;

		static std::vector<InputBind> ParseBinds(const Json& j, const std::string& key);

	public:

		virtual ~ControlClass() = 0;
	};

	inline ControlClass::~ControlClass() = default;
}
