#pragma once
#include "Screen.h"

namespace Essence
{
	class SettingsScreen : public Screen
	{
	public:

		explicit SettingsScreen(ScreenManager* parent);

		void OnResize(int width, int height) override;
		void Show() override;
		void Hide() override;
	};
}
