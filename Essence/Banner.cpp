#include "Banner.h"
#include "StringUtilities.h"
#include "GameFunctions.h"

namespace Essence
{
	Banner::Banner(const Json& j) : Entity(EntityGroups::Objects)
	{
		const std::vector<glm::vec2> points = GameFunctions::ParseVec2List(j.at("Points").get<std::string>());

		start = points[0];
		end = points[1];
	}

	void Banner::Update(const float dt)
	{
		//cloth.Update(dt);
	}

	void Banner::Draw(SpriteBatch& sb)
	{
	}
}
