#include "PlayerHealthDisplay.h"

namespace Essence
{
	PlayerHealthDisplay::PlayerHealthDisplay(const int offsetX, const int offsetY, const Alignments alignment) :
		HudElement(offsetX, offsetY, alignment)
	{
	}

	void PlayerHealthDisplay::OnDamage(const int damage)
	{
	}

	void PlayerHealthDisplay::OnHeal(const int healing)
	{
	}

	void PlayerHealthDisplay::OnMaxHealthIncrease(const int maxHealth)
	{
	}

	void PlayerHealthDisplay::Update(const float dt)
	{
	}

	void PlayerHealthDisplay::Draw(SpriteBatch& sb)
	{
	}
}
