#pragma once
#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glad.h>

namespace Essence
{
	class PrimitiveGroup
	{

		GLenum mode;

	public:

		explicit PrimitiveGroup(GLenum mode);

		GLenum GetMode() const;

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec4> colors;
		std::vector<int> indices;

		int indexOffset = 0;

		void Clear();
	};
}
