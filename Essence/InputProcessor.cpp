#include "InputProcessor.h"
#include "AggregateData.h"
#include "KeyboardData.h"
#include "Messaging.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Bounds.h"
#include "Camera.h"
#include "Resolution.h"

namespace Essence
{
	InputProcessor::InputProcessor(const Camera& camera) : camera(camera)
	{
		for (InputStates& state : keyArray)
		{
			state = InputStates::Released;
		}

		for (InputStates& state : buttonArray)
		{
			state = InputStates::Released;
		}
	}

	void InputProcessor::OnKeyPress(const int key, const int mods)
	{
		keyArray[key] = InputStates::PressedThisFrame;
		keyPresses.emplace_back(key, mods);
	}

	void InputProcessor::OnKeyRelease(const int key)
	{
		keyArray[key] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseButtonPress(const int button)
	{
		buttonArray[button] = InputStates::PressedThisFrame;
	}

	void InputProcessor::OnMouseButtonRelease(const int button)
	{
		buttonArray[button] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseMove(const float x, const float y)
	{
		mousePosition.x = x;
		mousePosition.y = y;
	}

	void InputProcessor::Update(const float dt)
	{
		KeyboardData keyboardData = CreateKeyboardData();
		MouseData mouseData = CreateMouseData();

		AggregateData aggregateData;
		aggregateData.SetData(InputTypes::Keyboard, keyboardData);
		aggregateData.SetData(InputTypes::Mouse, mouseData);

		Messaging::Send(MessageTypes::Mouse, mouseData, dt);
		Messaging::Send(MessageTypes::Keyboard, keyboardData, dt);
		Messaging::Send(MessageTypes::Input, aggregateData, dt);

		keyPresses.clear();
	}

	KeyboardData InputProcessor::CreateKeyboardData()
	{
		std::vector<int> keysDown;
		std::vector<int> keysReleasedThisFrame;

		// Keys pressed this frame are handled in the key callback function.
		for (int key = 0; key < GLFW_KEY_LAST; key++)
		{
			switch (keyArray[key])
			{
				case InputStates::Held:
				case InputStates::PressedThisFrame:
					keysDown.push_back(key);

					break;

				case InputStates::ReleasedThisFrame:
					keysReleasedThisFrame.push_back(key);

					break;
			}
		}

		KeyboardData data(keysDown, keyPresses, keysReleasedThisFrame, keyArray);

		for (InputStates& state : keyArray)
		{
			switch (state)
			{
				case InputStates::PressedThisFrame: state = InputStates::Held; break;
				case InputStates::ReleasedThisFrame: state = InputStates::Released; break;
			}
		}

		return data;
	}

	MouseData InputProcessor::CreateMouseData()
	{
		// The conversion from glm::vec3 to glm::vec2 is automatic (for computing world position).
		const glm::mat4 viewInverse = glm::inverse(camera.GetView());
		
		const int halfWidth = Resolution::Width / 2;
		const int halfHeight = Resolution::Height / 2;

		glm::vec2 normalizedPosition = mousePosition;
		normalizedPosition.x /= halfWidth;
		normalizedPosition.y /= halfHeight;
		
		glm::vec2 worldPosition(viewInverse * glm::vec4(normalizedPosition, 0, 1));
		worldPosition.x *= halfWidth;
		worldPosition.y *= halfHeight;

		MouseData data(mousePosition, worldPosition, oldScreenPosition, oldWorldPosition, buttonArray);

		oldScreenPosition = mousePosition;
		oldWorldPosition = worldPosition;

		for (InputStates& state : buttonArray)
		{
			switch (state)
			{
				case InputStates::PressedThisFrame: state = InputStates::Held; break;
				case InputStates::ReleasedThisFrame: state = InputStates::Released; break;
			}
		}

		return data;
	}
}
