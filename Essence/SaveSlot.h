#pragma once
#include "Container2D.h"

namespace Essence
{
	class SaveSlot : public Container2D
	{
	public:

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
