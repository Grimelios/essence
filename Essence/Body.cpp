#include "Body.h"
#include "World.h"

namespace Essence
{
	Body::Body(World* world, Shape* shape, const BodyTypes bodyType) :
		world(world),
		shape(shape),
		bodyType(bodyType)
	{
	}

	Shape* Body::GetShape() const
	{
		return shape;
	}

	void Body::Dispose()
	{
		world->Remove(this);
	}
}
