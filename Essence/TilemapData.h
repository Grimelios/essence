#pragma once
#include <vector>
#include <string>
#include <nlohmann/json.hpp>
#include "Bounds.h"

namespace Essence
{
	class TilemapData
	{
	private:

		using Json = nlohmann::json;

	public:

		explicit TilemapData(const Json& j);

		int width;
		int height;

		std::vector<int> tiles;
		std::vector<Bounds> boxes;
		std::string tileset;
	};
}
