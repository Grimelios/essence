#pragma once
#include "DamageSource.h"

namespace Essence
{
	class Entity;
	class Weapon
	{
	private:

		std::optional<DamageSource> damageSource;

	public:

		virtual ~Weapon() = 0;

		// Owner can change based on who's currently wielding the weapon.
		Entity* owner = nullptr;
	};

	inline Weapon::~Weapon() = default;
}
