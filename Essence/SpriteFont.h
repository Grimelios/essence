#pragma once
#include <optional>
#include "GlyphData.h"
#include <glm/vec2.hpp>

namespace Essence
{
	class Texture2D;
	class SpriteFont
	{
	public:

		// The value of 127 covers common English characters, numbers, and punctuation.
		static const int CharacterRange = 127;

		// Using std::optional is required since not all characters within the range will have data defined.
		using GlyphArray = std::array<std::optional<GlyphData>, CharacterRange>;

		SpriteFont(const Texture2D& texture, int size, const GlyphArray& dataArray);

		const GlyphArray& GetGlyphs() const;
		const Texture2D& GetTexture() const;

		int GetSize() const;

		glm::ivec2 Measure(const std::string& value) const;
		glm::ivec2 MeasureLiteral(const std::string& value, glm::ivec2& offset) const;

	private:

		const Texture2D& texture;
		const GlyphArray dataArray;

		int size;
	};
}
