#include "MouseData.h"

namespace Essence
{
	MouseData::MouseData(const glm::vec2& screenPosition, const glm::vec2& worldPosition,
		const glm::vec2& oldScreenPosition, const glm::vec2& oldWorldPosition, const ButtonArray& buttonArray) :

		screenPosition(screenPosition),
		worldPosition(worldPosition),
		oldScreenPosition(oldScreenPosition),
		oldWorldPosition(oldWorldPosition),
		buttonArray(buttonArray)
	{
	}

	glm::vec2 MouseData::GetScreenPosition() const
	{
		return screenPosition;
	}

	glm::vec2 MouseData::GetWorldPosition() const
	{
		return worldPosition;
	}

	glm::vec2 MouseData::GetOldScreenPosition() const
	{
		return oldScreenPosition;
	}

	glm::vec2 MouseData::GetOldWorldPosition() const
	{
		return oldWorldPosition;
	}

	InputStates MouseData::GetState(const int button) const
	{
		return buttonArray[button];
	}

	bool MouseData::AnyButtonPressed() const
	{
		for (InputStates state : buttonArray)
		{
			if (state == InputStates::PressedThisFrame)
			{
				return true;
			}
		}

		return false;
	}

	bool MouseData::Query(const int data, const InputStates state) const
	{
		return (buttonArray[data] & state) == state;
	}
}
