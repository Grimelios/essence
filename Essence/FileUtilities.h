#pragma once
#include <vector>
#include <string>
#include <optional>

namespace Essence::FileUtilities
{
	enum class WriteModes
	{
		Append,
		Overwrite
	};

	std::string ReadAllText(const std::string& filename);
	std::vector<std::string> ReadAllLines(const std::string& filename);
	std::vector<std::string> GetFiles(const std::string& directory, const std::optional<std::string>& extension);

	void SkipLine(std::ifstream& stream);
	void SkipLines(std::ifstream& stream, int count);
	void Write(const std::string& filename, const std::string& value, WriteModes mode = WriteModes::Overwrite);
}
