#include "ScreenControls.h"
#include "JsonUtilities.h"

namespace Essence
{
	ScreenControls::ScreenControls()
	{
		const Json j = JsonUtilities::Load("Controls.json").at("Screens");

		map = ParseBinds(j, "Map");
		pause = ParseBinds(j, "Pause");
	}
}
