#pragma once
#include "IClickable.h"

namespace Essence
{
	class ISelectable : public IClickable
	{
	public:

		virtual ~ISelectable() = default;
		virtual void Select() = 0;
		virtual void Deselect() = 0;
	};
}
