#include "TimerCollection.h"

namespace Essence
{
	void TimerCollection::Update(const float dt)
	{
		if (timers.IsEmpty())
		{
			return;
		}

		auto* node = timers.GetHead();

		while (node != nullptr)
		{
			auto& timer = node->data;
			timer->Update(dt);

			auto* next = node->next;

			if (timer->HasCompleted())
			{
				timers.Remove(node);
			}

			node = next;
		}
	}
}
