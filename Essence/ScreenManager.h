#pragma once
#include "Screen.h"
#include "ScreenControls.h"
#include <memory>

namespace Essence
{
	class AggregateData;
	class ScreenManager
	{
	private:

		using ScreenPointer = std::unique_ptr<Screen>;
		using ScreenList = std::vector<ScreenPointer>;

		static const int MapIndex = static_cast<int>(ScreenTypes::Map);
		static const int PauseIndex = static_cast<int>(ScreenTypes::Pause);
		static const int SettingsIndex = static_cast<int>(ScreenTypes::Settings);
		static const int FileSelectIndex = static_cast<int>(ScreenTypes::FileSelect);

		ScreenList screens;
		Screen* activeScreen = nullptr;
		ScreenControls controls;

		int GetScreenIndexByHotkey(const AggregateData& data) const;

		void OnResize();
		void ProcessInput(const AggregateData& data);

	public:

		Screen* GetActiveScreen() const;

		ScreenManager();

		// Not all screens are accessible at all times. Differentiating between the title screen and gameplay allows only relevant
		// screens to stay in memory. 
		void CreateScreens(bool titleMode);
		void Show(ScreenTypes screenType);
	};
}
