#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include "SpriteText.h"
#include <glm/vec2.hpp>
#include "SingleTimer.h"

namespace Essence
{
	class SpriteFont;
	class SpeechGlyph : public IDynamic, public IRenderable
	{
	private:
		
		SpriteText spriteText;
		SingleTimer timer;

		virtual void Tick(float progress) = 0;

	public:

		SpeechGlyph(const SpriteFont& font, char value, const glm::vec2& position, float revealTime,
			float initialElapsed);

		virtual ~SpeechGlyph() = default;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
