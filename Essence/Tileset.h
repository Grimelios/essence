#pragma once
#include <string>

namespace Essence
{
	class Tileset
	{
	private:

		std::string filename;

		int tileWidth;
		int tileHeight;
		int tileSpacing;

	public:

		Tileset(std::string filename, int tileWidth, int tileHeight, int tileSpacing);

		const std::string& GetFilename() const;

		int GetTileWidth() const;
		int GetTileHeight() const;
		int GetTileSpacing() const;
	};
}
