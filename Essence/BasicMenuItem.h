#pragma once
#include "MenuItem.h"
#include "SpriteText.h"

namespace Essence
{
	class BasicMenuItem : public MenuItem
	{
	private:

		int textWidth;
		int textHeight;

		glm::ivec2 textOffset;

		SpriteText spriteText;

	public:

		BasicMenuItem(const SpriteFont& font, const std::string& value, Alignments alignment, int index, Menu* parent);

		void SetEnabled(bool enabled) override;
		void SetLocation(const glm::ivec2& location) override;
		void Select() override;
		void Deselect() override;
		void Draw(SpriteBatch& sb) override;
	};
}
