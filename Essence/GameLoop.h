#pragma once
#include "IDynamic.h"
#include "IRenderable.h"

namespace Essence
{
	class Camera;
	class SpriteBatch;
	class GameLoop : public IDynamic, public IRenderable
	{
	protected:

		Camera& camera;
		SpriteBatch& sb;

		GameLoop(Camera& camera, SpriteBatch& sb);

	public:

		virtual void Initialize() = 0;
	};
}
