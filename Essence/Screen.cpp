#include "Screen.h"
#include "Container2D.h"

namespace Essence
{
	Screen::Screen(ScreenManager* parent) : parent(parent)
	{
	}

	void Screen::Update(const float dt)
	{
		for (auto* element : elements)
		{
			if (element->visible)
			{
				element->Update(dt);
			}
		}
	}

	void Screen::Draw(SpriteBatch& sb)
	{
		for (auto* element : elements)
		{
			if (element->visible)
			{
				element->Draw(sb);
			}
		}
	}
}
