#include "RainbowMoss.h"
#include "Scene.h"

namespace Essence
{
	RainbowMoss::RainbowMoss() : Entity(EntityGroups::World),
		renderer("")
	{
	}

	void RainbowMoss::Initialize(Scene* scene, EntityLayer* layer)
	{
		renderer.camera = scene->GetCamera();
	}

	void RainbowMoss::Draw(SpriteBatch& sb)
	{
	}
}
