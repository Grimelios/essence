#include "Bed.h"

namespace Essence
{
	Bed::Bed() : Entity(EntityGroups::Objects),
		sprite("Bed.png")
	{
		components.push_back(&sprite);
	}

	int Bed::GetBounceSpeed() const
	{
		return bounceSpeed;
	}
}
