#include "KeyboardData.h"
#include <utility>

namespace Essence
{
	KeyboardData::KeyboardData(std::vector<int> keysHeld, std::vector<KeyPress> keysPressedThisFrame,
		std::vector<int> keysReleasedThisFrame, const KeyArray& keyArray) :
		
		keysHeld(std::move(keysHeld)),
		keysPressedThisFrame(std::move(keysPressedThisFrame)),
		keysReleasedThisFrame(std::move(keysReleasedThisFrame)),
		keyArray(keyArray)
	{
	}

	const std::vector<int>& KeyboardData::GetKeysHeld() const
	{
		return keysHeld;
	}

	const std::vector<KeyPress>& KeyboardData::GetKeysPressedThisFrame() const
	{
		return keysPressedThisFrame;
	}

	const std::vector<int>& KeyboardData::GetKeysReleasedThisFrame() const
	{
		return keysReleasedThisFrame;
	}

	bool KeyboardData::AnyButtonPressed() const
	{
		return !keysPressedThisFrame.empty();
	}

	bool KeyboardData::Query(const int data, const InputStates state) const
	{
		return (keyArray[data] & state) == state;
	}
}
