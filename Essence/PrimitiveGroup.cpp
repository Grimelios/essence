#include "PrimitiveGroup.h"

namespace Essence
{
	PrimitiveGroup::PrimitiveGroup(const GLenum mode) : mode(mode)
	{
	}

	GLenum PrimitiveGroup::GetMode() const
	{
		return mode;
	}

	void PrimitiveGroup::Clear()
	{
		vertices.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
