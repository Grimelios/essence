#pragma once

namespace Essence
{
	enum class PlayerSkills
	{
		Run,
		Jump,
		Ascend,
		Divebomb,
		DoubleJump,
		Count = 5
	};
}
