#pragma once
#include <array>
#include <string>
#include <map>
#include "DoublyLinkedList.h"
#include "SupportedLanguages.h"

namespace Essence
{
	class ILocalizable;
	class Localizer
	{
	private:

		using TargetPair = std::pair<ILocalizable*, std::string>;
		using TargetNode = DoublyLinkedListNode<TargetPair>;
		using TargetList = DoublyLinkedList<TargetPair>;

		using Entry = std::array<std::string, static_cast<int>(SupportedLanguages::Count)>;
		using EntryMap = std::map<std::string, Entry>;

		static int language;
		static TargetList targetList;
		static EntryMap entryMap;

	public:

		static std::string Register(ILocalizable* target, const std::string& key);
		static void Unregister(ILocalizable* target);
		static void SetLanguage(SupportedLanguages language);
	};
}
