#pragma once
#include "LivingEntity.h"
#include "Sprite.h"
#include "PlayerControls.h"
#include "PlayerData.h"
#include "Event.h"
#include "PlayerController.h"
#include <glm/vec2.hpp>
#include "PlayerSkills.h"

namespace Essence
{
	class AggregateData;
	class PlayerHealthDisplay;
	class Player : public LivingEntity
	{
	private:

		friend class PlayerController;

		static const int RunIndex = static_cast<int>(PlayerSkills::Run);
		static const int JumpIndex = static_cast<int>(PlayerSkills::Jump);
		static const int AscendIndex = static_cast<int>(PlayerSkills::Ascend);
		static const int DivebombIndex = static_cast<int>(PlayerSkills::Divebomb);
		static const int DoubleJumpIndex = static_cast<int>(PlayerSkills::DoubleJump);
		static const int TotalSkills = static_cast<int>(PlayerSkills::Count);

		Sprite sprite;
		PlayerData playerData;
		PlayerController controller;

		std::optional<InputBind> jumpBindUsed = std::nullopt;

		int jumpsRemaining = 0;

		bool jumpActive = false;
		bool jumpDecelerating = false;
		bool inWater = false;

		std::array<bool, TotalSkills> skillsUnlocked;
		std::array<bool, TotalSkills> skillsEnabled;

		void OnLanding(Entity* entity);
		bool CheckSkillEnabledOnUnlock(PlayerSkills skill) const;

	protected:

		bool OnCollision(const glm::vec2& normal, Entity* entity) override;

	public:

		Player();

		PlayerHealthDisplay* healthDisplay = nullptr;

		Event<float> onJump;

		void UnlockSkill(PlayerSkills skill);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
