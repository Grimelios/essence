#pragma once
#include "IRenderable.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include "Alignments.h"
#include "SpriteModifiers.h"
#include "Color.h"

namespace Essence
{
	class Component2D : public IRenderable
	{
	protected:

		static const unsigned FloatsPerVertex = 5;
		static const unsigned FloatsPerQuad = FloatsPerVertex * 4;
		static const unsigned SourceIndex = 2;
		static const unsigned ColorIndex = 4;

		Alignments alignment;
		Color color;
		SpriteModifiers mods = SpriteModifiers::None;

		glm::vec2 position = glm::vec2(0);
		glm::vec2 scale = glm::vec2(1);
		glm::vec2 origin = glm::ivec2(0);

		float rotation = 0;

		bool positionChanged = true;
		bool sourceChanged = true;
		bool colorChanged = true;

		explicit Component2D(Alignments alignment);

	public:

		const glm::vec2& GetPosition() const;
		const glm::vec2& GetScale() const;
		const Color& GetColor() const;

		SpriteModifiers GetMods() const;

		float GetRotation() const;

		template<class... Args>
		void SetPosition(const Args&&... args);
		void SetPosition(const glm::vec2& position);

		template<class... Args>
		void SetScale(const Args&&... args);
		void SetScale(const glm::vec2& scale);

		template<class... Args>
		void SetColor(const Args&&... args);
		void SetColor(const Color& color);
		void SetRotation(float rotation);
		void SetOpacity(int opacity);
		void SetMods(SpriteModifiers mods);
	};

	template<class... Args>
	void Component2D::SetPosition(const Args&&... args)
	{
		position = glm::vec2(args...);
		positionChanged = true;
	}

	template<class... Args>
	void Component2D::SetScale(const Args&&... args)
	{
		scale = glm::vec2(args...);
		positionChanged = true;
	}

	template<class... Args>
	void Component2D::SetColor(const Args&&... args)
	{
		color = Color(args...);
		colorChanged = true;
	}
}
