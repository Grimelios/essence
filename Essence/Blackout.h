#pragma once
#include "IRenderable.h"
#include "SingleTimer.h"
#include "UIElement.h"
#include "glm/vec4.hpp"
#include "Bounds.h"

namespace Essence
{
	class Blackout : public UIElement
	{
	private:

		SingleTimer timer;
		Bounds fullscreenBounds;

		glm::vec4 color = glm::vec4(0);

		bool fadingOut = false;

		void Tick(float progress);

	public:

		Blackout();

		void Trigger();
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
