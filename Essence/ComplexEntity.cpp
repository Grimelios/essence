#include "ComplexEntity.h"
#include "Scene.h"
#include "Body.h"
#include "World.h"
#include "Space.h"

namespace Essence
{
	ComplexEntity::ComplexEntity(const EntityGroups group) : Entity(group)
	{
	}

	bool ComplexEntity::OnCollision(const glm::vec2& normal, Entity* entity)
	{
		return true;
	}

	void ComplexEntity::Initialize(Scene* scene, EntityLayer* layer)
	{
		if (shape != nullptr)
		{
			if (usesBody)
			{
				body = scene->GetWorld()->CreateBody(shape.get(), BodyTypes::Dynamic, this);
				body->onCollision = [this](const glm::vec2& normal, Entity* entity)
				{
					return OnCollision(normal, entity);
				};
			}

			if (usesSensor)
			{
				sensor = scene->GetSpace()->CreateSensor(shape.get(), this, SensorTypes::Dynamic);
			}
		}

		Entity::Initialize(scene, layer);
	}

	void ComplexEntity::SetPosition(const glm::vec2& position)
	{
		if (body != nullptr)
		{
			body->position = position;
		}

		Entity::SetPosition(position);
	}

	void ComplexEntity::SetRotation(const float rotation)
	{
		if (body != nullptr)
		{
			body->rotation = rotation;
		}

		Entity::SetRotation(rotation);
	}

	void ComplexEntity::Dispose()
	{
		if (shape != nullptr)
		{
			if (body != nullptr)
			{
				body->Dispose();
			}

			if (sensor != nullptr)
			{
				sensor->Dispose();
			}
		}

		Entity::Dispose();
	}

	void ComplexEntity::Update(const float dt)
	{
		timers.Update(dt);
	}
}
