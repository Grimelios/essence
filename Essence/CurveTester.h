#pragma once
#include "IDynamic.h"
#include "Curve.h"
#include <glm/vec2.hpp>
#include "IRenderable.h"
#include "SpriteText.h"
#include "Sprite.h"

namespace Essence
{
	class CurveTester : public IDynamic, public IRenderable
	{
	private:

		Curve curve;
		Sprite sprite;
		SpriteText spriteText;

		std::vector<glm::vec2>* controlPoints;

		int segmentCount = 1;

	public:

		CurveTester();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
