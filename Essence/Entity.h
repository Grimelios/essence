#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include <glm/vec2.hpp>
#include "nlohmann/json.hpp"
#include "IDisposable.h"
#include "EntityGroups.h"

namespace Essence
{
	class Scene;
	class Component2D;
	class EntityLayer;
	class Entity : public IDynamic, public IRenderable, public IDisposable
	{
	private:

		EntityGroups group = EntityGroups::Unassigned;

	protected:

		// Constructing entities using Json blobs simplifies object creation by the scene.
		using Json = nlohmann::json;

		explicit Entity(EntityGroups group);

		glm::vec2 position = glm::vec2(0);

		float rotation = 0;

		// Note that a list of components is used even though many entities will only contain a single component (such
		// as a sprite). Using this list means that even simple classes don't need to override a bunch of functions.
		std::vector<Component2D*> components;

		Scene* scene = nullptr;
		EntityLayer* layer = nullptr;

		// Note that some entities (like the player) aren't spawned from Json files.
		virtual void Load(const Json& j);

	public:

		virtual ~Entity() = 0;

		glm::vec2 GetPosition() const;

		float GetRotation() const;

		EntityGroups GetGroup() const;

		virtual void Initialize(Scene* scene, EntityLayer* layer);
		virtual void SetPosition(const glm::vec2& position);
		virtual void SetRotation(float rotation);

		void Dispose() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	inline Entity::~Entity() = default;
}
