#pragma once
#include "ControlClass.h"

namespace Essence
{
	class ScreenControls : public ControlClass
	{
	public:

		ScreenControls();

		std::vector<InputBind> map;
		std::vector<InputBind> pause;
	};
}
