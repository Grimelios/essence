#pragma once
#include "IDynamic.h"
#include "IDisposable.h"
#include "RenderTarget.h"
#include "Shader.h"
#include "Sprite.h"

namespace Essence
{
	class KeyboardData;
	class MouseData;
	class WaterTester : public IDynamic, public IDisposable
	{
	private:

		Sprite sprite;
		Sprite stencilSprite;
		Shader spriteShader;
		Shader refractionShader;
		Shader primitiveShader;
		RenderTarget fullscreenTarget;

		GLuint spriteVao = 0;
		GLuint fullscreenVao = 0;
		GLuint primitiveVao = 0;
		GLuint dataBuffer = 0;

		//VertexSpecification refractionSpec;
		//VertexSpecification spriteSpec;
		//VertexSpecification primitiveSpec;

		float factor = 0;
		float cycle = 0;

		void ProcessKeyboard(const KeyboardData& data);
		void ProcessMouse(const MouseData& data);

	public:

		WaterTester();

		void Dispose() override;
		void Update(float dt) override;
		void Draw();
	};
}
