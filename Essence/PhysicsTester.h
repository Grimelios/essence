#pragma once
#include "AxisBox.h"
#include "IDynamic.h"
#include "SpriteText.h"
#include "KeyboardData.h"
#include "Line.h"

namespace Essence
{
	class MouseData;
	class PrimitiveBatch;
	class PhysicsTester : public IDynamic, public IRenderable
	{
	private:

		Line line1;
		Line line2;
		Line result;
		Line vLine;

		glm::vec2 velocity = glm::vec2(0);

		bool overlapping = false;

		void ProcessMouse(const MouseData& data, float dt);
		void ProcessKeyboard(const KeyboardData& data, float dt);
		void Resolve(Line& line1, Line& line2, float dt);

	public:

		PhysicsTester();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
