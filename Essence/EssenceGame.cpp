#include "EssenceGame.h"
#include "GameplayLoop.h"
#include "Resolution.h"

namespace Essence
{
	EssenceGame::EssenceGame(const int width, const int height) : Game("The Essence of Magic", width, height),
		sb(camera)
	{
		glClearColor(0, 0, 0, 1);
	}

	void EssenceGame::Initialize()
	{
		//gameLoop = std::make_unique<GameplayLoop>(camera, sb, pb);
		//gameLoop->Initialize();

		camera.origin = glm::ivec2(Resolution::Width, Resolution::Height) / 2;
	}

	void EssenceGame::Update(const float dt)
	{
		inputProcessor.Update(dt);
		//gameLoop->Update(dt);
		camera.Update(dt);
	}

	void EssenceGame::Draw()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glTester.Draw(sb);
		sb.Flush();

		/*
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gameLoop->Draw(sb, pb);
		pb.ResetZ();
		sb.ResetZ();
		*/
	}
}
