#pragma once

namespace Essence
{
	enum class SensorFlags
	{
		None = 0,
		Player = 1<<0,
		Ascendable = 1<<1
	};

	inline int operator&(const SensorFlags flag1, const SensorFlags flag2)
	{
		return static_cast<int>(flag1) & static_cast<int>(flag2);
	}

	inline SensorFlags operator|(SensorFlags flag1, SensorFlags flag2)
	{
		return static_cast<SensorFlags>(static_cast<int>(flag1) | static_cast<int>(flag2));
	}
}
