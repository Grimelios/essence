#pragma once
#include "ComplexEntity.h"
#include "ITargetable.h"

namespace Essence
{
	class LivingEntity : public ComplexEntity, public ITargetable
	{
	protected:

		int health = 0;
		int maximumHealth = 0;

		explicit LivingEntity(EntityGroups group);

		virtual void OnDeath();

	public:

		virtual ~LivingEntity() = 0;

		void ApplyDamage(int damage) override;
		void Kill();
	};

	inline LivingEntity::~LivingEntity() = default;
}
