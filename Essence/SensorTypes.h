#pragma once

namespace Essence
{
	enum class SensorTypes
	{
		Dynamic,
		Static
	};
}
