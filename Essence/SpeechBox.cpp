#include "SpeechBox.h"
#include "Resolution.h"
#include "GameFunctions.h"
#include "ContentCache.h"

namespace Essence
{
	SpeechBox::SpeechBox(const std::string& font) :
		font(ContentCache::GetFont(font))
	{
		centered = true;
		bounds.width = 400;
		bounds.height = 200;
		spaceWidth = this->font.GetGlyphs()[static_cast<int>(' ')]->GetWidth();

		Container2D::SetLocation(glm::ivec2(Resolution::Width / 2, bounds.height / 2 + 30));
	}

	bool SpeechBox::RevealGlyph(const float time)
	{
		const std::string& line = lines[lineIndex];
		const char c = line[glyphIndex];

		if (glyphIndex == static_cast<int>(line.length()) - 1)
		{
			glyphIndex = 0;

			if (lineIndex == static_cast<int>(lines.size()) - 1)
			{
				return false;
			}

			lineIndex++;
			linePosition.x = 0;
			linePosition.y = 0;
		}
		else
		{
			const glm::vec2 glyphPosition = glm::vec2(linePosition.x + revealedWidth, linePosition.y);

			revealedWidth += font.GetGlyphs()[static_cast<int>(c)]->GetWidth();
			glyphIndex++;

			if (line[glyphIndex] == ' ')
			{
				revealedWidth += spaceWidth;
				glyphIndex++;
			}
		}

		return true;
	}

	void SpeechBox::Refresh(const std::string& value)
	{
		lines = GameFunctions::WrapLines(value, font, bounds.width);
		lineIndex = 0;
	}

	void SpeechBox::Draw(SpriteBatch& sb)
	{
		for (auto& glyph : glyphs)
		{
			glyph->Draw(sb);
		}

		Container2D::Draw(sb);
	}
}
