#include "RNG.h"

namespace Essence
{
	RNG::Generator RNG::generator;
	RNG::FloatDistribution RNG::floatDistribution;
	RNG::BoolDistribution RNG::boolDistribution;

	void RNG::Initialize()
	{
		// See https://stackoverflow.com/questions/288739/generate-random-numbers-uniformly-over-an-entire-range.
		std::random_device device;

		generator = Generator(device());
		floatDistribution = FloatDistribution(0.0f, 1.0f);
		boolDistribution = BoolDistribution(0, 1);
	}

	int RNG::GetInt(const int limit)
	{
		// Limit is exclusive in this function (i.e. return a random number up to, but not including, the limit).
		return static_cast<int>(floatDistribution(generator) * static_cast<float>(limit - 1));
	}

	int RNG::GetInt(const int min, const int max)
	{
		// Min and max are inclusive.
		return static_cast<int>(floatDistribution(generator) * static_cast<float>(max - min + 1)) + min;
	}

	bool RNG::GetBool()
	{
		return boolDistribution(generator) == 1;
	}
}
