#include "RopeTester.h"
#include "Messaging.h"
#include "MouseData.h"
#include "GameFunctions.h"

namespace Essence
{
	RopeTester::RopeTester() :
		rope(40, 20, true)
	{
		auto& points = rope.GetVerletPoints();
		points[0].fixed = true;
		points.back().fixed = true;

		Messaging::Subscribe(MessageTypes::Mouse, [this](const std::any& data, const float dt)
		{
			ProcessMouse(std::any_cast<MouseData>(data));
		});
	}

	void RopeTester::ProcessMouse(const MouseData& data)
	{
		const auto& points = rope.GetVerletPoints();
		const glm::vec2 p1 = data.GetScreenPosition();

		glm::vec2 p2;

		float closestSquared = std::numeric_limits<float>::max();

		for (int i = 0; i < static_cast<int>(points.size()) - 1; i++)
		{
			glm::vec2 result;

			const float squared = GameFunctions::ComputeDistanceSquaredToLine(points[i].position,
				points[i + 1].position, p1, result);

			if (squared < closestSquared)
			{
				closestSquared = squared;
				p2 = result;
			}
		}

		resultLine.SetEndpoints(p1, p2);
	}

	float rotation = 0;

	void RopeTester::Update(const float dt)
	{
		rope.Update(dt);

		auto& points = rope.GetVerletPoints();

		rotation += dt;
		points[0].position = glm::vec2(150, 200) + GameFunctions::ComputeDirection(rotation) * 50.0f;
		points.back().position = glm::vec2(650, 150) + GameFunctions::ComputeDirection(-rotation * 1.5f + 3.14159f) * 
			50.0f;
	}

	void RopeTester::Draw(SpriteBatch& sb)
	{
		const auto& points = rope.GetVerletPoints();

		for (int i = 0; i < static_cast<int>(points.size()) - 1; i++)
		{
			//sb.DrawLine(points[i].position, points[i + 1].position, glm::vec4(1));
		}

		const auto& bounds = rope.GetBounds();

		//sb.DrawBounds(bounds, glm::vec4(0.25f, 0.25f, 0.25f, 1.0f));

		if (bounds.Contains(resultLine.position - resultLine.GetHalfVector()))
		{
			//sb.DrawLine(resultLine, glm::vec4(1, 1, 0, 1));
		}
	}
}
