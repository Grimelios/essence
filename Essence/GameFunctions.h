#pragma once
#include <glm/vec2.hpp>
#include <string>
#include <vector>
#include "Alignments.h"

namespace Essence
{
	class Bounds;
	class SpriteFont;

	namespace GameFunctions
	{
		int ComputeSign(float value);

		float ComputeAngle(const glm::vec2& value);
		float ComputeAngle(const glm::vec2& start, const glm::vec2& end);
		float ComputeDistanceToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p, glm::vec2& result);
		float ComputeDistanceSquaredToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
			glm::vec2& result);

		float ComputeLengthSquared(const glm::vec2& value);
		float ComputeDistanceSquared(const glm::vec2& p1, const glm::vec2& p2);

		bool IsInteger(const std::string& s);

		std::string ToString(const glm::vec2& value);
		std::string ToString(const Bounds& bounds);
		std::vector<std::string> WrapLines(const std::string& value, const SpriteFont& font, int width);

		glm::vec2 ParseVec2(const std::string& s);
		std::vector<glm::vec2> ParseVec2List(const std::string& s);

		glm::vec2 ComputeDirection(float angle);
		glm::vec2 ComputeOrigin(int width, int height, Alignments alignment);
	}
}
