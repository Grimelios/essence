#include "GameplayLoop.h"
#include "SpriteBatch.h"
#include "Player.h"
#include "Jumper.h"
#include "Tmx.h"
#include "Camera.h"

namespace Essence
{
	GameplayLoop::GameplayLoop(Camera& camera, SpriteBatch& sb) : GameLoop(camera, sb),
		scene(&world, &space, &camera),
		accumulator(&world)
	{
	}

	void GameplayLoop::Initialize()
	{
		Tmx::Convert();

		Player* player = scene.Add<Player>(0);
		player->UnlockSkill(PlayerSkills::Run);
		player->UnlockSkill(PlayerSkills::Jump);
		player->UnlockSkill(PlayerSkills::Ascend);
		player->SetPosition(glm::vec2(200));
		
		scene.LoadFragment("ThroneRoom0.json");

		camera.mode = CameraModes::Follow;
		camera.target = player;
	}

	void GameplayLoop::Update(const float dt)
	{
		//curveTester.Update(dt);

		return;

		scene.Update(dt);
		accumulator.Update(dt);
		space.Advance();
		headsUpDisplay.Update(dt);

		auto* screen = screenManager.GetActiveScreen();

		if (screen != nullptr)
		{
			screen->Update(dt);
		}

		if (letterbox.IsVisible())
		{
			letterbox.Update(dt);
		}
	}

	void GameplayLoop::Draw(SpriteBatch& sb)
	{
		//scene.Draw(sb, pb);

		//curveTester.Draw(sb, pb);

		//physicsTester.Draw(sb, pb);
		//headsUpDisplay.Draw(sb, pb);

		auto* screen = screenManager.GetActiveScreen();

		if (screen != nullptr)
		{
			screen->Draw(sb);
		}
		
		if (letterbox.IsVisible())
		{
			letterbox.Draw(sb);
		}
	}
}
