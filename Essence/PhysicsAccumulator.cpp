#include "PhysicsAccumulator.h"
#include "World.h"

namespace Essence
{
	const float PhysicsAccumulator::PhysicsStep = 1.0f / 120;

	PhysicsAccumulator::PhysicsAccumulator(World* world) : world(world)
	{
	}

	void PhysicsAccumulator::Update(const float dt)
	{
		accumulator += dt;

		while (accumulator >= PhysicsStep)
		{
			world->Step(PhysicsStep);
			accumulator -= PhysicsStep;
		}
	}
}
