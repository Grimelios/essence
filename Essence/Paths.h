#pragma once
#include <string>

namespace Essence::Paths
{
	const std::string Content = "Content/";
	const std::string Fonts = "Content/Fonts/";
	const std::string Json = "Content/Json/";
	const std::string Textures = "Content/Textures/";
	const std::string Shaders = "Content/Shaders/";
}
