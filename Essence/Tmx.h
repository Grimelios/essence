#pragma once
#include <string>
#include "rapidxml.hpp"
#include <nlohmann/json.hpp>
#include <map>
#include "EntityTypes.h"

namespace Essence
{
	enum class TiledShapes
	{
		Point,
		Polyline
	};

	class Tmx
	{
	private:

		using XmlAttribute = rapidxml::xml_attribute<>;
		using XmlDocument = rapidxml::xml_document<>;
		using XmlNode = rapidxml::xml_node<>;

		using Json = nlohmann::json;

		static const std::string TmxDirectory;
		static const std::map<std::string, EntityTypes> TypeMap;
		static const std::map<std::string, TiledShapes> ShapeMap;

		static void ParseFragment(const std::string& filename);

		static Json ParseTiles(XmlNode* map, XmlNode* tileset, XmlNode* layer);
		static Json ParseCollisions(XmlNode* group);

		static std::vector<Json> ParseEntities(XmlNode* group);

	public:

		static void Convert();
	};
}
