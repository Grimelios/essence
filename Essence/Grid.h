#pragma once
#include <vector>

namespace Essence
{
	template<class T>
	class Grid
	{
	private:

		int width;

		std::vector<T> list;

	public:

		Grid(int width, int height, T&& value);

		int GetSize() const;

		T& At(int x, int y);
		const T& At(int x, int y) const;

		std::vector<T>& GetList();
	};

	template<class T>
	Grid<T>::Grid(const int width, const int height, T&& value) :
		width(width),
		list(width * height, value)
	{
	}

	template<class T>
	int Grid<T>::GetSize() const
	{
		return static_cast<int>(list.size());
	}

	template<class T>
	T& Grid<T>::At(const int x, const int y)
	{
		return list[y * width + x];
	}

	template<class T>
	const T& Grid<T>::At(const int x, const int y) const
	{
		return list[y * width + x];
	}

	template<class T>
	std::vector<T>& Grid<T>::GetList()
	{
		return list;
	}
}
