#pragma once
#include <string>

namespace Essence
{
	class ILocalizable
	{
	public:

		virtual ~ILocalizable() = default;
		virtual void OnLocalize(const std::string& key, const std::string& value) = 0;
	};
}
