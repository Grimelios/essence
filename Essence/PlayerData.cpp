#include "PlayerData.h"
#include "JsonUtilities.h"

namespace Essence
{
	PlayerData::PlayerData()
	{
		nlohmann::json j = JsonUtilities::Load("Player.json");

		acceleration = j.at("Acceleration").get<float>();
		deceleration = j.at("Deceleration").get<float>();
		maxSpeed = j.at("MaxSpeed").get<float>();
		jumpSpeedInitial = j.at("JumpSpeedInitial").get<float>();
		jumpSpeedLimited = j.at("JumpSpeedLimited").get<float>();
		jumpDeceleration = j.at("JumpDeceleration").get<float>();
	}
}
