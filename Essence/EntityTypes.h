#pragma once

namespace Essence
{
	enum class EntityTypes
	{
		Banner,
		Bed,
		Door,
		Fire,
		Window,
		Tilemap
	};
}
