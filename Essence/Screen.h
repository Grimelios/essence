#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>

namespace Essence
{
	enum class ScreenTypes
	{
		Map,
		Pause,
		Settings,
		FileSelect,
		Count = 4
	};

	class Container2D;
	class ScreenManager;
	class Screen : public IDynamic, public IRenderable
	{
	protected:

		ScreenManager* parent;

		std::vector<Container2D*> elements;

		explicit Screen(ScreenManager* parent);

	public:

		virtual void OnResize(int width, int height) = 0;
		virtual void Show() = 0;
		virtual void Hide() = 0;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
