#include "WaterTester.h"
#include <glm/mat4x4.hpp>
#include "Resolution.h"
#include "Texture2D.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Messaging.h"
#include "KeyboardData.h"
#include "MouseData.h"
#include "GameFunctions.h"
#include <glfw3.h>

namespace Essence
{
	WaterTester::WaterTester() :
		sprite("OriBig.png"),
		stencilSprite("Ori.png"),
		fullscreenTarget(Resolution::Width, Resolution::Height, RenderTargetFlags::Depth)
	{
		const glm::mat4 mvp = glm::ortho(0.0f, static_cast<float>(Resolution::Width), 0.0f,
			static_cast<float>(Resolution::Height));

		// Initialize sprite data
		spriteShader.Attach(ShaderTypes::Vertex, "Sprite.vert");
		spriteShader.Attach(ShaderTypes::Fragment, "Sprite.frag");
		spriteShader.CreateProgram();
		glUseProgram(spriteShader.GetProgram());
		spriteShader.SetUniform("mvp", mvp);

		//spriteSpec.SetBaseOffset(sizeof(float) * 16);
		//spriteSpec.AddAttribute<float>(2, GL_FLOAT, false);
		//spriteSpec.AddAttribute<float>(2, GL_FLOAT, false);
		//spriteSpec.AddAttribute<float>(4, GL_FLOAT, false);

		// Initialize refraction data
		refractionShader.Attach(ShaderTypes::Vertex, "Fullscreen.vert");
		refractionShader.Attach(ShaderTypes::Fragment, "WaterRefraction.frag");
		refractionShader.CreateProgram();
		glUseProgram(refractionShader.GetProgram());
		refractionShader.SetUniform("lightColor", glm::vec3(1));
		refractionShader.SetUniform("radius", 0.4f);
		refractionShader.SetUniform("ambientIntensity", 0.05f);
		refractionShader.SetUniform("halfWindowWidth", Resolution::WindowWidth / 2);

		//refractionSpec.AddAttribute<float>(2, GL_FLOAT, false);
		//refractionSpec.AddAttribute<float>(2, GL_FLOAT, false);

		// Initialize primitive data
		primitiveShader.Attach(ShaderTypes::Vertex, "SimplePrimitives.vert");
		primitiveShader.Attach(ShaderTypes::Fragment, "Primitives.frag");
		primitiveShader.CreateProgram();

		//primitiveSpec.SetBaseOffset(sizeof(float) * 52);
		//primitiveSpec.AddAttribute<float>(2, GL_FLOAT, false);
		//primitiveSpec.AddAttribute<float>(4, GL_FLOAT, false);

		GLuint vaos[3];

		glGenVertexArrays(3, vaos);

		spriteVao = vaos[0];
		fullscreenVao = vaos[1];
		primitiveVao = vaos[2];

		glGenBuffers(1, &dataBuffer);

		// Bind sprite data
		glBindVertexArray(spriteVao);
		glBindBuffer(GL_ARRAY_BUFFER, dataBuffer);

		//spriteSpec.Apply();

		for (unsigned i = 0; i < 3; i++)
		{
			glEnableVertexAttribArray(i);
		}

		// Bind refraction data
		glBindVertexArray(fullscreenVao);
		glBindBuffer(GL_ARRAY_BUFFER, dataBuffer);

		//refractionSpec.Apply();

		for (unsigned i = 0; i < 2; i++)
		{
			glEnableVertexAttribArray(i);
		}

		// Bind primitive data
		glBindVertexArray(primitiveVao);
		glBindBuffer(GL_ARRAY_BUFFER, dataBuffer);

		//primitiveSpec.Apply();

		for (unsigned i = 0; i < 2; i++)
		{
			glEnableVertexAttribArray(i);
		}

		glBindVertexArray(0);

		// Buffer static data
		float data[72] =
		{
			// Fullscreen data
			-1.0f, -1.0f, 0.0f, 0.0f,
			-1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 1.0f, 1.0f,

			// Sprite data
			0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 1080.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1920.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1920.0f, 1080.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,

			// Primitive data
			-0.4f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			0.4f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			-0.4f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			0.4f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f
		};

		// Bind data
		glBindBuffer(GL_ARRAY_BUFFER, dataBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 72, data, GL_STATIC_DRAW);

		Messaging::Subscribe(MessageTypes::Keyboard, [this](const std::any& data, float dt)
		{
			ProcessKeyboard(std::any_cast<KeyboardData>(data));
		});

		Messaging::Subscribe(MessageTypes::Mouse, [this](const std::any& data, float dt)
		{
			ProcessMouse(std::any_cast<MouseData>(data));
		});
	}

	void WaterTester::ProcessKeyboard(const KeyboardData& data)
	{
		if (data.Query(GLFW_KEY_UP, InputStates::PressedThisFrame))
		{
			factor = std::min(1.0f, factor + 0.001f);
		}
		else if (data.Query(GLFW_KEY_DOWN, InputStates::PressedThisFrame))
		{
			factor = std::max(0.0f, factor - 0.001f);
		}

		refractionShader.Apply();
		refractionShader.SetUniform("factor", factor);
	}

	void WaterTester::ProcessMouse(const MouseData& data)
	{
		const glm::vec2& position = data.GetScreenPosition();

		const float pX = position.x / Resolution::WindowWidth;
		const float lightAngle = pX * glm::pi<float>();

		const glm::vec2 direction = GameFunctions::ComputeDirection(lightAngle);

		refractionShader.Apply();
		refractionShader.SetUniform("lightDirection", glm::vec3(direction.x, 0, direction.y));
	}

	void WaterTester::Dispose()
	{
		//sprite.Dispose();
		//stencilSprite.Dispose();
		fullscreenTarget.Dispose();

		GLuint vaos[3] =
		{
			spriteVao,
			fullscreenVao,
			primitiveVao
		};

		glDeleteVertexArrays(3, vaos);
		glDeleteBuffers(1, &dataBuffer);
	}

	void WaterTester::Update(const float dt)
	{
		cycle += dt / 3;

		if (cycle > glm::two_pi<float>())
		{
			cycle -= glm::two_pi<float>();
		}
	}

	void WaterTester::Draw()
	{
		// Draw sprites
		fullscreenTarget.Use();
		glUseProgram(spriteShader.GetProgram());

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_STENCIL_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, Resolution::Width, Resolution::Height);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sprite.GetTexture().GetTextureId());
		glBindVertexArray(spriteVao);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// Draw primitives (and update the stencil buffer)
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glViewport(0, 0, Resolution::WindowWidth, Resolution::WindowHeight);

		glUseProgram(primitiveShader.GetProgram());

		glBindVertexArray(primitiveVao);
		glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilMask(0xFF);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// Draw refraction (to the main screen)
		glUseProgram(refractionShader.GetProgram());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fullscreenTarget.GetTextureId());
		glBindVertexArray(fullscreenVao);
		glStencilFunc(GL_EQUAL, 1, 0xFF);
		glStencilMask(0x00);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}
}
