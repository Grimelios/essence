#include "Entity.h"
#include "Component2D.h"
#include "Scene.h"
#include "GameFunctions.h"
#include "EntityLayer.h"

namespace Essence
{
	Entity::Entity(const EntityGroups group) : group(group)
	{
	}

	void Entity::Load(const Json& j)
	{
		// This assumes that all entities that call this function contain position.
		SetPosition(GameFunctions::ParseVec2(j.at("Position").get<std::string>()));

		// Most entities remain axis-aligned (not rotated).
		const auto i = j.find("Rotation");

		if (i != j.end())
		{
			SetRotation(i->get<float>());
		}
	}

	glm::vec2 Entity::GetPosition() const
	{
		return position;
	}

	float Entity::GetRotation() const
	{
		return rotation;
	}

	EntityGroups Entity::GetGroup() const
	{
		return group;
	}

	void Entity::Initialize(Scene* scene, EntityLayer* layer)
	{
		this->scene = scene;
		this->layer = layer;
	}

	void Entity::SetPosition(const glm::vec2& position)
	{
		this->position = position;

		for (Component2D* c : components)
		{
			//c->position = position;
		}
	}

	void Entity::SetRotation(const float rotation)
	{
		this->rotation = rotation;

		for (Component2D* c : components)
		{
			//c->rotation = rotation;
		}
	}

	void Entity::Dispose()
	{
		layer->Remove(this);
	}

	void Entity::Update(const float dt)
	{
	}

	void Entity::Draw(SpriteBatch& sb)
	{
		for (Component2D* c : components)
		{
			c->Draw(sb);
		}
	}
}
