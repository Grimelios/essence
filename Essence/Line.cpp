#include "Line.h"
#include "GameFunctions.h"
#include <glm/detail/func_geometric.inl>

namespace Essence
{
	Line::Line() : Line(0)
	{
	}

	Line::Line(const float length) : Line(length, 0)
	{
	}

	Line::Line(const float length, const float rotation) : Shape(rotation, ShapeTypes::Line)
	{
		SetLength(length);
	}

	Line::Line(const glm::vec2& start, const glm::vec2& end) : Shape(ShapeTypes::Line)
	{
		SetEndpoints(start, end);
	}

	const glm::vec2& Line::GetHalfVector() const
	{
		return halfVector;
	}

	float Line::GetLength() const
	{
		return length;
	}

	float Line::GetHalfLength() const
	{
		return length / 2;
	}

	void Line::SetLength(const float length)
	{
		this->length = length;

		RecomputeHalfVector();
	}

	void Line::SetRotation(const float rotation)
	{
		this->rotation = rotation;

		RecomputeHalfVector();
	}

	void Line::SetEndpoints(const glm::vec2& start, const glm::vec2& end)
	{
		length = distance(start, end);
		rotation = GameFunctions::ComputeAngle(start, end);
		position = (start + end) / 2.0f;

		RecomputeHalfVector();
	}

	void Line::RecomputeHalfVector()
	{
		halfVector = GameFunctions::ComputeDirection(rotation) * length / 2.0f;
	}
}
