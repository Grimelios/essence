#include "SpriteFont.h"
#include "Texture2D.h"
#include <glm/vec2.hpp>

namespace Essence
{
	SpriteFont::SpriteFont(const Texture2D& texture, const int size, const GlyphArray& dataArray) :
		texture(texture),
		dataArray(dataArray),
		size(size)
	{
	}

	const SpriteFont::GlyphArray& SpriteFont::GetGlyphs() const
	{
		return dataArray;
	}

	const Texture2D& SpriteFont::GetTexture() const
	{
		return texture;
	}

	int SpriteFont::GetSize() const
	{
		return size;
	}

	glm::ivec2 SpriteFont::Measure(const std::string& value) const
	{
		if (value.empty())
		{
			return glm::ivec2(0, size);
		}

		int sumWidth = 0;
		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length - 1; i++)
		{
			sumWidth += dataArray[value[i]]->GetAdvance();
		}

		sumWidth += dataArray[value[length - 1]]->GetWidth();

		return glm::ivec2(sumWidth, size);
	}

	glm::ivec2 SpriteFont::MeasureLiteral(const std::string& value, glm::ivec2& offset) const
	{
		if (value.empty())
		{
			return glm::ivec2(0);
		}

		int sumWidth = 0;
		int top = INT_MAX;
		int bottom = 0;

		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length; i++)
		{
			const GlyphData& data = dataArray[value[i]].value();
			
			const int x = data.GetOffset().x;
			const int y = data.GetOffset().y;
			const int advance = data.GetAdvance();

			if (i == 0)
			{
				offset.x = x;
				sumWidth += advance - x;
			}
			else if (i == length - 1)
			{
				sumWidth += x + data.GetWidth();
			}
			else
			{
				sumWidth += advance;
			}

			top = std::min(top, y);
			bottom = std::max(bottom, y + data.GetHeight());
		}

		offset.y = top;

		return glm::ivec2(sumWidth, bottom - top);
	}
}
