#pragma once
#include "Entity.h"
#include "IInteractive.h"

namespace Essence
{
	class Door : public Entity, public IInteractive
	{
	private:

		using Json = nlohmann::json;

		int linkedDoor;

		std::string linkedFragment;

	public:

		explicit Door(const Json& p);

		int GetLinkedDoor() const;

		const std::string& GetLinkedFragment() const;

		void OnInteract(Player* player) override;
	};
}
