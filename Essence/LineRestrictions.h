#pragma once

namespace Essence
{
	enum class LineRestrictions
	{
		RestrictToX,
		RestrictToY,
		None
	};
}
