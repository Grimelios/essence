#include "ClothTester.h"
#include "Messaging.h"
#include "MouseData.h"

namespace Essence
{
	ClothTester::ClothTester() : cloth(4, 4, 60)
	{
		auto& points = cloth.GetPoints();
		
		const int width = cloth.GetWidth();
		const int height = cloth.GetHeight();

		points.At(0, 0).fixed = true;
		points.At(0, 0).position = glm::vec2(150, 100);
		//points.At(width - 1, 0).fixed = true;
		//points.At(width - 1, 0).position = glm::vec2(300, 250);

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				//points.At(j, i).position = glm::vec2(100 + 60 * j, 100 + 60 * i);
				//points.At(j, i).previousPosition = points.At(j, i).position;
			}
		}

		Messaging::Subscribe(MessageTypes::Mouse, [this](const std::any& data, const float dt)
		{
			HandleMouse(std::any_cast<MouseData>(data));
		});
	}

	void ClothTester::HandleMouse(const MouseData& data)
	{
	}

	void ClothTester::Update(const float dt)
	{
		cloth.Update(dt);
	}

	void ClothTester::Draw(SpriteBatch& sb)
	{
		const auto& points = cloth.GetPoints();

		const int width = cloth.GetWidth();
		const int height = cloth.GetHeight();

		//const glm::vec4 color(1);

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				const glm::vec2& p = points.At(j, i).position;

				// Draw horizontal lines
				if (j < width - 1)
				{
					//pb.DrawLine(p, points.At(j + 1, i).position, color);
				}

				// Draw vertical lines
				if (i < height - 1)
				{
					//pb.DrawLine(p, points.At(j, i + 1).position, color);
				}

				// Draw down-right diagonal lines
				if (j < width - 1 && i < height - 1)
				{
					//pb.DrawLine(p, points.At(j + 1, i + 1).position, color);
				}

				// Draw down-left diagonal lines
				if (j > 0 && i < height - 1)
				{
					//pb.DrawLine(p, points.At(j - 1, i + 1).position, color);
				}
			}
		}
		
		// Draw right-side vertical lines
		for (int i = 0; i < height - 1; i++)
		{
			//pb.DrawLine(points.At(width - 1, i).position, points.At(width - 1, i + 1).position, color);
		}

		// Draw bottom horizontal lines
		for (int j = 0; j < width - 1; j++)
		{
			//pb.DrawLine(points.At(j, height - 1).position, points.At(j + 1, height - 1).position, color);
		}
	}
}
