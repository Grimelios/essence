#pragma once
#include <string>
#include <vector>

namespace Essence::StringUtilities
{
	std::vector<std::string> Split(const std::string& s, char delimeter, bool removeEmpty = false);
	std::string RemoveExtension(const std::string& s);
	
	int IndexOf(const std::string& s, char c);
	int IndexOf(const std::string& s, char c, int start);
	int IndexOf(const std::string& s, const std::string& value);
	int IndexOf(const std::string& s, const std::string& value, int start);
	int LastIndexOf(const std::string& s, char c);

	bool EndsWith(const std::string& s, const std::string& end);
}
