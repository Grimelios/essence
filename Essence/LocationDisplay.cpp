#include "LocationDisplay.h"

namespace Essence
{
	LocationDisplay::LocationDisplay(const int offsetX, const int offsetY, const Alignments alignment) :
		HudElement(offsetX, offsetY, alignment),

		spriteText("Basic")
	{
		components.push_back(&spriteText);
	}

	void LocationDisplay::SetLocation(const glm::ivec2& location)
	{
		spriteText.position = location;
	}
}
