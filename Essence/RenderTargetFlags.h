#pragma once

namespace Essence
{
	enum class RenderTargetFlags
	{
		None,
		Depth,
		Stencil,
		DepthStencil
	};
}
