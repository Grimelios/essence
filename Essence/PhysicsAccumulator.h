#pragma once
#include "IDynamic.h"

namespace Essence
{
	class World;
	class PhysicsAccumulator : public IDynamic
	{
	private:

		static const float PhysicsStep;

		float accumulator = 0;

		World* world;

	public:

		explicit PhysicsAccumulator(World* world);

		void Update(float dt) override;
	};
}
