#include "Rain.h"
#include "Resolution.h"
#include "SpriteBatch.h"

namespace Essence
{
	Rain::Rain() : target(Resolution::Width, Resolution::Height, RenderTargetFlags::None)
	{
	}

	void Rain::Update(const float dt)
	{
		for (int i = 0; i < 3; i++)
		{
		}

		for (auto& drop : raindrops)
		{
			drop += velocity * dt;
		}
	}

	void Rain::Draw(SpriteBatch& sb)
	{
	}
}
