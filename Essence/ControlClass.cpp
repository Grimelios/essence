#include "ControlClass.h"

namespace Essence
{
	std::vector<InputBind> ControlClass::ParseBinds(const Json& j, const std::string& key)
	{
		std::vector<Json> blocks = j.at(key);
		std::vector<InputBind> binds;
		binds.reserve(blocks.size());

		for (const Json& block : blocks)
		{
			const InputTypes type = block.at("InputType").get<InputTypes>();
			const int data = block.at("Data").get<int>();

			binds.emplace_back(type, data);
		}

		return binds;
	}
}
