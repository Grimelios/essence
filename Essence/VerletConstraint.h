#pragma once
#include <glm/vec2.hpp>

namespace Essence
{
	class VerletPoint;
	class VerletConstraint
	{
	private:

		VerletPoint& p1;
		VerletPoint& p2;

		float segmentLength;
		int endpoint;
		bool shouldConstrain;

		glm::vec2 offset;

	public:

		VerletConstraint(VerletPoint& p1, VerletPoint& p2, float segmentLength, int endpoint);

		void Compute();
		void Resolve() const;
	};
}
