#pragma once
#include "HudElement.h"
#include "SpriteText.h"
#include "ILocalizable.h"

namespace Essence
{
	class ZoneDisplay : public HudElement, public ILocalizable
	{
	private:

		SpriteText spriteText;

	public:

		ZoneDisplay(int offsetX, int offsetY, Alignments alignment);

		void OnLocalize(const std::string& key, const std::string& value) override;
		void SetLocation(const glm::ivec2& location) override;
		void Dispose() override;
	};
}
