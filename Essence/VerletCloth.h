#pragma once
#include "IDynamic.h"
#include "VerletPoint.h"
#include "Grid.h"
#include "VerletConstraint.h"

namespace Essence
{
	class VerletCloth : public IDynamic
	{
	private:

		int width;
		int height;

		// Storing segment length as a float rather than an int prevents needing to cast a few times.
		float segmentLength;
		float crossSegmentLength;

		Grid<VerletPoint> points;
		std::vector<VerletConstraint> constraints;

	public:
		
		VerletCloth(int width, int height, int segmentLength);

		int GetWidth() const;
		int GetHeight() const;

		Grid<VerletPoint>& GetPoints();

		void Update(float dt) override;
	};
}
