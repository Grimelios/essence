#pragma once
#include "GameLoop.h"

namespace Essence
{
	class SplashLoop : public GameLoop
	{
	public:

		SplashLoop(Camera& camera, SpriteBatch& sb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};
}
