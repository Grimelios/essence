#include "Menu.h"
#include "AggregateData.h"
#include "KeyboardData.h"
#include "MouseData.h"

namespace Essence
{
	MenuControls Menu::controls;

	Menu::Menu(const int spacing) : spacing(spacing)
	{
	}

	void Menu::Initialize(ItemList& items, const int selectedIndex)
	{
		this->items = std::move(items);

		for (auto& item : this->items)
		{
			itemSet.items.push_back(item.get());
		}

		SetSelectedIndex(selectedIndex);
	}

	void Menu::SetLocation(const glm::ivec2& location)
	{
		for (int i = 0; i < static_cast<int>(items.size()); i++)
		{
			items[i]->SetLocation(location + glm::ivec2(0, spacing * i));
		}
	}

	void Menu::SetSelectedIndex(const int index)
	{
		if (selectedIndex != -1)
		{
			items[selectedIndex]->Deselect();
		}

		selectedIndex = index;
		items[selectedIndex]->Select();
	}

	void Menu::ProcessInput(const AggregateData& data)
	{
		// The mouse is intentionally processed first since, when used, it overrides other input sources (like keyboards and
		// controllers).
		itemSet.ProcessMouse(data.GetData<MouseData>(InputTypes::Mouse));

		// Successfully clicking an item implies the mouse was also hovered over that item (meaning that other menu actions can be
		// ignored this frame).
		if (itemSet.WasItemClickedThisFrame())
		{
			return;
		}

		if (data.Query(controls.submit, InputStates::PressedThisFrame))
		{
			Submit(selectedIndex);
		}

		const bool up = data.Query(controls.up, InputStates::PressedThisFrame);
		const bool down = data.Query(controls.down, InputStates::PressedThisFrame);;

		if (up ^ down)
		{
			auto& previousItem = items[selectedIndex];

			if (up)
			{
				do
				{
					selectedIndex = selectedIndex == 0 ? static_cast<int>(items.size()) - 1 : --selectedIndex;
				}
				while (!items[selectedIndex]->IsEnabled());
			}
			else
			{
				do
				{
					selectedIndex = selectedIndex == items.size() - 1 ? 0 : ++selectedIndex;
				}
				while (!items[selectedIndex]->IsEnabled());
			}

			previousItem->Deselect();

			auto& selectedItem = items[selectedIndex];

			selectedItem->Select();
			itemSet.hoveredItem = selectedItem.get();
		}
	}
}
