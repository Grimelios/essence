#pragma once
#include "SingleTimer.h"
#include "IDisposable.h"

namespace Essence
{
	class Shape;
	class Space;
	class Sensor;
	class Entity;
	class ITargetable;
	class DamageSource : public IDynamic, public IDisposable
	{
	private:

		Entity* source;
		Sensor* sensor;
		
		int damage;

		// The timer is optional to accomodate unlimited damage sources (such as static hazards).
		std::optional<SingleTimer> timer;

	public:

		DamageSource(int damage, int lifetime, Space* space, Shape* shape, Entity* source);

		bool HasExpired() const;

		void Dispose() override;
		void Update(float dt) override;
	};
}
