#pragma once
#include "Game.h"
#include <memory>
#include "GameLoop.h"
#include "SpriteBatch.h"
#include "ScreenManager.h"
#include "GLTester.h"

namespace Essence
{
	class EssenceGame : public Game
	{
	private:

		GLTester glTester;
		SpriteBatch sb;
		ScreenManager screenManager;

		std::unique_ptr<GameLoop> gameLoop = nullptr;

	protected:

		void Update(float dt) override;
		void Draw() override;

	public:

		EssenceGame(int width, int height);

		void Initialize() override;
	};
}
