#include "BasicMenuItem.h"
#include "SpriteFont.h"

namespace Essence
{
	BasicMenuItem::BasicMenuItem(const SpriteFont& font, const std::string& value, const Alignments alignment,
		const int index, Menu* parent) :

		MenuItem(index, parent),

		spriteText(font, value, alignment)
	{
		const glm::ivec2 dimensions = font.MeasureLiteral(value, textOffset);

		textWidth = dimensions.x;
		textHeight = dimensions.y;
		centered = alignment == Alignments::Center;
		spriteText.SetColor(100);
	}

	void BasicMenuItem::SetEnabled(const bool enabled)
	{
		this->enabled = enabled;

		if (!enabled)
		{
			spriteText.SetColor(25);
		}
	}

	void BasicMenuItem::SetLocation(const glm::ivec2& location)
	{
		spriteText.SetPosition(glm::vec2(location.x, location.y));

		// Dimensions must be reset here in order to correctly expand the bounds below. Expanding bounds makes it a bit
		// easier to click on text (especially if the text is relatively small).
		bounds.width = textWidth;
		bounds.height = textHeight;

		if (centered)
		{
			bounds.SetCenter(location);
		}
		else
		{
			bounds.SetLocation(location + textOffset);
		}

		bounds.Expand(2);
	}

	void BasicMenuItem::Select()
	{
		spriteText.SetColor(255);
	}

	void BasicMenuItem::Deselect()
	{
		spriteText.SetColor(100);
	}

	void BasicMenuItem::Draw(SpriteBatch& sb)
	{
		spriteText.Draw(sb);
	}
}
