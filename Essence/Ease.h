#pragma once

namespace Essence
{
	enum class EaseTypes
	{
		Linear,
		QuadraticIn,
		QuadraticOut,
		CubicIn,
		CubicOut
	};

	class Ease
	{
	private:

		static float QuadraticIn(float t);
		static float QuadraticOut(float t);
		static float CubicIn(float t);
		static float CubicOut(float t);

	public:

		static float Compute(EaseTypes type, float t);
	};
}
