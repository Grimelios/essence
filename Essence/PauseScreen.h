#pragma once
#include "Screen.h"
#include "PauseMenu.h"

namespace Essence
{
	class PauseScreen : public Screen
	{
	private:

		PauseMenu menu;

	public:

		explicit PauseScreen(ScreenManager* parent);
		
		void OnResize(int width, int height) override;
		void Show() override;
		void Hide() override;
		void Unpause();
	};
}
