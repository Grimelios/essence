#pragma once
#include "Body.h"
#include "PhysicsHelper.h"
#include "DoublyLinkedList.h"

namespace Essence
{
	class Shape;
	class Entity;
	class World
	{
	private:

		using BodyList = DoublyLinkedList<Body>;
		using BodyNode = DoublyLinkedListNode<Body>;

		BodyList bodies;
		PhysicsHelper helper;

	public:

		Body* CreateBody(Shape* shape, BodyTypes bodyType, Entity* owner);

		float gravity = 1000;

		void Remove(Body* body);
		void Step(float dt) const;
	};
}
