#pragma once
#include <cstddef>

namespace Essence
{
	class Color
	{
	public:

		static const Color White;
		static const Color Black;
		static const Color Red;
		static const Color Green;
		static const Color Blue;

		Color();
		explicit Color(int value);
		Color(int r, int g, int b);
		Color(int r, int g, int b, int a);

		std::byte r;
		std::byte g;
		std::byte b;
		std::byte a;

		float ToFloat() const;
	};
}
