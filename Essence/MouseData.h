#pragma once
#include <glm/vec2.hpp>
#include "InputData.h"
#include <glad/glad.h>
#include <glfw3.h>
#include <array>

namespace Essence
{
	enum class MouseButtons
	{
		Left,
		Right,
		Middle
	};

	class MouseData : public InputData
	{
	private:

		using ButtonArray = std::array<InputStates, GLFW_MOUSE_BUTTON_LAST>;

		glm::vec2 screenPosition;
		glm::vec2 worldPosition;
		glm::vec2 oldScreenPosition;
		glm::vec2 oldWorldPosition;

		ButtonArray buttonArray;

	public:

		MouseData(const glm::vec2& screenPosition, const glm::vec2& worldPosition, const glm::vec2& oldScreenPosition,
			const glm::vec2& oldWorldPosition, const ButtonArray& buttonArray);

		glm::vec2 GetScreenPosition() const;
		glm::vec2 GetWorldPosition() const;
		glm::vec2 GetOldScreenPosition() const;
		glm::vec2 GetOldWorldPosition() const;

		InputStates GetState(int button) const;

		bool AnyButtonPressed() const override;
		bool Query(int data, InputStates state) const override;
	};
}
