#include "Door.h"

namespace Essence
{
	Door::Door(const Json& p) : Entity(EntityGroups::Objects),

		linkedDoor(p.at("LinkedDoor").get<int>()),
		linkedFragment(p.at("LinkedFragment").get<std::string>())
	{
	}

	int Door::GetLinkedDoor() const
	{
		return linkedDoor;
	}

	const std::string& Door::GetLinkedFragment() const
	{
		return linkedFragment;
	}

	void Door::OnInteract(Player* player)
	{
	}
}
