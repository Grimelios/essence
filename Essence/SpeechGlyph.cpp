#include "SpeechGlyph.h"

namespace Essence
{
	SpeechGlyph::SpeechGlyph(const SpriteFont& font, const char value, const glm::vec2& position,
		const float revealTime, const float initialElapsed) :

		spriteText(font, std::string(1, value)),
		timer(revealTime, std::nullopt, initialElapsed)
	{
		spriteText.SetPosition(position);
	}

	void SpeechGlyph::Update(const float dt)
	{
	}

	void SpeechGlyph::Draw(SpriteBatch& sb)
	{
		spriteText.Draw(sb);
	}
}
