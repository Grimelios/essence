#pragma once
#include <glm/vec2.hpp>

namespace Essence
{
	class IClickable
	{
	public:

		virtual ~IClickable() = default;

		virtual bool Contains(const glm::vec2& mousePosition) = 0;
		virtual bool IsEnabled() const = 0;

		virtual void OnHover(const glm::vec2& mousePosition) = 0;
		virtual void OnUnhover() = 0;
		virtual void OnMouseMove(const glm::vec2& mousePosition) = 0;
		virtual void OnClick(const glm::vec2& mousePosition) = 0;
	};
}
