#pragma once

namespace Essence
{
	enum class WindowModes
	{
		Windowed,
		WindowedFullscreen,
		Fullscreen,
	};
}
