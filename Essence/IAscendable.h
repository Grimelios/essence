#pragma once
#include <vector>
#include <glm/vec2.hpp>

namespace Essence
{
	class IAscendable
	{
	public:

		virtual ~IAscendable() = default;
		virtual std::vector<glm::vec2> GetPoints() const = 0;
	};
}
