#pragma once
#include <glm/vec2.hpp>

namespace Essence
{
	class VerletPoint
	{
	public:

		bool fixed = false;

		int index = -1;

		// These values are intentionally kept public even though it technically means you could set position without
		// setting previous position.
		glm::vec2 position;
		glm::vec2 previousPosition;
	};
}
