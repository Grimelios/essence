#pragma once
#include "Screen.h"
#include "SaveSlot.h"

namespace Essence
{
	class FileSelectScreen : public Screen
	{
	private:

		std::vector<SaveSlot> saveSlots;

	public:

		explicit FileSelectScreen(ScreenManager* parent);

		void OnResize(int width, int height) override;
		void Show() override;
		void Hide() override;
	};
}
