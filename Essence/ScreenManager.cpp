#include "ScreenManager.h"
#include "Messaging.h"
#include "AggregateData.h"
#include <glfw3.h>
#include "MapScreen.h"
#include "PauseScreen.h"
#include "Resolution.h"
#include "FileSelectScreen.h"

namespace Essence
{
	ScreenManager::ScreenManager()
	{
		const int count = static_cast<int>(ScreenTypes::Count);

		screens.reserve(count);

		// All screens begin as null pointers, then are properly initialized based on the current gamestate (since not
		// all screens are relevant in all states).
		for (int i = 0; i < count; i++)
		{
			screens.push_back(nullptr);
		}

		controls.map.emplace_back(InputTypes::Keyboard, GLFW_KEY_M);
		controls.pause.emplace_back(InputTypes::Keyboard, GLFW_KEY_ESCAPE);

		OnResize();

		Messaging::Subscribe(MessageTypes::Input, [this](const std::any& data, const float dt)
		{
			ProcessInput(std::any_cast<AggregateData>(data));
		});

		Messaging::Subscribe(MessageTypes::Resize, [this](const std::any& data, const float dt)
		{
			OnResize();
		});
	}

	void ScreenManager::OnResize()
	{
		const int width = Resolution::Width;
		const int height = Resolution::Height;

		for (auto& screen : screens)
		{
			if (screen != nullptr)
			{
				screen->OnResize(width, height);
			}
		}
	}

	void ScreenManager::ProcessInput(const AggregateData& data)
	{
		const int index = GetScreenIndexByHotkey(data);

		if (index == -1)
		{
			return;
		}

		Screen* screen = screens[index].get();

		if (activeScreen == nullptr)
		{
			activeScreen = screen;
			activeScreen->Show();

			return;
		}

		if (screen == activeScreen)
		{
			activeScreen->Hide();
			activeScreen = nullptr;
		}
		else
		{
			activeScreen->Hide();
			activeScreen = screen;
			activeScreen->Show();
		}
	}

	int ScreenManager::GetScreenIndexByHotkey(const AggregateData& data) const
	{
		if (data.Query(controls.map, InputStates::PressedThisFrame))
		{
			return static_cast<int>(ScreenTypes::Map);
		}
		
		if (data.Query(controls.pause, InputStates::PressedThisFrame))
		{
			return static_cast<int>(ScreenTypes::Pause);
		}

		return -1;
	}

	Screen* ScreenManager::GetActiveScreen() const
	{
		return activeScreen;
	}

	void ScreenManager::CreateScreens(const bool titleMode)
	{
		for (auto& screen : screens)
		{
			if (screen != nullptr)
			{
				screen.reset();
			}
		}

		if (titleMode)
		{
			screens[FileSelectIndex] = std::make_unique<FileSelectScreen>(this);
		}
		else
		{
			screens[MapIndex] = std::make_unique<MapScreen>(this);
			screens[PauseIndex] = std::make_unique<PauseScreen>(this);
		}
	}

	void ScreenManager::Show(const ScreenTypes screenType)
	{
		activeScreen = screens[static_cast<int>(screenType)].get();
		activeScreen->Show();
	}
}
