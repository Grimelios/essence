#pragma once
#include "Menu.h"

namespace Essence
{
	class TitleMenu : public Menu
	{
	public:

		TitleMenu();

		void Submit(int index) override;
	};
}
