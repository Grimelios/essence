#pragma once

namespace Essence
{
	class SpriteBatch;
	class IRenderable
	{
	public:

		virtual ~IRenderable() = default;
		virtual void Draw(SpriteBatch& sb) = 0;
	};
}
