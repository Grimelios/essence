#include "PauseScreen.h"

namespace Essence
{
	PauseScreen::PauseScreen(ScreenManager* parent) : Screen(parent),
		menu(this)
	{
		elements.push_back(&menu);
	}

	void PauseScreen::OnResize(const int width, const int height)
	{
		menu.SetLocation(glm::vec2(width / 2, 50));
	}

	void PauseScreen::Show()
	{
	}

	void PauseScreen::Hide()
	{
	}

	void PauseScreen::Unpause()
	{
	}
}
