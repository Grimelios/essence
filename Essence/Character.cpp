#include "Character.h"
#include <utility>

namespace Essence
{
	Character::Character(std::string name) : LivingEntity(EntityGroups::Characters),
		name(std::move(name))
	{
	}

	const std::string& Character::GetName() const
	{
		return name;
	}
}
