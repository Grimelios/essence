#include "ZoneDisplay.h"
#include "Localizer.h"

namespace Essence
{
	ZoneDisplay::ZoneDisplay(const int offsetX, const int offsetY, const Alignments alignment) :
		HudElement(offsetX, offsetY, alignment),

		spriteText("Basic", Alignments::Center)
	{
		components.push_back(&spriteText);
		spriteText.SetValue(Localizer::Register(this, "SnowyPeak"));
	}

	void ZoneDisplay::OnLocalize(const std::string& key, const std::string& value)
	{
		spriteText.SetValue(value);
	}

	void ZoneDisplay::SetLocation(const glm::ivec2& location)
	{
		spriteText.SetPosition(location);
	}

	void ZoneDisplay::Dispose()
	{
		Localizer::Unregister(this);
	}
}
