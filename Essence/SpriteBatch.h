#pragma once
#include <array>
#include "Shader.h"
#include <glm/mat4x4.hpp>
#include <glad/glad.h>

namespace Essence
{
	enum class BatchCoords
	{
		Screen,
		World
	};

	class Color;
	class Bounds;
	class Camera;
	class Texture2D;
	class SpriteBatch
	{
	private:

		static const unsigned BufferSize = 4096;
		static const unsigned IndexBufferSize = 1024;
		static const unsigned short RestartIndex = 65535;

		using IndexList = std::vector<unsigned short>;

		Shader spriteShader;
		Shader primitiveShader;
		Shader* activeShader = nullptr;

		const Camera& camera;

		GLuint bufferId = 0;
		GLuint indexBufferId = 0;
		GLuint activeTexture = 0;
		GLenum mode = 0;

		glm::mat4 projection = glm::mat4(0);
		glm::mat4 screenMatrix = glm::mat4(0);

		std::array<std::byte, BufferSize> buffer { };
		std::array<unsigned short, IndexBufferSize> indexBuffer { };
		std::byte* currentPointer = &buffer[0];

		unsigned bufferSize = 0;

		// Using two variables allows for primitive restarting.
		unsigned indexCount = 0;
		unsigned indexStart = 0;

		BatchCoords coords = BatchCoords::Screen;

		bool restartEnabled = false;

		void OnResize();
		void Buffer(const std::byte* buffer, unsigned vertexCount, unsigned byteCount);

	public:

		explicit SpriteBatch(const Camera& camera);
		~SpriteBatch();

		void Apply(Shader& shader);
		void SetMode(GLenum mode);
		void BindTexture(const Texture2D& texture);
		void DrawLine(const glm::vec2& p1, const glm::vec2& p2, const Color& color);
		void DrawBounds(const Bounds& bounds, const Color& color);

		template<class T, unsigned S>
		void Buffer(const std::array<T, S>& data);

		template<class T>
		void Buffer(const T* data, unsigned size);
		void Flush();
	};

	template<class T, unsigned S>
	void SpriteBatch::Buffer(const std::array<T, S>& data)
	{
		const unsigned byteCount = static_cast<unsigned>(sizeof(T)) * S;
		const unsigned vertexCount = byteCount / activeShader->GetStride();

		Buffer(reinterpret_cast<const std::byte*>(&data[0]), vertexCount, byteCount);
	}

	template<class T>
	void SpriteBatch::Buffer(const T* data, const unsigned size)
	{
		const unsigned byteCount = static_cast<unsigned>(sizeof(T)) * size;
		const unsigned vertexCount = byteCount / activeShader->GetStride();

		Buffer(reinterpret_cast<const std::byte*>(data), vertexCount, byteCount);
	}
}
